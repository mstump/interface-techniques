# ##############################################################################
# Copyright (c) 2023 Mercedes-Benz Tech Innovation GmbH
#
# This program and the accompanying materials are made available under the terms
# of the Eclipse Public License 2.0 which is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# ##############################################################################

add_executable(type_traits_test)
add_executable(type_traits::test ALIAS type_traits_test)
target_sources(type_traits_test PRIVATE main.cpp)
target_link_libraries(
  type_traits_test PRIVATE type_traits::interfaces exceptions_map::map
                           functions_map::map pointers_map::map)
add_test(NAME type_traits_test COMMAND type_traits_test)

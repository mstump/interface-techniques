/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>
#include <limits>
#include <type_traits>

#include "type_traits/interfaces/map_traits.hpp"

namespace type_traits::interfaces {

// GetWidth
template <typename LaneType>
using GetWidth_t = decltype(std::declval<LaneType>().GetWidth());

template <typename LaneType, typename = std::void_t<>>
struct has_GetWidth : std::false_type {};

template <typename LaneType>
struct has_GetWidth<LaneType, std::void_t<GetWidth_t<LaneType>>> : std::true_type {};

template <typename LaneType>
inline constexpr bool has_GetWidth_v = has_GetWidth<LaneType>::value;

// GetLeftLane
template <typename LaneType>
using GetLeftLane_t = decltype(std::declval<LaneType>().GetLeftLane());

template <typename LaneType, typename = std::void_t<>>
struct has_GetLeftLane : std::false_type {};

template <typename LaneType>
struct has_GetLeftLane<LaneType, std::void_t<GetLeftLane_t<LaneType>>> : std::true_type {};

template <typename LaneType>
inline constexpr bool has_GetLeftLane_v = has_GetLeftLane<LaneType>::value;

// GetRightLane
template <typename LaneType>
using GetRightLane_t = decltype(std::declval<LaneType>().GetRightLane());

template <typename LaneType, typename = std::void_t<>>
struct has_GetRightLane : std::false_type {};

template <typename LaneType>
struct has_GetRightLane<LaneType, std::void_t<GetRightLane_t<LaneType>>> : std::true_type {};

template <typename LaneType>
inline constexpr bool has_GetRightLane_v = has_GetRightLane<LaneType>::value;

// width
template <typename LaneType>
using width_t = decltype(std::declval<LaneType>().width);

template <typename LaneType, typename = std::void_t<>>
struct has_width : std::false_type {};

template <typename LaneType>
struct has_width<LaneType, std::void_t<width_t<LaneType>>> : std::true_type {};

template <typename LaneType>
inline constexpr bool has_width_v = has_width<LaneType>::value;

// left_lane pointer
template <typename LaneType>
using left_lane_t = decltype(std::declval<LaneType>().left_lane);

template <typename LaneType, typename = std::void_t<>>
struct has_left_lane : std::false_type {};

template <typename LaneType>
struct has_left_lane<LaneType, std::void_t<left_lane_t<LaneType>>> : std::true_type {};

template <typename LaneType>
inline constexpr bool has_left_lane_pointer_v =
    has_left_lane<LaneType>::value && std::is_pointer_v<left_lane_t<LaneType>>;

// right_lane pointer
template <typename LaneType>
using right_lane_t = decltype(std::declval<LaneType>().right_lane);

template <typename LaneType, typename = std::void_t<>>
struct has_right_lane : std::false_type {};

template <typename LaneType>
struct has_right_lane<LaneType, std::void_t<right_lane_t<LaneType>>> : std::true_type {};

template <typename LaneType>
inline constexpr bool has_right_lane_pointer_v =
    has_right_lane<LaneType>::value && std::is_pointer_v<right_lane_t<LaneType>>;

// ILane
template <typename LaneType>
struct ILane {
  explicit ILane(LaneType& lane) : lane_(lane) {}

  [[nodiscard]] auto GetWidth() const noexcept -> double {
    if constexpr (has_GetWidth_v<LaneType>) {
      try {
        return lane_.get().GetWidth();
      } catch (...) {
        return std::numeric_limits<double>::quiet_NaN();
      }
    } else if constexpr (has_width_v<LaneType>) {
      return lane_.get().width;

    } else {
      static_assert(always_false_v<LaneType>, "LaneType does not have a supported const GetWidth function");
    }
  }

  auto GetLeftLane() noexcept -> std::optional<ILane<LaneType>> {
    if constexpr (has_GetLeftLane_v<LaneType>) {
      using ReturnType = decltype(std::declval<LaneType>().GetLeftLane());

      if constexpr (std::is_reference_v<ReturnType>) {
        try {
          return ILane{lane_.get().GetLeftLane()};
        } catch (...) {
          return std::nullopt;
        }
      } else if constexpr (is_optional_reference_wrapper_v<ReturnType>) {
        if (auto left_lane = lane_.get().GetLeftLane(); left_lane.has_value()) {
          return ILane{left_lane.value().get()};
        }
        return std::nullopt;
      } else {
        static_assert(always_false_v<LaneType>, "LaneType does not have a supported GetLeftLane function");
      }

    } else if constexpr (has_left_lane_pointer_v<LaneType>) {
      if (auto* left_lane = lane_.get().left_lane; left_lane != nullptr) {
        return ILane{*left_lane};
      }
      return std::nullopt;

    } else {
      static_assert(always_false_v<LaneType>, "LaneType does not have a supported GetLeftLane function");
    }
  }

  auto GetLeftLane() const noexcept -> std::optional<const ILane<LaneType>> {
    if constexpr (has_GetLeftLane_v<LaneType>) {
      using ReturnType = decltype(std::declval<const LaneType>().GetLeftLane());

      if constexpr (std::is_reference_v<ReturnType>) {
        try {
          return ILane{lane_.get().GetLeftLane()};
        } catch (...) {
          return std::nullopt;
        }
      } else if constexpr (is_optional_reference_wrapper_v<ReturnType>) {
        if (const auto left_lane = lane_.get().GetLeftLane(); left_lane.has_value()) {
          return ILane{left_lane.value().get()};
        }
        return std::nullopt;
      } else {
        static_assert(always_false_v<LaneType>, "LaneType does not have a supported const GetLeftLane function");
      }

    } else if constexpr (has_left_lane_pointer_v<LaneType>) {
      if (auto* const left_lane = lane_.get().left_lane; left_lane != nullptr) {
        return ILane{*left_lane};
      }
      return std::nullopt;

    } else {
      static_assert(always_false_v<LaneType>, "LaneType does not have a supported const GetLeftLane function");
    }
  }

  auto GetRightLane() noexcept -> std::optional<ILane<LaneType>> {
    if constexpr (has_GetRightLane_v<LaneType>) {
      using ReturnType = decltype(std::declval<LaneType>().GetRightLane());

      if constexpr (std::is_reference_v<ReturnType>) {
        try {
          return ILane{lane_.get().GetRightLane()};
        } catch (...) {
          return std::nullopt;
        }
      } else if constexpr (is_optional_reference_wrapper_v<ReturnType>) {
        if (auto right_lane = lane_.get().GetRightLane(); right_lane.has_value()) {
          return ILane{right_lane.value().get()};
        }
        return std::nullopt;
      } else {
        static_assert(always_false_v<LaneType>, "LaneType does not have a supported  GetRightLane function");
      }

    } else if constexpr (has_right_lane_pointer_v<LaneType>) {
      if (auto* right_lane = lane_.get().left_lane; right_lane != nullptr) {
        return ILane{*right_lane};
      }
      return std::nullopt;

    } else {
      static_assert(always_false_v<LaneType>, "LaneType does not have a supported  GetRightLane function");
    }
  }

  auto GetRightLane() const noexcept -> std::optional<const ILane<LaneType>> {
    if constexpr (has_GetRightLane_v<LaneType>) {
      using ReturnType = decltype(std::declval<const LaneType>().GetRightLane());

      if constexpr (std::is_reference_v<ReturnType>) {
        try {
          return ILane{lane_.get().GetRightLane()};
        } catch (...) {
          return std::nullopt;
        }
      } else if constexpr (is_optional_reference_wrapper_v<ReturnType>) {
        if (const auto right_lane = lane_.get().GetRightLane(); right_lane.has_value()) {
          return ILane{right_lane.value().get()};
        }
        return std::nullopt;
      } else {
        static_assert(always_false_v<LaneType>, "LaneType does not have a supported const GetRightLane function");
      }

    } else if constexpr (has_right_lane_pointer_v<LaneType>) {
      if (auto* const right_lane = lane_.get().left_lane; right_lane != nullptr) {
        return ILane{*right_lane};
      }
      return std::nullopt;

    } else {
      static_assert(always_false_v<LaneType>, "LaneType does not have a supported const GetRightLane function");
    }
  }

  auto Get() noexcept -> LaneType& { return lane_.get(); }

  [[nodiscard]] auto Get() const noexcept -> const LaneType& { return lane_.get(); }

 private:
  std::reference_wrapper<LaneType> lane_;
};

}  // namespace type_traits::interfaces

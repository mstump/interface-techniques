/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>
#include <type_traits>

#include "common/common.hpp"
#include "type_traits/interfaces/i_lane.hpp"
#include "type_traits/interfaces/map_traits.hpp"

namespace type_traits::interfaces {

// GetLane
template <typename RoadType>
using GetLane_t = decltype(std::declval<RoadType>().GetLane(std::declval<int>()));

template <typename RoadType, typename = std::void_t<>>
struct has_GetLane : std::false_type {};

template <typename RoadType>
struct has_GetLane<RoadType, std::void_t<GetLane_t<RoadType>>> : std::true_type {};

template <typename RoadType>
inline constexpr bool has_GetLane_v = has_GetLane<RoadType>::value;

// GetNextRoad
template <typename RoadType>
using GetNextRoad_t = decltype(std::declval<RoadType>().GetNextRoad());

template <typename RoadType, typename = std::void_t<>>
struct has_GetNextRoad : std::false_type {};

template <typename RoadType>
struct has_GetNextRoad<RoadType, std::void_t<GetNextRoad_t<RoadType>>> : std::true_type {};

template <typename RoadType>
inline constexpr bool has_GetNextRoad_v = has_GetNextRoad<RoadType>::value;

// GetPreviousRoad
template <typename RoadType>
using GetPreviousRoad_t = decltype(std::declval<RoadType>().GetPreviousRoad());

template <typename RoadType, typename = std::void_t<>>
struct has_GetPreviousRoad : std::false_type {};

template <typename RoadType>
struct has_GetPreviousRoad<RoadType, std::void_t<GetPreviousRoad_t<RoadType>>> : std::true_type {};

template <typename RoadType>
inline constexpr bool has_GetPreviousRoad_v = has_GetPreviousRoad<RoadType>::value;

// lanes vector
template <typename RoadType>
using lanes_t = decltype(std::declval<RoadType>().lanes);

template <typename RoadType, typename = std::void_t<>>
struct has_lanes : std::false_type {};

template <typename RoadType>
struct has_lanes<RoadType, std::void_t<lanes_t<RoadType>>> : std::true_type {};

template <typename RoadType>
inline constexpr bool has_lanes_vector_v = has_lanes<RoadType>::value && is_vector_v<lanes_t<RoadType>>;

// next_road pointer
template <typename RoadType>
using next_road_t = decltype(std::declval<RoadType>().next_road);

template <typename RoadType, typename = std::void_t<>>
struct has_next_road : std::false_type {};

template <typename RoadType>
struct has_next_road<RoadType, std::void_t<next_road_t<RoadType>>> : std::true_type {};

template <typename RoadType>
inline constexpr bool has_next_road_pointer_v =
    has_next_road<RoadType>::value && std::is_pointer_v<next_road_t<RoadType>>;

// previous_road pointer
template <typename RoadType>
using previous_road_t = decltype(std::declval<RoadType>().previous_road);

template <typename RoadType, typename = std::void_t<>>
struct has_previous_road : std::false_type {};

template <typename RoadType>
struct has_previous_road<RoadType, std::void_t<previous_road_t<RoadType>>> : std::true_type {};

template <typename RoadType>
inline constexpr bool has_previous_road_pointer_v =
    has_previous_road<RoadType>::value && std::is_pointer_v<previous_road_t<RoadType>>;

// IRoad
template <typename RoadType>
struct IRoad {
  explicit IRoad(RoadType& road) : road_(road) {}

  auto GetLane(const common::LaneId identifier) noexcept -> decltype(auto) {
    if constexpr (has_GetLane_v<RoadType>) {
      using ReturnType = decltype(std::declval<RoadType>().GetLane(std::declval<common::LaneId::UnderlyingType>()));

      if constexpr (std::is_reference_v<ReturnType>) {
        using LaneType = std::remove_reference_t<ReturnType>;

        try {
          return std::make_optional<ILane<LaneType>>(road_.get().GetLane(identifier()));
        } catch (...) {
          return std::optional<ILane<LaneType>>{};
        }

      } else if constexpr (is_optional_reference_wrapper_v<ReturnType>) {
        using LaneType = typename ReturnType::value_type::type;

        if (auto lane = road_.get().GetLane(identifier()); lane.has_value()) {
          return std::make_optional<ILane<LaneType>>(lane.value().get());
        }
        return std::optional<ILane<LaneType>>{};

      } else {
        static_assert(always_false_v<RoadType>, "RoadType does not have a supported GetLane function");
      }

    } else if constexpr (has_lanes_vector_v<RoadType>) {
      using LaneType =
          std::remove_reference_t<decltype(std::declval<RoadType>().lanes.at(std::declval<std::size_t>()))>;

      try {
        return std::make_optional<ILane<LaneType>>(road_.get().lanes.at(identifier()));
      } catch (...) {
        return std::optional<ILane<LaneType>>{};
      }

    } else {
      static_assert(always_false_v<RoadType>, "RoadType does not have a supported GetLane function");
    }
  }

  auto GetLane(const common::LaneId identifier) const noexcept -> decltype(auto) {
    if constexpr (has_GetLane_v<RoadType>) {
      using ReturnType = decltype(std::declval<RoadType>().GetLane(std::declval<int>()));

      if constexpr (std::is_reference_v<ReturnType>) {
        using LaneType = std::remove_reference_t<ReturnType>;

        try {
          return std::make_optional<const ILane<LaneType>>(road_.get().GetLane(identifier()));
        } catch (...) {
          return std::optional<const ILane<LaneType>>{};
        }

      } else if constexpr (is_optional_reference_wrapper_v<ReturnType>) {
        using LaneType = typename ReturnType::value_type::type;

        if (const auto lane = road_.get().GetLane(identifier()); lane.has_value()) {
          return std::make_optional<const ILane<LaneType>>(lane.value().get());
        }
        return std::optional<const ILane<LaneType>>{};

      } else {
        static_assert(always_false_v<RoadType>, "RoadType does not have a supported const GetLane function");
      }

    } else if constexpr (has_lanes_vector_v<RoadType>) {
      using LaneType =
          std::remove_reference_t<decltype(std::declval<RoadType>().lanes.at(std::declval<std::size_t>()))>;

      try {
        return std::make_optional<const ILane<LaneType>>(road_.get().lanes.at(identifier()));
      } catch (...) {
        return std::optional<const ILane<LaneType>>{};
      }

    } else {
      static_assert(always_false_v<RoadType>, "RoadType does not have a supported const GetLane function");
    }
  }

  auto GetNextRoad() noexcept -> std::optional<IRoad<RoadType>> {
    if constexpr (has_GetNextRoad_v<RoadType>) {
      using ReturnType = decltype(std::declval<RoadType>().GetNextRoad());

      if constexpr (std::is_reference_v<ReturnType>) {
        try {
          return IRoad{road_.get().GetNextRoad()};
        } catch (...) {
          return std::nullopt;
        }
      } else if constexpr (is_optional_reference_wrapper_v<ReturnType>) {
        if (auto next_road = road_.get().GetNextRoad(); next_road.has_value()) {
          return IRoad{next_road.value().get()};
        }
        return std::nullopt;
      } else {
        static_assert(always_false_v<RoadType>, "RoadType does not have a supported GetNextRoad function");
      }

    } else if constexpr (has_next_road_pointer_v<RoadType>) {
      if (auto* next_road = road_.get().next_road; next_road != nullptr) {
        return IRoad{*next_road};
      }
      return std::nullopt;

    } else {
      static_assert(always_false_v<RoadType>, "RoadType does not have a supported GetNextRoad function");
    }
  }

  auto GetNextRoad() const noexcept -> std::optional<const IRoad<RoadType>> {
    if constexpr (has_GetNextRoad_v<RoadType>) {
      using ReturnType = decltype(std::declval<RoadType>().GetNextRoad());

      if constexpr (std::is_reference_v<ReturnType>) {
        try {
          return IRoad{road_.get().GetNextRoad()};
        } catch (...) {
          return std::nullopt;
        }
      } else if constexpr (is_optional_reference_wrapper_v<ReturnType>) {
        if (auto next_road = road_.get().GetNextRoad(); next_road.has_value()) {
          return IRoad{next_road.value().get()};
        }
        return std::nullopt;
      } else {
        static_assert(always_false_v<RoadType>, "RoadType does not have a supported const GetNextRoad function");
      }

    } else if constexpr (has_next_road_pointer_v<RoadType>) {
      if (auto* const next_road = road_.get().next_road; next_road != nullptr) {
        return IRoad{*next_road};
      }
      return std::nullopt;

    } else {
      static_assert(always_false_v<RoadType>, "RoadType does not have a supported const GetNextRoad function");
    }
  }

  auto GetPreviousRoad() noexcept -> std::optional<IRoad<RoadType>> {
    if constexpr (has_GetPreviousRoad_v<RoadType>) {
      using ReturnType = decltype(std::declval<RoadType>().GetPreviousRoad());

      if constexpr (std::is_reference_v<ReturnType>) {
        try {
          return IRoad{road_.get().GetPreviousRoad()};
        } catch (...) {
          return std::nullopt;
        }
      } else if constexpr (is_optional_reference_wrapper_v<ReturnType>) {
        if (auto previous_road = road_.get().GetPreviousRoad(); previous_road.has_value()) {
          return IRoad{previous_road.value().get()};
        }
        return std::nullopt;
      } else {
        static_assert(always_false_v<RoadType>, "RoadType does not have a supported GetPreviousRoad function");
      }

    } else if constexpr (has_previous_road_pointer_v<RoadType>) {
      if (auto* previous_road = road_.get().previous_road; previous_road != nullptr) {
        return IRoad{*previous_road};
      }
      return std::nullopt;

    } else {
      static_assert(always_false_v<RoadType>, "RoadType does not have a supported GetPreviousRoad function");
    }
  }

  auto GetPreviousRoad() const noexcept -> std::optional<const IRoad<RoadType>> {
    if constexpr (has_GetPreviousRoad_v<RoadType>) {
      using ReturnType = decltype(std::declval<RoadType>().GetPreviousRoad());

      if constexpr (std::is_reference_v<ReturnType>) {
        try {
          return IRoad{road_.get().GetPreviousRoad()};
        } catch (...) {
          return std::nullopt;
        }
      } else if constexpr (is_optional_reference_wrapper_v<ReturnType>) {
        if (const auto previous_road = road_.get().GetPreviousRoad(); previous_road.has_value()) {
          return IRoad{previous_road.value().get()};
        }
        return std::nullopt;
      } else {
        static_assert(always_false_v<RoadType>, "RoadType does not have a supported const GetPreviousRoad function");
      }

    } else if constexpr (has_previous_road_pointer_v<RoadType>) {
      if (auto* const previous_road = road_.get().previous_road; previous_road != nullptr) {
        return IRoad{*previous_road};
      }
      return std::nullopt;

    } else {
      static_assert(always_false_v<RoadType>, "RoadType does not have a supported const GetPreviousRoad function");
    }
  }

  auto Get() noexcept -> RoadType& { return road_.get(); }
  [[nodiscard]] auto Get() const noexcept -> const RoadType& { return road_.get(); }

 private:
  std::reference_wrapper<RoadType> road_;
};

}  // namespace type_traits::interfaces

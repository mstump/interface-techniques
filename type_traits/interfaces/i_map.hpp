/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>
#include <optional>
#include <type_traits>

#include "common/common.hpp"
#include "type_traits/interfaces/i_road.hpp"
#include "type_traits/interfaces/map_traits.hpp"

namespace type_traits::interfaces {

// GetRoad
template <typename MapType>
using GetRoad_t = decltype(std::declval<MapType>().GetRoad(std::declval<int>()));

template <typename MapType, typename = std::void_t<>>
struct has_GetRoad : std::false_type {};

template <typename MapType>
struct has_GetRoad<MapType, std::void_t<GetRoad_t<MapType>>> : std::true_type {};

template <typename MapType>
inline constexpr bool has_GetRoad_v = has_GetRoad<MapType>::value;

// roads vector
template <typename MapType>
using roads_t = decltype(std::declval<MapType>().roads);

template <typename MapType, typename = std::void_t<>>
struct has_roads : std::false_type {};

template <typename MapType>
struct has_roads<MapType, std::void_t<roads_t<MapType>>> : std::true_type {};

template <typename MapType>
inline constexpr bool has_roads_vector_v =
    has_roads<MapType>::value && is_vector_v<decltype(std::declval<MapType>().roads)>;

template <typename MapType>
struct IMap {
  explicit IMap(MapType& map) : map_(map) {}

  auto GetRoad(const common::RoadId identifier) noexcept -> decltype(auto) {
    if constexpr (has_GetRoad_v<MapType>) {
      using ReturnType =
          decltype(std::declval<const MapType>().GetRoad(std::declval<common::RoadId::UnderlyingType>()));

      if constexpr (std::is_reference_v<ReturnType>) {
        using RoadType = std::remove_reference_t<ReturnType>;

        try {
          return std::make_optional<IRoad<RoadType>>(map_.get().GetRoad(identifier()));
        } catch (...) {
          return std::optional<IRoad<RoadType>>{};
        }

      } else if constexpr (is_optional_reference_wrapper_v<ReturnType>) {
        using RoadType = typename ReturnType::value_type::type;

        if (auto road = map_.get().GetRoad(identifier()); road.has_value()) {
          return std::make_optional<IRoad<RoadType>>(road.value().get());
        }
        return std::optional<IRoad<RoadType>>{};
      }

      else {
        static_assert(always_false_v<MapType>, "MapType not supported");
      }

    } else if constexpr (has_roads_vector_v<MapType>) {
      using RoadType = std::remove_reference_t<decltype(std::declval<MapType>().roads.at(std::declval<std::size_t>()))>;

      try {
        return std::make_optional<IRoad<RoadType>>(map_.get().roads.at(identifier()));
      } catch (...) {
        return std::optional<IRoad<RoadType>>{};
      }

    } else {
      static_assert(always_false_v<MapType>, "MapType not supported");
    }
  }

  auto GetRoad(const common::RoadId identifier) const noexcept -> decltype(auto) {
    if constexpr (has_GetRoad_v<MapType>) {
      using ReturnType = decltype(std::declval<const MapType>().GetRoad(std::declval<int>()));

      if constexpr (std::is_reference_v<ReturnType>) {
        using RoadType = std::remove_reference_t<ReturnType>;

        try {
          return std::make_optional<const IRoad<RoadType>>(map_.get().GetRoad(identifier()));
        } catch (...) {
          return std::optional<const IRoad<RoadType>>{};
        }

      } else if constexpr (is_optional_reference_wrapper_v<ReturnType>) {
        using RoadType = typename ReturnType::value_type::type;

        if (const auto road = map_.get().GetRoad(identifier()); road.has_value()) {
          return std::make_optional<const IRoad<RoadType>>(road.value().get());
        }
        return std::optional<const IRoad<RoadType>>{};
      }

      else {
        static_assert(always_false_v<MapType>, "MapType not supported");
      }

    } else if constexpr (has_roads_vector_v<MapType>) {
      using RoadType = std::remove_reference_t<decltype(std::declval<MapType>().roads.at(std::declval<std::size_t>()))>;

      try {
        return std::make_optional<const IRoad<RoadType>>(map_.get().roads.at(identifier()));
      } catch (...) {
        return std::optional<const IRoad<RoadType>>{};
      }

    } else {
      static_assert(always_false_v<MapType>, "MapType not supported");
    }
  }

  auto Get() noexcept -> MapType& { return map_.get(); }
  [[nodiscard]] auto Get() const noexcept -> const MapType& { return map_.get(); }

 private:
  std::reference_wrapper<MapType> map_;
};

}  // namespace type_traits::interfaces

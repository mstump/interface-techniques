/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>
#include <optional>
#include <type_traits>

#include "type_traits_if_constexpr/interfaces/map_traits.hpp"

namespace type_traits_if_constexpr::interfaces {

// GetWidth
template <typename LaneType>
using GetWidth_t = decltype(std::declval<LaneType>().GetWidth());

template <typename LaneType, typename = std::void_t<>>
struct has_GetWidth : std::false_type {};

template <typename LaneType>
struct has_GetWidth<LaneType, std::void_t<GetWidth_t<LaneType>>> : std::true_type {};

template <typename LaneType>
inline constexpr bool has_GetWidth_v = has_GetWidth<LaneType>::value;

// GetLeftLane
template <typename LaneType>
using GetLeftLane_t = decltype(std::declval<LaneType>().GetLeftLane());

template <typename LaneType, typename = std::void_t<>>
struct has_GetLeftLane : std::false_type {};

template <typename LaneType>
struct has_GetLeftLane<LaneType, std::void_t<GetLeftLane_t<LaneType>>> : std::true_type {};

template <typename LaneType>
inline constexpr bool has_GetLeftLane_v = has_GetLeftLane<LaneType>::value;

// GetRightLane
template <typename LaneType>
using GetRightLane_t = decltype(std::declval<LaneType>().GetRightLane());

template <typename LaneType, typename = std::void_t<>>
struct has_GetRightLane : std::false_type {};

template <typename LaneType>
struct has_GetRightLane<LaneType, std::void_t<GetRightLane_t<LaneType>>> : std::true_type {};

template <typename LaneType>
inline constexpr bool has_GetRightLane_v = has_GetRightLane<LaneType>::value;

// width
template <typename LaneType>
using width_t = decltype(std::declval<LaneType>().width);

template <typename LaneType, typename = std::void_t<>>
struct has_width : std::false_type {};

template <typename LaneType>
struct has_width<LaneType, std::void_t<width_t<LaneType>>> : std::true_type {};

template <typename LaneType>
inline constexpr bool has_width_v = has_width<LaneType>::value;

// left_lane pointer
template <typename LaneType>
using left_lane_t = decltype(std::declval<LaneType>().left_lane);

template <typename LaneType, typename = std::void_t<>>
struct has_left_lane : std::false_type {};

template <typename LaneType>
struct has_left_lane<LaneType, std::void_t<left_lane_t<LaneType>>> : std::true_type {};

template <typename LaneType>
inline constexpr bool has_left_lane_pointer_v =
    has_left_lane<LaneType>::value && std::is_pointer_v<left_lane_t<LaneType>>;

// right_lane pointer
template <typename LaneType>
using right_lane_t = decltype(std::declval<LaneType>().right_lane);

template <typename LaneType, typename = std::void_t<>>
struct has_right_lane : std::false_type {};

template <typename LaneType>
struct has_right_lane<LaneType, std::void_t<right_lane_t<LaneType>>> : std::true_type {};

template <typename LaneType>
inline constexpr bool has_right_lane_pointer_v =
    has_right_lane<LaneType>::value && std::is_pointer_v<right_lane_t<LaneType>>;

// GetWidth
template <typename LaneType>
struct Width {
  static auto Get(const LaneType& lane) noexcept -> double {
    if constexpr (has_GetWidth_v<LaneType>) {
      try {
        return lane.GetWidth();
      } catch (...) {
        return std::numeric_limits<double>::quiet_NaN();
      }
    } else if constexpr (has_width_v<LaneType>) {
      return lane.width;

    } else {
      static_assert(always_false_v<LaneType>, "LaneType does not have a supported const GetWidth function");
    }
  }
};

template <typename LaneType>
auto GetWidth(const LaneType& lane) -> double {
  return Width<LaneType>::Get(lane);
}

// GetLeftLane
template <typename LaneType>
struct LeftLane {
  static auto Get(const LaneType& lane) noexcept -> std::optional<std::reference_wrapper<const LaneType>> {
    if constexpr (has_GetLeftLane_v<LaneType>) {
      using ReturnType = decltype(std::declval<const LaneType>().GetLeftLane());

      if constexpr (std::is_reference_v<ReturnType>) {
        try {
          return lane.GetLeftLane();
        } catch (...) {
          return std::nullopt;
        }
      } else if constexpr (is_optional_reference_wrapper_v<ReturnType>) {
        if (auto left_lane = lane.GetLeftLane(); left_lane.has_value()) {
          return left_lane.value().get();
        }
        return std::nullopt;
      } else {
        static_assert(always_false_v<LaneType>, "LaneType does not have a supported GetLeftLane function");
      }

    } else if constexpr (has_left_lane_pointer_v<LaneType>) {
      if (auto* left_lane = lane.left_lane; left_lane != nullptr) {
        return *left_lane;
      }
      return std::nullopt;

    } else {
      static_assert(always_false_v<LaneType>, "LaneType does not have a supported GetLeftLane function");
    }
  }
};

template <typename LaneType>
auto GetLeftLane(const LaneType& lane) -> std::optional<std::reference_wrapper<const LaneType>> {
  return LeftLane<LaneType>::Get(lane);
}

// GetRightLane
template <typename LaneType>
struct RightLane {
  static auto Get(const LaneType& lane) noexcept -> std::optional<std::reference_wrapper<const LaneType>> {
    if constexpr (has_GetRightLane_v<LaneType>) {
      using ReturnType = decltype(std::declval<const LaneType>().GetRightLane());

      if constexpr (std::is_reference_v<ReturnType>) {
        try {
          return lane.GetRightLane();
        } catch (...) {
          return std::nullopt;
        }
      } else if constexpr (is_optional_reference_wrapper_v<ReturnType>) {
        if (const auto right_lane = lane.GetRightLane(); right_lane.has_value()) {
          return right_lane.value().get();
        }
        return std::nullopt;
      } else {
        static_assert(always_false_v<LaneType>, "LaneType does not have a supported const GetRightLane function");
      }

    } else if constexpr (has_right_lane_pointer_v<LaneType>) {
      if (auto* const right_lane = lane.left_lane; right_lane != nullptr) {
        return *right_lane;
      }
      return std::nullopt;

    } else {
      static_assert(always_false_v<LaneType>, "LaneType does not have a supported const GetRightLane function");
    }
  }
};

template <typename LaneType>
auto GetRightLane(const LaneType& lane) -> std::optional<std::reference_wrapper<const LaneType>> {
  return RightLane<LaneType>::Get(lane);
}

}  // namespace type_traits_if_constexpr::interfaces

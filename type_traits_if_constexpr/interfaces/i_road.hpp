/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>
#include <optional>
#include <type_traits>
#include <utility>

#include "common/common.hpp"
#include "type_traits_if_constexpr/interfaces/map_traits.hpp"

namespace type_traits_if_constexpr::interfaces {

// GetLane
template <typename RoadType>
using GetLane_t = decltype(std::declval<RoadType>().GetLane(std::declval<int>()));

template <typename RoadType, typename = std::void_t<>>
struct has_GetLane : std::false_type {};

template <typename RoadType>
struct has_GetLane<RoadType, std::void_t<GetLane_t<RoadType>>> : std::true_type {};

template <typename RoadType>
inline constexpr bool has_GetLane_v = has_GetLane<RoadType>::value;

// GetNextRoad
template <typename RoadType>
using GetNextRoad_t = decltype(std::declval<RoadType>().GetNextRoad());

template <typename RoadType, typename = std::void_t<>>
struct has_GetNextRoad : std::false_type {};

template <typename RoadType>
struct has_GetNextRoad<RoadType, std::void_t<GetNextRoad_t<RoadType>>> : std::true_type {};

template <typename RoadType>
inline constexpr bool has_GetNextRoad_v = has_GetNextRoad<RoadType>::value;

// GetPreviousRoad
template <typename RoadType>
using GetPreviousRoad_t = decltype(std::declval<RoadType>().GetPreviousRoad());

template <typename RoadType, typename = std::void_t<>>
struct has_GetPreviousRoad : std::false_type {};

template <typename RoadType>
struct has_GetPreviousRoad<RoadType, std::void_t<GetPreviousRoad_t<RoadType>>> : std::true_type {};

template <typename RoadType>
inline constexpr bool has_GetPreviousRoad_v = has_GetPreviousRoad<RoadType>::value;

// lanes vector
template <typename RoadType>
using lanes_t = decltype(std::declval<RoadType>().lanes);

template <typename RoadType, typename = std::void_t<>>
struct has_lanes : std::false_type {};

template <typename RoadType>
struct has_lanes<RoadType, std::void_t<lanes_t<RoadType>>> : std::true_type {};

template <typename RoadType>
inline constexpr bool has_lanes_vector_v = has_lanes<RoadType>::value && is_vector_v<lanes_t<RoadType>>;

// next_road pointer
template <typename RoadType>
using next_road_t = decltype(std::declval<RoadType>().next_road);

template <typename RoadType, typename = std::void_t<>>
struct has_next_road : std::false_type {};

template <typename RoadType>
struct has_next_road<RoadType, std::void_t<next_road_t<RoadType>>> : std::true_type {};

template <typename RoadType>
inline constexpr bool has_next_road_pointer_v =
    has_next_road<RoadType>::value && std::is_pointer_v<next_road_t<RoadType>>;

// previous_road pointer
template <typename RoadType>
using previous_road_t = decltype(std::declval<RoadType>().previous_road);

template <typename RoadType, typename = std::void_t<>>
struct has_previous_road : std::false_type {};

template <typename RoadType>
struct has_previous_road<RoadType, std::void_t<previous_road_t<RoadType>>> : std::true_type {};

template <typename RoadType>
inline constexpr bool has_previous_road_pointer_v =
    has_previous_road<RoadType>::value && std::is_pointer_v<previous_road_t<RoadType>>;

// GetLane
template <typename RoadType>
struct Lane {
  static auto Get(const RoadType& road, const common::LaneId identifier) noexcept -> decltype(auto) {
    if constexpr (has_GetLane_v<RoadType>) {
      using ReturnType =
          decltype(std::declval<const RoadType>().GetLane(std::declval<common::LaneId::UnderlyingType>()));

      if constexpr (std::is_reference_v<ReturnType>) {
        using LaneType = std::remove_reference_t<ReturnType>;

        try {
          return std::make_optional<std::reference_wrapper<const LaneType>>(road.GetLane(identifier()));
        } catch (...) {
          return std::optional<std::reference_wrapper<const LaneType>>{};
        }

      } else if constexpr (is_optional_reference_wrapper_v<ReturnType>) {
        using LaneType = typename ReturnType::value_type::type;

        if (auto lane = road.GetLane(identifier()); lane.has_value()) {
          return std::make_optional<std::reference_wrapper<const LaneType>>(lane.value().get());
        }
        return std::optional<std::reference_wrapper<const LaneType>>{};

      } else {
        static_assert(always_false_v<RoadType>, "RoadType not supported");
      }

    } else if constexpr (has_lanes_vector_v<RoadType>) {
      using LaneType = std::remove_reference_t<decltype(std::declval<const RoadType>().lanes.at(
          std::declval<common::LaneId::UnderlyingType>()))>;

      try {
        return std::make_optional<std::reference_wrapper<const LaneType>>(road.lanes.at(identifier()));
      } catch (...) {
        return std::optional<std::reference_wrapper<const LaneType>>{};
      }

    } else {
      static_assert(always_false_v<RoadType>, "RoadType not supported");
    }
  }
};

template <typename RoadType>
auto GetLane(const RoadType& road, const common::LaneId identifier) -> decltype(auto) {
  return Lane<RoadType>::Get(road, identifier);
}

// GetNextRoad
template <typename RoadType>
struct NextRoad {
  static auto Get(const RoadType& road) noexcept -> std::optional<std::reference_wrapper<const RoadType>> {
    if constexpr (has_GetNextRoad_v<RoadType>) {
      using ReturnType = decltype(std::declval<const RoadType>().GetNextRoad());

      if constexpr (std::is_reference_v<ReturnType>) {
        try {
          return road.GetNextRoad();
        } catch (...) {
          return std::nullopt;
        }
      } else if constexpr (is_optional_reference_wrapper_v<ReturnType>) {
        if (auto next_road = road.GetNextRoad(); next_road.has_value()) {
          return next_road.value().get();
        }
        return std::nullopt;
      } else {
        static_assert(always_false_v<RoadType>, "RoadType does not have a supported GetNextRoad function");
      }

    } else if constexpr (has_next_road_pointer_v<RoadType>) {
      if (auto* next_road = road.next_road; next_road != nullptr) {
        return *next_road;
      }
      return std::nullopt;

    } else {
      static_assert(always_false_v<RoadType>, "RoadType does not have a supported GetNextRoad function");
    }
  }
};

template <typename RoadType>
auto GetNextRoad(const RoadType& road) -> std::optional<std::reference_wrapper<const RoadType>> {
  return NextRoad<RoadType>::Get(road);
}

// GetPreviousRoad
template <typename RoadType>
struct PreviousRoad {
  static auto Get(const RoadType& road) noexcept -> std::optional<std::reference_wrapper<const RoadType>> {
    if constexpr (has_GetPreviousRoad_v<RoadType>) {
      using ReturnType = decltype(std::declval<const RoadType>().GetPreviousRoad());

      if constexpr (std::is_reference_v<ReturnType>) {
        try {
          return road.GetPreviousRoad();
        } catch (...) {
          return std::nullopt;
        }
      } else if constexpr (is_optional_reference_wrapper_v<ReturnType>) {
        if (auto previous_road = road.GetPreviousRoad(); previous_road.has_value()) {
          return previous_road.value().get();
        }
        return std::nullopt;
      } else {
        static_assert(always_false_v<RoadType>, "RoadType does not have a supported GetPreviousRoad function");
      }

    } else if constexpr (has_previous_road_pointer_v<RoadType>) {
      if (auto* previous_road = road.previous_road; previous_road != nullptr) {
        return *previous_road;
      }
      return std::nullopt;

    } else {
      static_assert(always_false_v<RoadType>, "RoadType does not have a supported GetPreviousRoad function");
    }
  }
};

template <typename RoadType>
auto GetPreviousRoad(const RoadType& road) -> std::optional<std::reference_wrapper<const RoadType>> {
  return PreviousRoad<RoadType>::Get(road);
}

}  // namespace type_traits_if_constexpr::interfaces

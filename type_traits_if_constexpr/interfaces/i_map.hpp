/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>
#include <optional>
#include <type_traits>
#include <utility>

#include "common/common.hpp"
#include "type_traits_if_constexpr/interfaces/map_traits.hpp"

namespace type_traits_if_constexpr::interfaces {

// GetRoad
template <typename MapType>
using GetRoad_t = decltype(std::declval<MapType>().GetRoad(std::declval<int>()));

template <typename MapType, typename = std::void_t<>>
struct has_GetRoad : std::false_type {};

template <typename MapType>
struct has_GetRoad<MapType, std::void_t<GetRoad_t<MapType>>> : std::true_type {};

template <typename MapType>
inline constexpr bool has_GetRoad_v = has_GetRoad<MapType>::value;

// roads vector
template <typename MapType>
using roads_t = decltype(std::declval<MapType>().roads);

template <typename MapType, typename = std::void_t<>>
struct has_roads : std::false_type {};

template <typename MapType>
struct has_roads<MapType, std::void_t<roads_t<MapType>>> : std::true_type {};

template <typename MapType>
inline constexpr bool has_roads_vector_v =
    has_roads<MapType>::value && is_vector_v<decltype(std::declval<MapType>().roads)>;

// GetRoad
template <typename MapType>
struct Road {
  static auto Get(const MapType& map, const common::RoadId identifier) noexcept -> decltype(auto) {
    if constexpr (has_GetRoad_v<MapType>) {
      using ReturnType =
          decltype(std::declval<const MapType>().GetRoad(std::declval<common::RoadId::UnderlyingType>()));

      if constexpr (std::is_reference_v<ReturnType>) {
        using RoadType = std::remove_reference_t<ReturnType>;

        try {
          return std::make_optional<std::reference_wrapper<const RoadType>>(map.GetRoad(identifier()));
        } catch (...) {
          return std::optional<std::reference_wrapper<const RoadType>>{};
        }

      } else if constexpr (is_optional_reference_wrapper_v<ReturnType>) {
        using RoadType = typename ReturnType::value_type::type;

        if (auto road = map.GetRoad(identifier()); road.has_value()) {
          return std::make_optional<std::reference_wrapper<const RoadType>>(road.value().get());
        }
        return std::optional<std::reference_wrapper<const RoadType>>{};
      }

      else {
        static_assert(always_false_v<MapType>, "MapType not supported");
      }

    } else if constexpr (has_roads_vector_v<MapType>) {
      using RoadType =
          std::remove_reference_t<decltype(std::declval<const MapType>().roads.at(std::declval<std::size_t>()))>;

      try {
        return std::make_optional<std::reference_wrapper<const RoadType>>(map.roads.at(identifier()));
      } catch (...) {
        return std::optional<std::reference_wrapper<const RoadType>>{};
      }

    } else {
      static_assert(always_false_v<MapType>, "MapType not supported");
    }
  }
};

template <typename MapType>
auto GetRoad(const MapType& map, const common::RoadId identifier) -> decltype(auto) {
  return Road<MapType>::Get(map, identifier);
}

}  // namespace type_traits_if_constexpr::interfaces

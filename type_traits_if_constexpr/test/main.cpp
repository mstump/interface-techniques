/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include <exceptions_map.hpp>
#include <functions_map.hpp>
#include <pointers_map.hpp>

#include "common/common.hpp"
#include "type_traits_if_constexpr/interfaces/i_lane.hpp"
#include "type_traits_if_constexpr/interfaces/i_map.hpp"
#include "type_traits_if_constexpr/interfaces/i_road.hpp"

template <typename T>
auto LocalGetNextLeftWidth(const T& map, common::RoadId road_id, common::LaneId lane_id) noexcept {
  const auto road = type_traits_if_constexpr::interfaces::GetRoad(map, road_id);
  if (!road) {
    return 0.0;
  }

  const auto next_road = type_traits_if_constexpr::interfaces::GetNextRoad(road.value().get());
  if (!next_road) {
    return 0.0;
  }

  const auto lane = type_traits_if_constexpr::interfaces::GetLane(next_road.value().get(), lane_id);
  if (!lane) {
    return 0.0;
  }

  const auto left_lane = type_traits_if_constexpr::interfaces::GetLeftLane(lane.value().get());
  if (!left_lane) {
    return 0.0;
  }

  const auto width = type_traits_if_constexpr::interfaces::GetWidth(left_lane.value().get());

  return width;
}

template <typename T>
auto LocalGetPreviousRightWidth(const T& map, common::RoadId road_id, common::LaneId lane_id) noexcept {
  const auto road = type_traits_if_constexpr::interfaces::GetRoad(map, road_id);
  if (!road) {
    return 0.0;
  }

  const auto previous_road = type_traits_if_constexpr::interfaces::GetPreviousRoad(road.value().get());
  if (!previous_road) {
    return 0.0;
  }

  const auto lane = type_traits_if_constexpr::interfaces::GetLane(previous_road.value().get(), lane_id);
  if (!lane) {
    return 0.0;
  }

  const auto right_lane = type_traits_if_constexpr::interfaces::GetRightLane(lane.value().get());
  if (!right_lane) {
    return 0.0;
  }

  const auto width = type_traits_if_constexpr::interfaces::GetWidth(right_lane.value().get());

  return width;
}

auto main() -> int {
  using common::LaneId;
  using common::RoadId;

  auto width{0.0};

  {
    auto map = exceptions_map::ExceptionsMap::Create();

    width += LocalGetNextLeftWidth(map, RoadId{1}, LaneId{1});
    width += LocalGetPreviousRightWidth(map, RoadId{1}, LaneId{1});
  }

  {
    auto map = functions_map::FunctionsMap::Create();

    width += LocalGetNextLeftWidth(map, RoadId{1}, LaneId{1});
    width += LocalGetPreviousRightWidth(map, RoadId{1}, LaneId{1});
  }

  {
    auto map = pointers_map::PointersMap::Create();

    width += LocalGetNextLeftWidth(map, RoadId{1}, LaneId{1});
    width += LocalGetPreviousRightWidth(map, RoadId{1}, LaneId{1});
  }

  constexpr auto kExpectedWidth{6.0};
  return width == kExpectedWidth ? 0 : 1;
}

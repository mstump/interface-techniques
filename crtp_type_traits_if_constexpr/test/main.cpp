/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include <exceptions_map.hpp>
#include <functions_map.hpp>
#include <pointers_map.hpp>
#include <type_traits>

#include "common/common.hpp"
#include "crtp_type_traits_if_constexpr/adapters/lane_adapter.hpp"
#include "crtp_type_traits_if_constexpr/adapters/map_adapter.hpp"
#include "crtp_type_traits_if_constexpr/adapters/road_adapter.hpp"

auto main() -> int {
  using common::LaneId;
  using common::RoadId;

  auto width{0.0};

  {
    auto exceptions_map = exceptions_map::ExceptionsMap::Create();
    auto exceptions_map_adapter = crtp_type_traits_if_constexpr::adapters::MapAdapter{exceptions_map};

    static_assert(std::is_same_v<std::decay_t<decltype(exceptions_map_adapter.Get())>, exceptions_map::ExceptionsMap>);

    width += GetNextLeftWidth(exceptions_map_adapter, RoadId{1}, LaneId{1});
    width += GetPreviousRightWidth(exceptions_map_adapter, RoadId{1}, LaneId{1});
  }

  {
    auto functions_map = functions_map::FunctionsMap::Create();
    auto functions_map_adapter = crtp_type_traits_if_constexpr::adapters::MapAdapter{functions_map};

    static_assert(std::is_same_v<std::decay_t<decltype(functions_map_adapter.Get())>, functions_map::FunctionsMap>);

    width += GetNextLeftWidth(functions_map_adapter, RoadId{1}, LaneId{1});
    width += GetPreviousRightWidth(functions_map_adapter, RoadId{1}, LaneId{1});
  }

  {
    auto pointers_map = pointers_map::PointersMap::Create();
    auto pointers_map_adapter = crtp_type_traits_if_constexpr::adapters::MapAdapter{pointers_map};

    static_assert(std::is_same_v<std::decay_t<decltype(pointers_map_adapter.Get())>, pointers_map::PointersMap>);

    width += GetNextLeftWidth(pointers_map_adapter, RoadId{1}, LaneId{1});
    width += GetPreviousRightWidth(pointers_map_adapter, RoadId{1}, LaneId{1});
  }

  constexpr auto kExpectedWidth{6.0};
  return width == kExpectedWidth ? 0 : 1;
}

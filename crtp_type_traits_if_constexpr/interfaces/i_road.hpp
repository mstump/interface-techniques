/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "common/common.hpp"

namespace crtp_type_traits_if_constexpr::interfaces {

template <typename T>
struct IRoad {
  auto GetLane(const common::LaneId identifier) noexcept -> decltype(auto) { return Self().DoGetLane(identifier); }
  [[nodiscard]] auto GetLane(const common::LaneId identifier) const noexcept -> decltype(auto) {
    return Self().DoGetLane(identifier);
  }

  auto GetNextRoad() noexcept -> decltype(auto) { return Self().DoGetNextRoad(); }
  [[nodiscard]] auto GetNextRoad() const noexcept -> decltype(auto) { return Self().DoGetNextRoad(); }

  auto GetPreviousRoad() noexcept -> decltype(auto) { return Self().DoGetPreviousRoad(); }
  [[nodiscard]] auto GetPreviousRoad() const noexcept -> decltype(auto) { return Self().DoGetPreviousRoad(); }

  auto Get() noexcept -> decltype(auto) { return Self().DoGet(); }
  [[nodiscard]] auto Get() const noexcept -> decltype(auto) { return Self().DoGet(); }

 private:
  IRoad() = default;
  friend T;

  constexpr auto Self() noexcept -> T& { return static_cast<T&>(*this); }
  constexpr auto Self() const noexcept -> const T& { return static_cast<const T&>(*this); }
};

}  // namespace crtp_type_traits_if_constexpr::interfaces

/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "common/common.hpp"

namespace crtp_type_traits_if_constexpr::interfaces {

template <typename T>
struct IMap {
  auto GetRoad(const common::RoadId identifier) noexcept -> decltype(auto) { return Self().DoGetRoad(identifier); }
  [[nodiscard]] auto GetRoad(const common::RoadId identifier) const noexcept -> decltype(auto) {
    return Self().DoGetRoad(identifier);
  }

  auto Get() noexcept -> decltype(auto) { return Self().DoGet(); }
  [[nodiscard]] auto Get() const noexcept -> decltype(auto) { return Self().DoGet(); }

 private:
  IMap() = default;
  friend T;

  constexpr auto Self() noexcept -> T& { return static_cast<T&>(*this); }
  constexpr auto Self() const noexcept -> const T& { return static_cast<const T&>(*this); }
};

}  // namespace crtp_type_traits_if_constexpr::interfaces

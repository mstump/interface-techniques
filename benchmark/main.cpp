/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include <benchmark/benchmark.h>

#include <exceptions_map.hpp>
#include <functions_map.hpp>
#include <pointers_map.hpp>

#include "common/common.hpp"
#include "crtp/adapters/exceptions_map_adapter/lane_adapter.hpp"
#include "crtp/adapters/exceptions_map_adapter/map_adapter.hpp"
#include "crtp/adapters/exceptions_map_adapter/road_adapter.hpp"
#include "crtp/adapters/functions_map_adapter/lane_adapter.hpp"
#include "crtp/adapters/functions_map_adapter/map_adapter.hpp"
#include "crtp/adapters/functions_map_adapter/road_adapter.hpp"
#include "crtp/adapters/pointers_map_adapter/lane_adapter.hpp"
#include "crtp/adapters/pointers_map_adapter/map_adapter.hpp"
#include "crtp/adapters/pointers_map_adapter/road_adapter.hpp"
#include "crtp_type_traits_if_constexpr/adapters/lane_adapter.hpp"
#include "crtp_type_traits_if_constexpr/adapters/map_adapter.hpp"
#include "crtp_type_traits_if_constexpr/adapters/road_adapter.hpp"
#include "inheritance/adapters/exceptions_map_adapter/map_adapter.hpp"
#include "inheritance/adapters/functions_map_adapter/map_adapter.hpp"
#include "inheritance/adapters/pointers_map_adapter/map_adapter.hpp"
#include "inheritance/interfaces/i_lane.hpp"
#include "inheritance/interfaces/i_map.hpp"
#include "inheritance/interfaces/i_road.hpp"
#include "template_inheritance/adapters/exceptions_map_adapter/map_adapter.hpp"
#include "template_inheritance/adapters/functions_map_adapter/map_adapter.hpp"
#include "template_inheritance/adapters/pointers_map_adapter/map_adapter.hpp"
#include "template_inheritance/interfaces/i_lane.hpp"
#include "template_inheritance/interfaces/i_map.hpp"
#include "template_inheritance/interfaces/i_road.hpp"
#include "template_inheritance/interfaces/lane.hpp"
#include "template_inheritance/interfaces/map.hpp"
#include "template_inheritance/interfaces/road.hpp"
#include "template_specialization/adapters/exceptions_map_adapter/lane_adapter.hpp"
#include "template_specialization/adapters/exceptions_map_adapter/map_adapter.hpp"
#include "template_specialization/adapters/exceptions_map_adapter/road_adapter.hpp"
#include "template_specialization/adapters/functions_map_adapter/lane_adapter.hpp"
#include "template_specialization/adapters/functions_map_adapter/map_adapter.hpp"
#include "template_specialization/adapters/functions_map_adapter/road_adapter.hpp"
#include "template_specialization/adapters/pointers_map_adapter/lane_adapter.hpp"
#include "template_specialization/adapters/pointers_map_adapter/map_adapter.hpp"
#include "template_specialization/adapters/pointers_map_adapter/road_adapter.hpp"
#include "type_erasure/adapters/exceptions_map_adapter/map_adapter.hpp"
#include "type_erasure/adapters/functions_map_adapter/map_adapter.hpp"
#include "type_erasure/adapters/pointers_map_adapter/map_adapter.hpp"
#include "type_erasure/interfaces/i_lane.hpp"
#include "type_erasure/interfaces/i_map.hpp"
#include "type_erasure/interfaces/i_road.hpp"
#include "type_traits/interfaces/i_lane.hpp"
#include "type_traits/interfaces/i_map.hpp"
#include "type_traits/interfaces/i_road.hpp"
#include "type_traits_if_constexpr/interfaces/i_lane.hpp"
#include "type_traits_if_constexpr/interfaces/i_map.hpp"
#include "type_traits_if_constexpr/interfaces/i_road.hpp"

namespace {

using common::LaneId;
using common::RoadId;

//
// direct
//
void BmDirectExceptionsMap(benchmark::State& state) {
  const auto map = exceptions_map::ExceptionsMap::Create();

  for (auto _ : state) {
    try {
      const auto& road = map.GetRoad(1);
      const auto& next_road = road.GetNextRoad();
      const auto& lane = next_road.GetLane(1);
      const auto& left_lane = lane.GetLeftLane();

      [[maybe_unused]] auto width = left_lane.GetWidth();
    } catch (...) {
      state.SkipWithError("exception thrown");
      break;
    }
  }
}

void BmDirectFunctionsMap(benchmark::State& state) {
  const auto map = functions_map::FunctionsMap::Create();

  for (auto _ : state) {
    auto road = map.GetRoad(1);
    if (!road) {
      state.SkipWithError("road is nullopt");
      break;
    }

    auto next_road = road.value().get().GetNextRoad();
    if (!next_road) {
      state.SkipWithError("next_road is nullopt");
      break;
    }

    auto lane = next_road.value().get().GetLane(1);
    if (!lane) {
      state.SkipWithError("lane is nullopt");
      break;
    }

    auto left_lane = lane.value().get().GetLeftLane();
    if (!left_lane) {
      state.SkipWithError("left_lane is nullopt");
      break;
    }

    [[maybe_unused]] auto width = left_lane.value().get().GetWidth();
  }
}

void BmDirectPointersMap(benchmark::State& state) {
  const auto map = pointers_map::PointersMap::Create();

  for (auto _ : state) {
    try {
      const auto& road = map.roads.at(1U);

      auto* next_road = road.next_road;
      if (next_road == nullptr) {
        state.SkipWithError("next_road == nullptr");
        break;
      }

      const auto& lane = next_road->lanes.at(1U);

      auto* left_lane = lane.left_lane;
      if (left_lane == nullptr) {
        state.SkipWithError("left_lane == nullptr");
        break;
      }

      [[maybe_unused]] auto width = left_lane->width;
    } catch (...) {
      state.SkipWithError("exception thrown");
      break;
    }
  }
}

//
// crtp
//
void BmCrtpExceptionsMap(benchmark::State& state) {
  auto map = exceptions_map::ExceptionsMap::Create();
  const auto map_adapter = crtp::exceptions_map_adapter::MapAdapter{map};

  for (auto _ : state) {
    [[maybe_unused]] auto width = GetNextLeftWidth(map_adapter, RoadId{1}, LaneId{1});
  }
}

void BmCrtpFunctionsMap(benchmark::State& state) {
  auto map = functions_map::FunctionsMap::Create();
  const auto map_adapter = crtp::functions_map_adapter::MapAdapter{map};

  for (auto _ : state) {
    [[maybe_unused]] auto width = GetNextLeftWidth(map_adapter, RoadId{1}, LaneId{1});
  }
}

void BmCrtpPointersMap(benchmark::State& state) {
  auto map = pointers_map::PointersMap::Create();
  const auto map_adapter = crtp::pointers_map_adapter::MapAdapter{map};

  for (auto _ : state) {
    [[maybe_unused]] auto width = GetNextLeftWidth(map_adapter, RoadId{1}, LaneId{1});
  }
}

//
// crtp_type_traits_if_constexpr
//
void BmCrtpTraitsIfConstexprExceptionsMap(benchmark::State& state) {
  auto map = exceptions_map::ExceptionsMap::Create();
  const auto map_adapter = crtp_type_traits_if_constexpr::adapters::MapAdapter{map};

  for (auto _ : state) {
    [[maybe_unused]] auto width = GetNextLeftWidth(map_adapter, RoadId{1}, LaneId{1});
  }
}

void BmCrtpTraitsIfConstexprFunctionsMap(benchmark::State& state) {
  auto map = functions_map::FunctionsMap::Create();
  const auto map_adapter = crtp_type_traits_if_constexpr::adapters::MapAdapter{map};

  for (auto _ : state) {
    [[maybe_unused]] auto width = GetNextLeftWidth(map_adapter, RoadId{1}, LaneId{1});
  }
}

void BmCrtpTraitsIfConstexprPointersMap(benchmark::State& state) {
  auto map = pointers_map::PointersMap::Create();
  const auto map_adapter = crtp_type_traits_if_constexpr::adapters::MapAdapter{map};

  for (auto _ : state) {
    [[maybe_unused]] auto width = GetNextLeftWidth(map_adapter, RoadId{1}, LaneId{1});
  }
}

//
// inheritance
//
void BmInheritanceExceptionsMap(benchmark::State& state) {
  auto map = exceptions_map::ExceptionsMap::Create();
  const auto map_adapter = inheritance::exceptions_map_adapter::MapAdapter{map};

  for (auto _ : state) {
    [[maybe_unused]] auto width = GetNextLeftWidth(map_adapter, RoadId{1}, LaneId{1});
  }
}

void BmInheritanceFunctionsMap(benchmark::State& state) {
  auto map = functions_map::FunctionsMap::Create();
  const auto map_adapter = inheritance::functions_map_adapter::MapAdapter{map};

  for (auto _ : state) {
    [[maybe_unused]] auto width = GetNextLeftWidth(map_adapter, RoadId{1}, LaneId{1});
  }
}

void BmInheritancePointersMap(benchmark::State& state) {
  auto map = pointers_map::PointersMap::Create();
  const auto map_adapter = inheritance::pointers_map_adapter::MapAdapter{map};

  for (auto _ : state) {
    [[maybe_unused]] auto width = GetNextLeftWidth(map_adapter, RoadId{1}, LaneId{1});
  }
}

//
// template_inheritance
//
void BmTemplateInheritanceExceptionsMap(benchmark::State& state) {
  auto exceptions_map = exceptions_map::ExceptionsMap::Create();
  const auto map_adapter = template_inheritance::exceptions_map_adapter::MapAdapter{exceptions_map};
  const auto map = template_inheritance::interfaces::Map{map_adapter};

  for (auto _ : state) {
    [[maybe_unused]] auto width = GetNextLeftWidth(map, RoadId{1}, LaneId{1});
  }
}

void BmTemplateInheritanceFunctionsMap(benchmark::State& state) {
  auto functions_map = functions_map::FunctionsMap::Create();
  const auto map_adapter = template_inheritance::functions_map_adapter::MapAdapter{functions_map};
  const auto map = template_inheritance::interfaces::Map{map_adapter};

  for (auto _ : state) {
    [[maybe_unused]] auto width = GetNextLeftWidth(map, RoadId{1}, LaneId{1});
  }
}

void BmTemplateInheritancePointersMap(benchmark::State& state) {
  auto pointers_map = pointers_map::PointersMap::Create();
  const auto map_adapter = template_inheritance::pointers_map_adapter::MapAdapter{pointers_map};
  const auto map = template_inheritance::interfaces::Map{map_adapter};

  for (auto _ : state) {
    [[maybe_unused]] auto width = GetNextLeftWidth(map, RoadId{1}, LaneId{1});
  }
}

//
// template_specialization
//
void BmTemplateSpecializationExceptionsMap(benchmark::State& state) {
  auto map = exceptions_map::ExceptionsMap::Create();
  const auto map_adapter = template_specialization::interfaces::IMap{map};

  for (auto _ : state) {
    [[maybe_unused]] auto width = GetNextLeftWidth(map_adapter, RoadId{1}, LaneId{1});
  }
}

void BmTemplateSpecializationFunctionsMap(benchmark::State& state) {
  auto map = functions_map::FunctionsMap::Create();
  const auto map_adapter = template_specialization::interfaces::IMap{map};

  for (auto _ : state) {
    [[maybe_unused]] auto width = GetNextLeftWidth(map_adapter, RoadId{1}, LaneId{1});
  }
}

void BmTemplateSpecializationPointersMap(benchmark::State& state) {
  auto map = pointers_map::PointersMap::Create();
  const auto map_adapter = template_specialization::interfaces::IMap{map};

  for (auto _ : state) {
    [[maybe_unused]] auto width = GetNextLeftWidth(map_adapter, RoadId{1}, LaneId{1});
  }
}

//
// type_erasure
//
void BmTypeErasureExceptionsMap(benchmark::State& state) {
  auto exceptions_map = exceptions_map::ExceptionsMap::Create();
  auto exceptions_map_adapter = type_erasure::exceptions_map_adapter::MapAdapter{exceptions_map};
  auto map = type_erasure::interfaces::IMap{exceptions_map_adapter};

  for (auto _ : state) {
    [[maybe_unused]] auto width = GetNextLeftWidth(map, RoadId{1}, LaneId{1});
  }
}

void BmTypeErasureFunctionsMap(benchmark::State& state) {
  auto functions_map = functions_map::FunctionsMap::Create();
  auto functions_map_adapter = type_erasure::functions_map_adapter::MapAdapter{functions_map};
  auto map = type_erasure::interfaces::IMap{functions_map_adapter};

  for (auto _ : state) {
    [[maybe_unused]] auto width = GetNextLeftWidth(map, RoadId{1}, LaneId{1});
  }
}

void BmTypeErasurePointersMap(benchmark::State& state) {
  auto pointers_map = pointers_map::PointersMap::Create();
  auto pointers_map_adapter = type_erasure::pointers_map_adapter::MapAdapter{pointers_map};
  auto map = type_erasure::interfaces::IMap{pointers_map_adapter};

  for (auto _ : state) {
    [[maybe_unused]] auto width = GetNextLeftWidth(map, RoadId{1}, LaneId{1});
  }
}

//
// type_traits
//
void BmTypeTraitsExceptionsMap(benchmark::State& state) {
  auto map = exceptions_map::ExceptionsMap::Create();
  const auto map_adapter = type_traits::interfaces::IMap{map};

  for (auto _ : state) {
    [[maybe_unused]] auto width = GetNextLeftWidth(map_adapter, RoadId{1}, LaneId{1});
  }
}

void BmTypeTraitsFunctionsMap(benchmark::State& state) {
  auto map = functions_map::FunctionsMap::Create();
  const auto map_adapter = type_traits::interfaces::IMap{map};

  for (auto _ : state) {
    [[maybe_unused]] auto width = GetNextLeftWidth(map_adapter, RoadId{1}, LaneId{1});
  }
}

void BmTypeTraitsPointersMap(benchmark::State& state) {
  auto map = pointers_map::PointersMap::Create();
  const auto map_adapter = type_traits::interfaces::IMap{map};

  for (auto _ : state) {
    [[maybe_unused]] auto width = GetNextLeftWidth(map_adapter, RoadId{1}, LaneId{1});
  }
}

//
// type_traits_if_constexpr
//
void BmTypeTraitsIfConstexprExceptionsMap(benchmark::State& state) {
  auto map = exceptions_map::ExceptionsMap::Create();

  for (auto _ : state) {
    const auto road = type_traits_if_constexpr::interfaces::GetRoad(map, RoadId{1});
    if (!road) {
      state.SkipWithError("road is nullopt");
      break;
    }

    const auto next_road = type_traits_if_constexpr::interfaces::GetNextRoad(road.value().get());
    if (!next_road) {
      state.SkipWithError("next_road is nullopt");
      break;
    }

    const auto lane = type_traits_if_constexpr::interfaces::GetLane(next_road.value().get(), LaneId{1});
    if (!lane) {
      state.SkipWithError("lane is nullopt");
      break;
    }

    const auto left_lane = type_traits_if_constexpr::interfaces::GetLeftLane(lane.value().get());
    if (!left_lane) {
      state.SkipWithError("left_lane is nullopt");
      break;
    }

    [[maybe_unused]] const auto width = type_traits_if_constexpr::interfaces::GetWidth(left_lane.value().get());
  }
}

void BmTypeTraitsIfConstexprFunctionsMap(benchmark::State& state) {
  auto map = functions_map::FunctionsMap::Create();

  for (auto _ : state) {
    const auto road = type_traits_if_constexpr::interfaces::GetRoad(map, RoadId{1});
    if (!road) {
      state.SkipWithError("road is nullopt");
      break;
    }

    const auto next_road = type_traits_if_constexpr::interfaces::GetNextRoad(road.value().get());
    if (!next_road) {
      state.SkipWithError("next_road is nullopt");
      break;
    }

    const auto lane = type_traits_if_constexpr::interfaces::GetLane(next_road.value().get(), LaneId{1});
    if (!lane) {
      state.SkipWithError("lane is nullopt");
      break;
    }

    const auto left_lane = type_traits_if_constexpr::interfaces::GetLeftLane(lane.value().get());
    if (!left_lane) {
      state.SkipWithError("left_lane is nullopt");
      break;
    }

    [[maybe_unused]] const auto width = type_traits_if_constexpr::interfaces::GetWidth(left_lane.value().get());
  }
}

void BmTypeTraitsIfConstexprPointersMap(benchmark::State& state) {
  auto map = pointers_map::PointersMap::Create();

  for (auto _ : state) {
    const auto road = type_traits_if_constexpr::interfaces::GetRoad(map, RoadId{1});
    if (!road) {
      state.SkipWithError("road is nullopt");
      break;
    }

    const auto next_road = type_traits_if_constexpr::interfaces::GetNextRoad(road.value().get());
    if (!next_road) {
      state.SkipWithError("next_road is nullopt");
      break;
    }

    const auto lane = type_traits_if_constexpr::interfaces::GetLane(next_road.value().get(), LaneId{1});
    if (!lane) {
      state.SkipWithError("lane is nullopt");
      break;
    }

    const auto left_lane = type_traits_if_constexpr::interfaces::GetLeftLane(lane.value().get());
    if (!left_lane) {
      state.SkipWithError("left_lane is nullopt");
      break;
    }

    [[maybe_unused]] const auto width = type_traits_if_constexpr::interfaces::GetWidth(left_lane.value().get());
  }
}

}  // namespace

BENCHMARK(BmDirectExceptionsMap);  // NOLINT
BENCHMARK(BmDirectFunctionsMap);   // NOLINT
BENCHMARK(BmDirectPointersMap);    // NOLINT

BENCHMARK(BmCrtpTraitsIfConstexprExceptionsMap);  // NOLINT
BENCHMARK(BmCrtpTraitsIfConstexprFunctionsMap);   // NOLINT
BENCHMARK(BmCrtpTraitsIfConstexprPointersMap);    // NOLINT

BENCHMARK(BmCrtpExceptionsMap);  // NOLINT
BENCHMARK(BmCrtpFunctionsMap);   // NOLINT
BENCHMARK(BmCrtpPointersMap);    // NOLINT

BENCHMARK(BmInheritanceExceptionsMap);  // NOLINT
BENCHMARK(BmInheritanceFunctionsMap);   // NOLINT
BENCHMARK(BmInheritancePointersMap);    // NOLINT

BENCHMARK(BmTemplateInheritanceExceptionsMap);  // NOLINT
BENCHMARK(BmTemplateInheritanceFunctionsMap);   // NOLINT
BENCHMARK(BmTemplateInheritancePointersMap);    // NOLINT

BENCHMARK(BmTemplateSpecializationExceptionsMap);  // NOLINT
BENCHMARK(BmTemplateSpecializationFunctionsMap);   // NOLINT
BENCHMARK(BmTemplateSpecializationPointersMap);    // NOLINT

BENCHMARK(BmTypeErasureExceptionsMap);  // NOLINT
BENCHMARK(BmTypeErasureFunctionsMap);   // NOLINT
BENCHMARK(BmTypeErasurePointersMap);    // NOLINT

BENCHMARK(BmTypeTraitsExceptionsMap);  // NOLINT
BENCHMARK(BmTypeTraitsFunctionsMap);   // NOLINT
BENCHMARK(BmTypeTraitsPointersMap);    // NOLINT

BENCHMARK(BmTypeTraitsIfConstexprExceptionsMap);  // NOLINT
BENCHMARK(BmTypeTraitsIfConstexprFunctionsMap);   // NOLINT
BENCHMARK(BmTypeTraitsIfConstexprPointersMap);    // NOLINT

BENCHMARK_MAIN();  // NOLINT

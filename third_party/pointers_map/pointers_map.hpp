/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <vector>  // for std::vector

#include "pointers_road.hpp"

namespace pointers_map {

struct PointersMap {
  static auto Create() -> PointersMap {
    auto map = PointersMap{};
    map.roads.reserve(3);

    auto& road0 = map.roads.emplace_back();
    road0.lanes.reserve(3);
    {
      auto& lane0 = road0.lanes.emplace_back();
      auto& lane1 = road0.lanes.emplace_back();
      auto& lane2 = road0.lanes.emplace_back();

      lane0.left_lane = &lane1;
      lane1.right_lane = &lane0;
      lane1.left_lane = &lane2;
      lane2.right_lane = &lane1;
    }

    auto& road1 = map.roads.emplace_back();
    road1.lanes.reserve(3);
    {
      auto& lane0 = road1.lanes.emplace_back();
      auto& lane1 = road1.lanes.emplace_back();
      auto& lane2 = road1.lanes.emplace_back();

      lane0.left_lane = &lane1;
      lane1.right_lane = &lane0;
      lane1.left_lane = &lane2;
      lane2.right_lane = &lane1;
    }

    auto& road2 = map.roads.emplace_back();
    road2.lanes.reserve(3);
    {
      auto& lane0 = road2.lanes.emplace_back();
      auto& lane1 = road2.lanes.emplace_back();
      auto& lane2 = road2.lanes.emplace_back();

      lane0.left_lane = &lane1;
      lane1.right_lane = &lane0;
      lane1.left_lane = &lane2;
      lane2.right_lane = &lane1;
    }

    road0.next_road = &road1;
    road1.previous_road = &road0;
    road1.next_road = &road2;
    road2.previous_road = &road1;

    return map;
  };

  std::vector<PointersRoad> roads;
};

}  // namespace pointers_map

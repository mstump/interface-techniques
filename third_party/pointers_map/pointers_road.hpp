/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <vector>  // for std::vector

#include "pointers_lane.hpp"

namespace pointers_map {

struct PointersRoad {
  std::vector<PointersLane> lanes;
  PointersRoad* next_road{};
  PointersRoad* previous_road{};
};

}  // namespace pointers_map

/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

namespace pointers_map {

struct PointersLane {
  volatile double width{1.0};
  PointersLane* left_lane{};
  PointersLane* right_lane{};
};

}  // namespace pointers_map

/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "exceptions_road.hpp"

#include <cstddef>    // for std::size_t
#include <stdexcept>  // for std::runtime_error
#include <vector>     // for std::vector

#include "exceptions_lane.hpp"

namespace exceptions_map {

auto ExceptionsRoad::GetLane(int identifier) -> ExceptionsLane& {
  return lanes_.at(static_cast<std::size_t>(identifier));
}

auto ExceptionsRoad::GetLane(int identifier) const -> const ExceptionsLane& {
  return lanes_.at(static_cast<std::size_t>(identifier));
}

auto ExceptionsRoad::GetNextRoad() -> ExceptionsRoad& {
  if (next_road_ == nullptr) {
    throw std::runtime_error("Next road is not available");
  }

  return *next_road_;
}

auto ExceptionsRoad::GetNextRoad() const -> const ExceptionsRoad& {
  if (next_road_ == nullptr) {
    throw std::runtime_error("Next road is not available");
  }

  return *next_road_;
}

auto ExceptionsRoad::GetPreviousRoad() -> ExceptionsRoad& {
  if (previous_road_ == nullptr) {
    throw std::runtime_error("Previous road is not available");
  }

  return *previous_road_;
}

[[nodiscard]] auto ExceptionsRoad::GetPreviousRoad() const -> const ExceptionsRoad& {
  if (previous_road_ == nullptr) {
    throw std::runtime_error("Previous road is not available");
  }

  return *previous_road_;
}

}  // namespace exceptions_map

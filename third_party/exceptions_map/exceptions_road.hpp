/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <vector>  // for std::vector

#include "exceptions_lane.hpp"

namespace exceptions_map {

class ExceptionsRoad {
  friend class ExceptionsMap;

 public:
  auto GetLane(int identifier) -> ExceptionsLane&;
  [[nodiscard]] auto GetLane(int identifier) const -> const ExceptionsLane&;

  auto GetNextRoad() -> ExceptionsRoad&;
  [[nodiscard]] auto GetNextRoad() const -> const ExceptionsRoad&;

  auto GetPreviousRoad() -> ExceptionsRoad&;
  [[nodiscard]] auto GetPreviousRoad() const -> const ExceptionsRoad&;

 private:
  std::vector<ExceptionsLane> lanes_;
  ExceptionsRoad* next_road_{};
  ExceptionsRoad* previous_road_{};
};

}  // namespace exceptions_map

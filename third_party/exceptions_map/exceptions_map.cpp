/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "exceptions_map.hpp"

#include "exceptions_road.hpp"

namespace exceptions_map {

auto ExceptionsMap::GetRoad(int identifier) -> ExceptionsRoad& { return roads_.at(identifier); }

auto ExceptionsMap::GetRoad(int identifier) const -> const ExceptionsRoad& { return roads_.at(identifier); }

}  // namespace exceptions_map

/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

namespace exceptions_map {

class ExceptionsLane {
  friend class ExceptionsMap;

 public:
  [[nodiscard]] auto GetWidth() const noexcept -> double;

  auto GetLeftLane() -> ExceptionsLane&;
  [[nodiscard]] auto GetLeftLane() const -> const ExceptionsLane&;

  auto GetRightLane() -> ExceptionsLane&;
  [[nodiscard]] auto GetRightLane() const -> const ExceptionsLane&;

 private:
  volatile double width_{1.0};
  ExceptionsLane* left_lane_{};
  ExceptionsLane* right_lane_{};
};

}  // namespace exceptions_map

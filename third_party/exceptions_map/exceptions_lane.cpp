/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "exceptions_lane.hpp"

#include <stdexcept>  // for std::runtime_error

namespace exceptions_map {

auto ExceptionsLane::GetWidth() const noexcept -> double { return width_; }

auto ExceptionsLane::GetLeftLane() -> ExceptionsLane& {
  if (left_lane_ == nullptr) {
    throw std::runtime_error("Left lane is not available");
  }

  return *left_lane_;
}

auto ExceptionsLane::GetLeftLane() const -> const ExceptionsLane& {
  if (left_lane_ == nullptr) {
    throw std::runtime_error("Left lane is not available");
  }

  return *left_lane_;
}

auto ExceptionsLane::GetRightLane() -> ExceptionsLane& {
  if (right_lane_ == nullptr) {
    throw std::runtime_error("Right lane is not available");
  }

  return *right_lane_;
}

auto ExceptionsLane::GetRightLane() const -> const ExceptionsLane& {
  if (right_lane_ == nullptr) {
    throw std::runtime_error("Right lane is not available");
  }

  return *right_lane_;
}

}  // namespace exceptions_map

/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "functions_map.hpp"

#include <functional>  // for std::reference_wrapper
#include <optional>    // for std::optional
#include <vector>      // for std::vector

#include "functions_road.hpp"

namespace functions_map {

auto FunctionsMap::GetRoad(int identifier) noexcept -> std::optional<std::reference_wrapper<FunctionsRoad>> {
  try {
    return roads_.at(static_cast<std::size_t>(identifier));
  } catch (...) {
    return std::nullopt;
  }
}

auto FunctionsMap::GetRoad(int identifier) const noexcept
    -> std::optional<std::reference_wrapper<const FunctionsRoad>> {
  try {
    return roads_.at(static_cast<std::size_t>(identifier));
  } catch (...) {
    return std::nullopt;
  }
}

}  // namespace functions_map

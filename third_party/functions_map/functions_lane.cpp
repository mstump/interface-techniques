/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "functions_lane.hpp"

#include <functional>  // for std::reference_wrapper
#include <optional>    // for std::optional

namespace functions_map {

auto FunctionsLane::GetWidth() const noexcept -> double { return width_; }

auto FunctionsLane::GetLeftLane() noexcept -> std::optional<std::reference_wrapper<FunctionsLane>> {
  return left_lane_;
}

auto FunctionsLane::GetLeftLane() const noexcept -> std::optional<std::reference_wrapper<const FunctionsLane>> {
  return left_lane_;
}

auto FunctionsLane::GetRightLane() noexcept -> std::optional<std::reference_wrapper<FunctionsLane>> {
  return right_lane_;
}

auto FunctionsLane::GetRightLane() const noexcept -> std::optional<std::reference_wrapper<const FunctionsLane>> {
  return right_lane_;
}

}  // namespace functions_map

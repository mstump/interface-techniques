/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>  // for std::reference_wrapper
#include <optional>    // for std::optional

namespace functions_map {

class FunctionsLane {
  friend class FunctionsMap;

 public:
  [[nodiscard]] auto GetWidth() const noexcept -> double;

  auto GetLeftLane() noexcept -> std::optional<std::reference_wrapper<FunctionsLane>>;
  [[nodiscard]] auto GetLeftLane() const noexcept -> std::optional<std::reference_wrapper<const FunctionsLane>>;

  auto GetRightLane() noexcept -> std::optional<std::reference_wrapper<FunctionsLane>>;
  [[nodiscard]] auto GetRightLane() const noexcept -> std::optional<std::reference_wrapper<const FunctionsLane>>;

 private:
  volatile double width_{1.0};
  std::optional<std::reference_wrapper<FunctionsLane>> left_lane_;
  std::optional<std::reference_wrapper<FunctionsLane>> right_lane_;
};

}  // namespace functions_map

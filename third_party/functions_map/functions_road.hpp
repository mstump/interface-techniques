/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>  // for std::reference_wrapper
#include <optional>    // for std::optional
#include <vector>      // for std::vector

#include "functions_lane.hpp"

namespace functions_map {

class FunctionsRoad {
  friend class FunctionsMap;

 public:
  auto GetLane(int identifier) noexcept -> std::optional<std::reference_wrapper<FunctionsLane>>;
  [[nodiscard]] auto GetLane(int identifier) const noexcept
      -> std::optional<std::reference_wrapper<const FunctionsLane>>;

  auto GetNextRoad() noexcept -> std::optional<std::reference_wrapper<FunctionsRoad>>;
  [[nodiscard]] auto GetNextRoad() const noexcept -> std::optional<std::reference_wrapper<const FunctionsRoad>>;

  auto GetPreviousRoad() noexcept -> std::optional<std::reference_wrapper<FunctionsRoad>>;
  [[nodiscard]] auto GetPreviousRoad() const noexcept -> std::optional<std::reference_wrapper<const FunctionsRoad>>;

 private:
  std::vector<FunctionsLane> lanes_;
  std::optional<std::reference_wrapper<FunctionsRoad>> next_road_;
  std::optional<std::reference_wrapper<FunctionsRoad>> previous_road_;
};

}  // namespace functions_map

/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "functions_road.hpp"

#include <cstddef>     // for std::size_t
#include <functional>  // for std::reference_wrapper
#include <optional>    // for std::optional

#include "functions_lane.hpp"

namespace functions_map {

auto FunctionsRoad::GetLane(int identifier) noexcept -> std::optional<std::reference_wrapper<FunctionsLane>> {
  try {
    return lanes_.at(static_cast<std::size_t>(identifier));
  } catch (...) {
    return std::nullopt;
  }
}

auto FunctionsRoad::GetLane(int identifier) const noexcept
    -> std::optional<std::reference_wrapper<const FunctionsLane>> {
  try {
    return lanes_.at(static_cast<std::size_t>(identifier));
  } catch (...) {
    return std::nullopt;
  }
}

auto FunctionsRoad::GetNextRoad() noexcept -> std::optional<std::reference_wrapper<FunctionsRoad>> {
  return next_road_;
}

auto FunctionsRoad::GetNextRoad() const noexcept -> std::optional<std::reference_wrapper<const FunctionsRoad>> {
  return next_road_;
}

auto FunctionsRoad::GetPreviousRoad() noexcept -> std::optional<std::reference_wrapper<FunctionsRoad>> {
  return previous_road_;
}

auto FunctionsRoad::GetPreviousRoad() const noexcept -> std::optional<std::reference_wrapper<const FunctionsRoad>> {
  return previous_road_;
}

}  // namespace functions_map

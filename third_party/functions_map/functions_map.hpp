/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>  // for std::reference_wrapper
#include <optional>    // for std::optional
#include <vector>      // for std::vector

#include "functions_road.hpp"

namespace functions_map {

class FunctionsMap {
 public:
  static auto Create() -> FunctionsMap {
    auto map = FunctionsMap{};
    map.roads_.reserve(3);

    auto& road0 = map.roads_.emplace_back();
    road0.lanes_.reserve(3);
    {
      auto& lane0 = road0.lanes_.emplace_back();
      auto& lane1 = road0.lanes_.emplace_back();
      auto& lane2 = road0.lanes_.emplace_back();

      lane0.left_lane_ = lane1;
      lane1.right_lane_ = lane0;
      lane1.left_lane_ = lane2;
      lane2.right_lane_ = lane1;
    }

    auto& road1 = map.roads_.emplace_back();
    road1.lanes_.reserve(3);
    {
      auto& lane0 = road1.lanes_.emplace_back();
      auto& lane1 = road1.lanes_.emplace_back();
      auto& lane2 = road1.lanes_.emplace_back();

      lane0.left_lane_ = lane1;
      lane1.right_lane_ = lane0;
      lane1.left_lane_ = lane2;
      lane2.right_lane_ = lane1;
    }

    auto& road2 = map.roads_.emplace_back();
    road2.lanes_.reserve(3);
    {
      auto& lane0 = road2.lanes_.emplace_back();
      auto& lane1 = road2.lanes_.emplace_back();
      auto& lane2 = road2.lanes_.emplace_back();

      lane0.left_lane_ = lane1;
      lane1.right_lane_ = lane0;
      lane1.left_lane_ = lane2;
      lane2.right_lane_ = lane1;
    }

    road0.next_road_ = road1;
    road1.previous_road_ = road0;
    road1.next_road_ = road2;
    road2.previous_road_ = road1;

    return map;
  };

  auto GetRoad(int identifier) noexcept -> std::optional<std::reference_wrapper<FunctionsRoad>>;
  [[nodiscard]] auto GetRoad(int identifier) const noexcept
      -> std::optional<std::reference_wrapper<const FunctionsRoad>>;

 private:
  std::vector<FunctionsRoad> roads_;
};

}  // namespace functions_map

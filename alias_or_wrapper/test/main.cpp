/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include <exceptions_lane.hpp>
#include <exceptions_map.hpp>
#include <exceptions_road.hpp>
#include <functions_lane.hpp>
#include <functions_map.hpp>
#include <functions_road.hpp>
#include <pointers_map.hpp>

#include "alias_or_wrapper/interfaces/i_lane.hpp"
#include "alias_or_wrapper/interfaces/i_map.hpp"
#include "alias_or_wrapper/interfaces/i_road.hpp"
#include "common/common.hpp"

auto main() -> int {
  using common::LaneId;
  using common::RoadId;

  auto width{0.0};

  {
    using Lane = alias_or_wrapper::interfaces::Lane<exceptions_map::ExceptionsLane>;
    using Map = alias_or_wrapper::interfaces::Map<exceptions_map::ExceptionsMap>;
    using Road = alias_or_wrapper::interfaces::Road<exceptions_map::ExceptionsRoad>;

    auto exceptions_map = exceptions_map::ExceptionsMap::Create();
    auto exceptions_map_adapter = Map{exceptions_map};

    static_assert(std::is_same_v<std::decay_t<decltype(exceptions_map_adapter)>, exceptions_map::ExceptionsMap>);

    auto road = Road{exceptions_map_adapter.GetRoad(1)};
    auto next_road = Road{road.GetNextRoad()};
    auto lane = Lane{road.GetLane(1)};
    auto left_lane = Lane{lane.GetLeftLane()};

    width += left_lane.GetWidth();
  }

  {
    auto functions_map = functions_map::FunctionsMap::Create();
    auto functions_map_adapter = alias_or_wrapper::interfaces::Map<functions_map::FunctionsMap>{functions_map};

    static_assert(std::is_same_v<std::decay_t<decltype(functions_map_adapter)>,
                                 template_specialization::interfaces::IMap<functions_map::FunctionsMap>>);

    width += GetNextLeftWidth(functions_map_adapter, RoadId{1}, LaneId{1});
  }

  {
    [[maybe_unused]] auto pointers_map = pointers_map::PointersMap::Create();
    // auto pointers_map_adapter = alias_or_wrapper::interfaces::Map<pointers_map::PointersMap>{pointers_map};

    // static_assert(std::is_same_v<std::decay_t<decltype(pointers_map_adapter)>,
    //                              template_specialization::interfaces::IMap<pointers_map::PointersMap>>);

    // width += GetNextLeftWidth(pointers_map_adapter, RoadId{1}, LaneId{1});
  }

  constexpr auto kExpectedWidth{3.0};
  return width == kExpectedWidth ? 0 : 1;
}

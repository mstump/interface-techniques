# ##############################################################################
# Copyright (c) 2023 Mercedes-Benz Tech Innovation GmbH
#
# This program and the accompanying materials are made available under the terms
# of the Eclipse Public License 2.0 which is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# ##############################################################################

add_executable(alias_or_wrapper_test)
add_executable(alias_or_wrapper::test ALIAS alias_or_wrapper_test)
target_sources(alias_or_wrapper_test PRIVATE main.cpp)
target_link_libraries(
  alias_or_wrapper_test
  PRIVATE alias_or_wrapper::interfaces exceptions_map::map functions_map::map
          pointers_map::map)
add_test(NAME alias_or_wrapper_test COMMAND alias_or_wrapper_test)

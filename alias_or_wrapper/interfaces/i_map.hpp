/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <type_traits>
#include <utility>

#include "alias_or_wrapper/interfaces/map_traits.hpp"
#include "template_specialization/adapters/exceptions_map_adapter/map_adapter.hpp"
#include "template_specialization/adapters/functions_map_adapter/map_adapter.hpp"
#include "template_specialization/adapters/pointers_map_adapter/map_adapter.hpp"
#include "template_specialization/interfaces/i_map.hpp"

namespace alias_or_wrapper::interfaces {

// GetRoad
template <typename MapType>
using GetRoad_t = decltype(std::declval<MapType>().GetRoad(std::declval<int>()));

template <typename MapType, typename = std::void_t<>>
struct has_GetRoad : std::false_type {};

template <typename MapType>
struct has_GetRoad<MapType, std::void_t<GetRoad_t<MapType>>> : std::true_type {};

template <typename MapType>
inline constexpr bool has_GetRoad_v = has_GetRoad<MapType>::value && std::is_reference_v<GetRoad_t<MapType>>;

// IsCompleteMap
template <typename MapType>
inline constexpr bool is_complete_map_v = has_GetRoad_v<MapType>;

template <typename MapType>
using Map = std::conditional_t<is_complete_map_v<MapType>, MapType, template_specialization::interfaces::IMap<MapType>>;

}  // namespace alias_or_wrapper::interfaces

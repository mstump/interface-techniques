/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>
#include <optional>
#include <type_traits>
#include <vector>

template <typename T>
constexpr auto always_false_v = false;

template <typename T>
constexpr auto is_optional_reference_wrapper_v =
    std::is_class_v<T> && std::is_same_v<std::optional<std::reference_wrapper<typename T::value_type::type>>, T>;

template <typename T>
constexpr auto is_vector_v = std::is_same_v<std::vector<typename T::value_type>, T>;

/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <type_traits>
#include <utility>

#include "alias_or_wrapper/interfaces/map_traits.hpp"
#include "template_specialization/adapters/exceptions_map_adapter/lane_adapter.hpp"
#include "template_specialization/adapters/functions_map_adapter/lane_adapter.hpp"
#include "template_specialization/adapters/pointers_map_adapter/lane_adapter.hpp"
#include "template_specialization/interfaces/i_lane.hpp"

namespace alias_or_wrapper::interfaces {

// GetWidth
template <typename LaneType>
using GetWidth_t = decltype(std::declval<LaneType>().GetWidth());

template <typename LaneType, typename = std::void_t<>>
struct has_GetWidth : std::false_type {};

template <typename LaneType>
struct has_GetWidth<LaneType, std::void_t<GetWidth_t<LaneType>>> : std::true_type {};

template <typename LaneType>
inline constexpr bool has_GetWidth_v = has_GetWidth<LaneType>::value && std::is_floating_point_v<GetWidth_t<LaneType>>;

// GetLeftLane
template <typename LaneType>
using GetLeftLane_t = decltype(std::declval<LaneType>().GetLeftLane());

template <typename LaneType, typename = std::void_t<>>
struct has_GetLeftLane : std::false_type {};

template <typename LaneType>
struct has_GetLeftLane<LaneType, std::void_t<GetLeftLane_t<LaneType>>> : std::true_type {};

template <typename LaneType>
inline constexpr bool has_GetLeftLane_v =
    has_GetLeftLane<LaneType>::value && std::is_reference_v<GetLeftLane_t<LaneType>>;

// GetRightLane
template <typename LaneType>
using GetRightLane_t = decltype(std::declval<LaneType>().GetRightLane());

template <typename LaneType, typename = std::void_t<>>
struct has_GetRightLane : std::false_type {};

template <typename LaneType>
struct has_GetRightLane<LaneType, std::void_t<GetRightLane_t<LaneType>>> : std::true_type {};

template <typename LaneType>
inline constexpr bool has_GetRightLane_v =
    has_GetRightLane<LaneType>::value && std::is_reference_v<GetRightLane_t<LaneType>>;

// IsCompleteLane
template <typename LaneType>
inline constexpr bool is_complete_lane_v =
    has_GetWidth_v<LaneType> && has_GetLeftLane_v<LaneType> && has_GetRightLane_v<LaneType>;

template <typename LaneType>
using Lane =
    std::conditional_t<is_complete_lane_v<LaneType>, LaneType, template_specialization::interfaces::ILane<LaneType>>;

}  // namespace alias_or_wrapper::interfaces

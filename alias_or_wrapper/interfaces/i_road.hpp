/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <type_traits>
#include <utility>

#include "alias_or_wrapper/interfaces/map_traits.hpp"
#include "template_specialization/adapters/exceptions_map_adapter/road_adapter.hpp"
#include "template_specialization/adapters/functions_map_adapter/road_adapter.hpp"
#include "template_specialization/adapters/pointers_map_adapter/road_adapter.hpp"
#include "template_specialization/interfaces/i_road.hpp"

namespace alias_or_wrapper::interfaces {

// GetLane
template <typename RoadType>
using GetLane_t = decltype(std::declval<RoadType>().GetLane(std::declval<int>()));

template <typename RoadType, typename = std::void_t<>>
struct has_GetLane : std::false_type {};

template <typename RoadType>
struct has_GetLane<RoadType, std::void_t<GetLane_t<RoadType>>> : std::true_type {};

template <typename RoadType>
inline constexpr bool has_GetLane_v = has_GetLane<RoadType>::value && std::is_reference_v<GetLane_t<RoadType>>;

// GetNextRoad
template <typename RoadType>
using GetNextRoad_t = decltype(std::declval<RoadType>().GetNextRoad());

template <typename RoadType, typename = std::void_t<>>
struct has_GetNextRoad : std::false_type {};

template <typename RoadType>
struct has_GetNextRoad<RoadType, std::void_t<GetNextRoad_t<RoadType>>> : std::true_type {};

template <typename RoadType>
inline constexpr bool has_GetNextRoad_v =
    has_GetNextRoad<RoadType>::value && std::is_reference_v<GetNextRoad_t<RoadType>>;

// GetPreviousRoad
template <typename RoadType>
using GetPreviousRoad_t = decltype(std::declval<RoadType>().GetPreviousRoad());

template <typename RoadType, typename = std::void_t<>>
struct has_GetPreviousRoad : std::false_type {};

template <typename RoadType>
struct has_GetPreviousRoad<RoadType, std::void_t<GetPreviousRoad_t<RoadType>>> : std::true_type {};

template <typename RoadType>
inline constexpr bool has_GetPreviousRoad_v =
    has_GetPreviousRoad<RoadType>::value && std::is_reference_v<GetPreviousRoad_t<RoadType>>;

// IsCompleteRoad
template <typename RoadType>
inline constexpr bool is_complete_road_v =
    has_GetLane_v<RoadType> && has_GetNextRoad_v<RoadType> && has_GetPreviousRoad_v<RoadType>;

template <typename RoadType>
using Road =
    std::conditional_t<is_complete_road_v<RoadType>, RoadType, template_specialization::interfaces::IRoad<RoadType>>;

}  // namespace alias_or_wrapper::interfaces

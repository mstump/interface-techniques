# Metrics

## Lines of Code

| variant                                            | interfaces | adapters |
|----------------------------------------------------|------------|----------|
| alias_or_wrapper + template_specialization __WIP__ | 118 + 71   | 379      |
| crtp                                               | 58         | 361      |
| crtp_type_traits_if_constexpr                      | 58         | 531      |
| inheritance                                        | 40         | 387      |
| template_inheritance                               | 174        | 380      |
| template_specialization                            | 71         | 379      |
| type_traits                                        | 525        | -        |
| type_traits_if_constexpr                           | 387        | -        |

## Benchmarks

* gcc 10.3.0 `-O3 -flto=auto -fno-fat-lto-objects`, GNU ld 2.34, static libs

```
--------------------------------------------------------------------------------
Benchmark                                      Time             CPU   Iterations
--------------------------------------------------------------------------------
BmDirectExceptionsMap                       1.03 ns         1.03 ns    670036985
BmDirectFunctionsMap                       0.832 ns        0.832 ns    839652347
BmDirectPointersMap                        0.608 ns        0.608 ns   1000000000
BmCrtpTraitsIfConstexprExceptionsMap        1.30 ns         1.30 ns    540977333
BmCrtpTraitsIfConstexprFunctionsMap         1.55 ns         1.55 ns    452202643
BmCrtpTraitsIfConstexprPointersMap          1.30 ns         1.30 ns    539916855
BmCrtpExceptionsMap                         1.30 ns         1.30 ns    539740458
BmCrtpFunctionsMap                          1.29 ns         1.29 ns    541681732
BmCrtpPointersMap                           1.30 ns         1.30 ns    530772301
BmInheritanceExceptionsMap                  55.4 ns         55.4 ns     12592157
BmInheritanceFunctionsMap                   56.2 ns         56.2 ns     12362362
BmInheritancePointersMap                    55.5 ns         55.5 ns     12515122
BmTemplateInheritanceExceptionsMap          56.3 ns         56.3 ns     12510388
BmTemplateInheritanceFunctionsMap           56.2 ns         56.2 ns     12422677
BmTemplateInheritancePointersMap            55.6 ns         55.6 ns     12478059
BmTemplateSpecializationExceptionsMap       1.29 ns         1.29 ns    540315444
BmTemplateSpecializationFunctionsMap        1.29 ns         1.29 ns    542160961
BmTemplateSpecializationPointersMap         1.29 ns         1.29 ns    541110445
BmTypeTraitsExceptionsMap                   1.29 ns         1.29 ns    541875018
BmTypeTraitsFunctionsMap                    1.55 ns         1.55 ns    450159194
BmTypeTraitsPointersMap                     1.29 ns         1.29 ns    540970146
BmTypeTraitsIfConstexprExceptionsMap       0.775 ns        0.775 ns    900863834
BmTypeTraitsIfConstexprFunctionsMap        0.947 ns        0.947 ns    737587620
BmTypeTraitsIfConstexprPointersMap         0.776 ns        0.776 ns    903810239
```

* gcc 10.3.0 `-O3 -flto=auto -fno-fat-lto-objects`, GNU ld 2.34, shared libs

```
--------------------------------------------------------------------------------
Benchmark                                      Time             CPU   Iterations
--------------------------------------------------------------------------------
BmDirectExceptionsMap                       8.29 ns         8.29 ns     83853867
BmDirectFunctionsMap                        24.2 ns         24.2 ns     28944228
BmDirectPointersMap                         1.05 ns         1.05 ns    503725670
BmCrtpTraitsIfConstexprExceptionsMap        8.00 ns         8.00 ns     74894728
BmCrtpTraitsIfConstexprFunctionsMap         24.1 ns         24.1 ns     29086241
BmCrtpTraitsIfConstexprPointersMap          1.03 ns         1.03 ns    677221466
BmCrtpExceptionsMap                         8.18 ns         8.18 ns     84893999
BmCrtpFunctionsMap                          14.5 ns         14.5 ns     48110257
BmCrtpPointersMap                           1.55 ns         1.55 ns    451030001
BmInheritanceExceptionsMap                  62.4 ns         62.4 ns     11238024
BmInheritanceFunctionsMap                   64.4 ns         64.4 ns     10830744
BmInheritancePointersMap                    56.6 ns         56.6 ns     12447520
BmTemplateInheritanceExceptionsMap          62.5 ns         62.4 ns     11149490
BmTemplateInheritanceFunctionsMap           65.4 ns         65.4 ns     10818082
BmTemplateInheritancePointersMap            56.7 ns         56.7 ns     12461585
BmTemplateSpecializationExceptionsMap       8.37 ns         8.37 ns     84350159
BmTemplateSpecializationFunctionsMap        14.7 ns         14.7 ns     47588517
BmTemplateSpecializationPointersMap         1.05 ns         1.05 ns    669113602
BmTypeTraitsExceptionsMap                   8.39 ns         8.39 ns     83755078
BmTypeTraitsFunctionsMap                    24.1 ns         24.1 ns     28980746
BmTypeTraitsPointersMap                     1.58 ns         1.58 ns    450778936
BmTypeTraitsIfConstexprExceptionsMap        8.45 ns         8.45 ns     84106083
BmTypeTraitsIfConstexprFunctionsMap         24.9 ns         24.9 ns     28179071
BmTypeTraitsIfConstexprPointersMap          1.30 ns         1.30 ns    540919121
```

* GCC 10.3.0 `-O3 -flto=auto -fno-fat-lto-objects`, GNU gold 1.16, static libs

```
BmDirectExceptionsMap                       1.04 ns         1.04 ns    670512175
BmDirectFunctionsMap                       0.832 ns        0.832 ns    839418548
BmDirectPointersMap                        0.605 ns        0.605 ns   1000000000
BmCrtpTraitsIfConstexprExceptionsMap        1.30 ns         1.30 ns    539791156
BmCrtpTraitsIfConstexprFunctionsMap         1.55 ns         1.55 ns    450019445
BmCrtpTraitsIfConstexprPointersMap          1.30 ns         1.30 ns    541902264
BmCrtpExceptionsMap                         1.29 ns         1.29 ns    540184786
BmCrtpFunctionsMap                          1.29 ns         1.29 ns    541856826
BmCrtpPointersMap                           1.29 ns         1.29 ns    541106638
BmInheritanceExceptionsMap                  56.7 ns         56.7 ns     12235198
BmInheritanceFunctionsMap                   56.5 ns         56.5 ns     12122883
BmInheritancePointersMap                    56.2 ns         56.2 ns     12459168
BmTemplateInheritanceExceptionsMap          56.6 ns         56.6 ns     12272455
BmTemplateInheritanceFunctionsMap           56.4 ns         56.4 ns     12395804
BmTemplateInheritancePointersMap            55.8 ns         55.8 ns     12555891
BmTemplateSpecializationExceptionsMap       1.29 ns         1.29 ns    542185510
BmTemplateSpecializationFunctionsMap        1.29 ns         1.29 ns    541540768
BmTemplateSpecializationPointersMap         1.29 ns         1.29 ns    540895188
BmTypeTraitsExceptionsMap                   1.29 ns         1.29 ns    536294274
BmTypeTraitsFunctionsMap                    1.55 ns         1.55 ns    452463886
BmTypeTraitsPointersMap                     1.29 ns         1.29 ns    542059576
BmTypeTraitsIfConstexprExceptionsMap       0.775 ns        0.775 ns    903106794
BmTypeTraitsIfConstexprFunctionsMap        0.947 ns        0.947 ns    737927488
BmTypeTraitsIfConstexprPointersMap         0.775 ns        0.775 ns    902456995
```

* GCC 10.3.0 `-O3 -flto=auto -fno-fat-lto-objects`, GNU gold 1.16, shared libs

```
--------------------------------------------------------------------------------
Benchmark                                      Time             CPU   Iterations
--------------------------------------------------------------------------------
BmDirectExceptionsMap                       7.57 ns         7.56 ns     92138944
BmDirectFunctionsMap                        24.1 ns         24.1 ns     28725642
BmDirectPointersMap                        0.608 ns        0.607 ns   1000000000
BmCrtpTraitsIfConstexprExceptionsMap        8.07 ns         8.06 ns     87128575
BmCrtpTraitsIfConstexprFunctionsMap         24.2 ns         24.2 ns     28965016
BmCrtpTraitsIfConstexprPointersMap          1.55 ns         1.55 ns    451136939
BmCrtpExceptionsMap                         8.48 ns         8.47 ns     82428832
BmCrtpFunctionsMap                          14.7 ns         14.7 ns     47577970
BmCrtpPointersMap                           1.03 ns         1.03 ns    677277660
BmInheritanceExceptionsMap                  62.4 ns         62.4 ns     11105341
BmInheritanceFunctionsMap                   64.6 ns         64.6 ns     10809093
BmInheritancePointersMap                    56.0 ns         55.9 ns     12506876
BmTemplateInheritanceExceptionsMap          63.7 ns         63.6 ns     10990921
BmTemplateInheritanceFunctionsMap           63.1 ns         63.1 ns     11057939
BmTemplateInheritancePointersMap            56.1 ns         56.0 ns     12441872
BmTemplateSpecializationExceptionsMap       8.17 ns         8.16 ns     85299812
BmTemplateSpecializationFunctionsMap        14.5 ns         14.5 ns     48347047
BmTemplateSpecializationPointersMap         1.56 ns         1.56 ns    451526810
BmTypeTraitsExceptionsMap                   7.60 ns         7.59 ns     92033098
BmTypeTraitsFunctionsMap                    24.4 ns         24.4 ns     28821195
BmTypeTraitsPointersMap                     1.03 ns         1.03 ns    678306615
BmTypeTraitsIfConstexprExceptionsMap        7.56 ns         7.56 ns     92435010
BmTypeTraitsIfConstexprFunctionsMap         24.9 ns         24.8 ns     28169407
BmTypeTraitsIfConstexprPointersMap          1.30 ns         1.30 ns    538723399
```

* clang 14.0.6 `-O3 -flto=thin`, LLD 14.0.6, static libs

```
--------------------------------------------------------------------------------
Benchmark                                      Time             CPU   Iterations
--------------------------------------------------------------------------------
BmDirectExceptionsMap                      0.130 ns        0.130 ns   1000000000
BmDirectFunctionsMap                       0.130 ns        0.130 ns   1000000000
BmDirectPointersMap                        0.129 ns        0.129 ns   1000000000
BmCrtpTraitsIfConstexprExceptionsMap        1.30 ns         1.30 ns    539220155
BmCrtpTraitsIfConstexprFunctionsMap        0.949 ns        0.949 ns    741729116
BmCrtpTraitsIfConstexprPointersMap          1.29 ns         1.29 ns    538049699
BmCrtpExceptionsMap                         1.29 ns         1.29 ns    539940481
BmCrtpFunctionsMap                         0.942 ns        0.942 ns    740433018
BmCrtpPointersMap                           1.29 ns         1.29 ns    535647805
BmInheritanceExceptionsMap                  1.30 ns         1.30 ns    541931077
BmInheritanceFunctionsMap                  0.943 ns        0.943 ns    742421851
BmInheritancePointersMap                    1.30 ns         1.30 ns    541701417
BmTemplateInheritanceExceptionsMap          1.29 ns         1.29 ns    540812814
BmTemplateInheritanceFunctionsMap          0.942 ns        0.942 ns    736483151
BmTemplateInheritancePointersMap            1.29 ns         1.29 ns    534733627
BmTemplateSpecializationExceptionsMap       1.30 ns         1.30 ns    541599964
BmTemplateSpecializationFunctionsMap       0.943 ns        0.943 ns    742116350
BmTemplateSpecializationPointersMap         1.29 ns         1.29 ns    540847529
BmTypeTraitsExceptionsMap                   1.29 ns         1.29 ns    539822015
BmTypeTraitsFunctionsMap                   0.943 ns        0.943 ns    739773399
BmTypeTraitsPointersMap                     1.29 ns         1.29 ns    536569664
BmTypeTraitsIfConstexprExceptionsMap       0.129 ns        0.129 ns   1000000000
BmTypeTraitsIfConstexprFunctionsMap        0.130 ns        0.130 ns   1000000000
BmTypeTraitsIfConstexprPointersMap         0.130 ns        0.130 ns   1000000000
```

* clang 14.0.6 `-O3 -flto=thin`, LLD 14.0.6, shared libs

```
--------------------------------------------------------------------------------
Benchmark                                      Time             CPU   Iterations
--------------------------------------------------------------------------------
BmDirectExceptionsMap                       7.99 ns         7.99 ns     87071007
BmDirectFunctionsMap                        7.79 ns         7.79 ns     90168239
BmDirectPointersMap                        0.129 ns        0.129 ns   1000000000
BmCrtpTraitsIfConstexprExceptionsMap        7.60 ns         7.60 ns     91715316
BmCrtpTraitsIfConstexprFunctionsMap         7.58 ns         7.58 ns     92428200
BmCrtpTraitsIfConstexprPointersMap          1.29 ns         1.29 ns    542916869
BmCrtpExceptionsMap                         8.20 ns         8.20 ns     85503211
BmCrtpFunctionsMap                          7.88 ns         7.87 ns     88939834
BmCrtpPointersMap                           1.29 ns         1.29 ns    541025663
BmInheritanceExceptionsMap                  8.02 ns         8.02 ns     87440584
BmInheritanceFunctionsMap                   7.87 ns         7.87 ns     88263133
BmInheritancePointersMap                    1.29 ns         1.29 ns    540948274
BmTemplateInheritanceExceptionsMap          8.42 ns         8.42 ns     83127630
BmTemplateInheritanceFunctionsMap           7.91 ns         7.91 ns     88516628
BmTemplateInheritancePointersMap            1.30 ns         1.30 ns    530407287
BmTemplateSpecializationExceptionsMap       8.44 ns         8.44 ns     84233397
BmTemplateSpecializationFunctionsMap        8.02 ns         8.02 ns     86115135
BmTemplateSpecializationPointersMap         1.29 ns         1.29 ns    541316843
BmTypeTraitsExceptionsMap                   7.64 ns         7.64 ns     92205076
BmTypeTraitsFunctionsMap                    7.57 ns         7.56 ns     91360630
BmTypeTraitsPointersMap                     1.30 ns         1.30 ns    542063547
BmTypeTraitsIfConstexprExceptionsMap        7.79 ns         7.79 ns     90325444
BmTypeTraitsIfConstexprFunctionsMap         7.98 ns         7.98 ns     89834721
BmTypeTraitsIfConstexprPointersMap         0.133 ns        0.133 ns   1000000000
```

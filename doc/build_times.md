## Build Times

* gcc 10.3.0 `-O3 -flto=auto -fno-fat-lto-objects`, GNU ld 2.34, static libs

![time_gcc_ld_static](./images/time_gcc_ld_static.svg)

* gcc 10.3.0 `-O3 -flto=auto -fno-fat-lto-objects`, GNU ld 2.34, shared libs

![time_gcc_ld_shared](./images/time_gcc_ld_shared.svg)

* GCC 10.3.0 `-O3 -flto=auto -fno-fat-lto-objects`, GNU gold 1.16, static libs

![time_gcc_gold_static](./images/time_gcc_gold_static.svg)

* GCC 10.3.0 `-O3 -flto=auto -fno-fat-lto-objects`, GNU gold 1.16, shared libs

![time_gcc_gold_shared](./images/time_gcc_gold_shared.svg)

* clang 14.0.6 `-O3 -flto=thin`, LLD 14.0.6, static libs

![time_clang_lld_static](./images/time_clang_lld_static.svg)

* clang 14.0.6 `-O3 -flto=thin`, LLD 14.0.6, shared libs

![time_clang_lld_shared](./images/time_clang_lld_shared.svg)

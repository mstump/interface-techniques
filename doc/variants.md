# Variants

0. `third_party` contains 3 different map mocks
    * `exceptions_map` is a class-based map with accessor-functions that _throws_
    * `functions_map` is a class-based map with accessor-functions that is _noexcept_
    * `pointers_map` is a pod-based map with direct member access and _pointers_
1. `alias_or_wrapper` uses _Type Traits_ to decide between direct usage or wrapper classes __WIP__
    * Potentially zero overhead, or minimal overhead wrapper classes
    * Complex interface
    * Potential exposure to map specifics
    * Relies on `template_specialization` for wrapper classes
    * Direct access to underlying type
    * Fully templated classes cannot be passed to regular functions nor be stored as member variables
2. `crtp` uses the _CRTP_ as an interface technique
    * Minimal overhead wrapper classes
    * Minimal interface
    * No exposure to map specifics
    * No default implementation
    * One user provided adapter per map implementation
    * Direct access to underlying type
    * Fully templated classes cannot be passed to regular functions nor be stored as member variables
3. `crtp_type_traits_if_constexpr` uses the _CRTP_ as an interface technique and _Type Traits_ with _If Constexpr_ in the adapter
    * Minimal overhead wrapper classes
    * Minimal interface
    * No exposure to map specifics
    * Complex default implementation
    * One adapter for all 3 map implementations
    * Direct access to underlying type
    * Fully templated classes cannot be passed to regular functions nor be stored as member variables
4. `inheritance` uses _Interitance_ as an interface technique
    * Dynamic dispatch runtime overhead
    * Minimal interface
    * No exposure to map specifics
    * No default implementation
    * One user provided adapter per map implementation
    * Direct access to underlying type (after cast to derived)
    * Pointers/references of base classes can be passed to regular functions and be stored as member variables
5. `template_inheritance` uses _Interitance_ with _Templated Derived Classes_ as an interface technique
    * Dynamic dispatch runtime overhead
    * Medium interface
    * No exposure to map specifics
    * No default implementation
    * One user provided adapter per map implementation
    * Direct access to underlying type (after cast to derived)
    * Pointers/references of base classes can be passed to regular functions and be stored as member variables
6. `template_specialization` uses _Template Specialization_ as an interface technique
    * Minimal overhead wrapper classes
    * Minimal interface
    * No exposure to map specifics
    * Minimal default implementation
    * One user provided adapter (template specialization) per map implementation
    * Direct access to underlying type
    * Fully templated classes cannot be passed to regular functions nor be stored as member variables
7. `type_erasure` uses _Type Erasure_ as an interface technique
    * Dynamic dispatch runtime overhead
    * Medium interface
    * No exposure to map specifics
    * No default implementation
    * One user provided adapter per map implementation
    * No direct access to underlying type
    * Concrete wrapper classes can be passed to regular functions and be stored as member variables
8. `type_traits` uses _Type Traits_ as an interface technique
    * Minimal overhead wrapper classes
    * Complex interface
    * No exposure to map specifics
    * Complex default implementation
    * One adapter for all 3 map implementations
    * Direct access to underlying type
    * Fully templated classes cannot be passed to regular functions nor be stored as member variables
9. `type_traits_if_constexpr` uses _Type Traits_ with _If Constexpr_ as an interface technique
    * Zero overhead free functions
    * Complex interface
    * Full exposure to map specifics
    * Complex default implementation
    * One adapter for all 3 map implementations
    * Direct access to underlying type

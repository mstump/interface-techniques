/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <utility>  // for std::move

namespace common {

template <typename T, typename Parameter>
class NamedType {
 public:
  using UnderlyingType = T;

  explicit NamedType(T const& value) : value_(value) {}
  explicit NamedType(T&& value) : value_(std::move(value)) {}
  auto operator()() -> T& { return value_; }
  [[nodiscard]] auto operator()() const -> T const& { return value_; }

 private:
  T value_;
};

using RoadId = NamedType<int, struct RoadIdParameter>;
using LaneId = NamedType<int, struct LaneIdParameter>;

template <typename T>
auto GetNextLeftWidth(const T& map, const RoadId road_id, const LaneId lane_id) noexcept {
  const auto road = map.GetRoad(road_id);
  if (!road) {
    return 0.0;
  }

  const auto next_road = road->GetNextRoad();
  if (!next_road) {
    return 0.0;
  }

  const auto lane = next_road->GetLane(lane_id);
  if (!lane) {
    return 0.0;
  }

  const auto left_lane = lane->GetLeftLane();
  if (!left_lane) {
    return 0.0;
  }

  const auto width = left_lane->GetWidth();

  return width;
}

template <typename T>
auto GetPreviousRightWidth(const T& map, const RoadId road_id, const LaneId lane_id) noexcept {
  const auto road = map.GetRoad(road_id);
  if (!road) {
    return 0.0;
  }

  const auto previous_road = road->GetPreviousRoad();
  if (!previous_road) {
    return 0.0;
  }

  const auto lane = previous_road->GetLane(lane_id);
  if (!lane) {
    return 0.0;
  }

  const auto right_lane = lane->GetRightLane();
  if (!right_lane) {
    return 0.0;
  }

  const auto width = right_lane->GetWidth();

  return width;
}

}  // namespace common

# Interface Techniques

## Build

Requires CMake >3.16 and GCC >10.0

```bash
cmake -GNinja -B build -S . -DCMAKE_BUILD_TYPE=Release
cmake --build build
```

## Test

```bash
ctest --test-dir build -C Release
```

## Scripts

Run in root directory

```bash
scripts/count.sh # Count lines of code of the interfaces and adapters of each variant
scripts/dump.sh  # Dump the disassembly of the test executable of each variant
scripts/time.plt # Print a histogram of the build time
```

## Documentation

1. [Variants](doc/variants.md)
2. [Benchmarks](doc/benchmarks.md)
3. [Build Times](doc/build_times.md)
4. [Metrics](doc/metrics.md)

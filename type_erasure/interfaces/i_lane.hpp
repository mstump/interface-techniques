/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <limits>    // for std::numeric_limits
#include <memory>    // for std::unique_ptr
#include <optional>  // for std::optional
#include <utility>   // for std::forward

#include "common/common.hpp"  // for RoadId

namespace type_erasure::interfaces {

class ILane {
 public:
  template <typename LaneType>
  // NOLINTNEXTLINE(bugprone-forwarding-reference-overload)
  explicit ILane(LaneType&& lane) noexcept
      : lane_model_{std::make_unique<LaneModel<LaneType>>(std::forward<LaneType>(lane))} {}

  ~ILane() noexcept = default;

  ILane(const ILane& other) noexcept : lane_model_{other.lane_model_->Clone()} {}

  ILane(ILane&& other) noexcept : lane_model_{std::move(other.lane_model_)} {}

  auto operator=(const ILane& other) noexcept -> ILane& {
    if (this != &other) {
      lane_model_ = other.lane_model_->Clone();
    }

    return *this;
  }

  auto operator=(ILane&& other) noexcept -> ILane& {
    if (this != &other) {
      lane_model_ = std::move(other.lane_model_);
    }

    return *this;
  }

  [[nodiscard]] auto GetWidth() const noexcept -> double {
    return lane_model_ ? lane_model_->GetWidth() : std::numeric_limits<double>::quiet_NaN();
  }

  auto GetLeftLane() noexcept -> std::optional<ILane> {
    return lane_model_ ? lane_model_->GetLeftLane() : std::nullopt;
  }

  [[nodiscard]] auto GetLeftLane() const noexcept -> std::optional<const ILane> {
    return lane_model_ ? std::as_const(*lane_model_).GetLeftLane() : std::nullopt;
  }

  auto GetRightLane() noexcept -> std::optional<ILane> {
    return lane_model_ ? lane_model_->GetRightLane() : std::nullopt;
  }

  [[nodiscard]] auto GetRightLane() const noexcept -> std::optional<const ILane> {
    return lane_model_ ? std::as_const(*lane_model_).GetRightLane() : std::nullopt;
  }

  struct LaneConcept {
    virtual ~LaneConcept() noexcept = default;

    [[nodiscard]] virtual auto Clone() const noexcept -> std::unique_ptr<LaneConcept> = 0;

    [[nodiscard]] virtual auto GetWidth() const noexcept -> double = 0;

    virtual auto GetLeftLane() noexcept -> std::optional<ILane> = 0;

    [[nodiscard]] virtual auto GetLeftLane() const noexcept -> std::optional<const ILane> = 0;

    virtual auto GetRightLane() noexcept -> std::optional<ILane> = 0;

    [[nodiscard]] virtual auto GetRightLane() const noexcept -> std::optional<const ILane> = 0;
  };

  template <typename LaneType>
  struct LaneModel final : public LaneConcept {
    explicit LaneModel(const LaneType& lane) noexcept : lane_{lane} {}

    ~LaneModel() noexcept override = default;

    [[nodiscard]] auto Clone() const noexcept -> std::unique_ptr<LaneConcept> override {
      return std::make_unique<LaneModel<LaneType>>(lane_);
    }

    [[nodiscard]] auto GetWidth() const noexcept -> double override { return lane_.GetWidth(); }

    auto GetLeftLane() noexcept -> std::optional<ILane> override {
      if (auto left_lane = lane_.GetLeftLane(); left_lane.has_value()) {
        return ILane{left_lane.value()};
      }

      return std::nullopt;
    }

    [[nodiscard]] auto GetLeftLane() const noexcept -> std::optional<const ILane> override {
      if (const auto left_lane = lane_.GetLeftLane(); left_lane.has_value()) {
        return ILane{left_lane.value()};
      }

      return std::nullopt;
    }

    auto GetRightLane() noexcept -> std::optional<ILane> override {
      if (auto right_lane = lane_.GetRightLane(); right_lane.has_value()) {
        return ILane{right_lane.value()};
      }

      return std::nullopt;
    }

    [[nodiscard]] auto GetRightLane() const noexcept -> std::optional<const ILane> override {
      if (const auto right_lane = lane_.GetRightLane(); right_lane.has_value()) {
        return ILane{right_lane.value()};
      }

      return std::nullopt;
    }

   private:
    LaneType lane_;
  };

 private:
  std::unique_ptr<LaneConcept> lane_model_;
};

}  // namespace type_erasure::interfaces

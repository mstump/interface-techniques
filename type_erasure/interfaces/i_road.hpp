/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <memory>    // for std::unique_ptr
#include <optional>  // for std::optional
#include <utility>   // for std::forward

#include "common/common.hpp"                   // for RoadId
#include "type_erasure/interfaces/i_lane.hpp"  // for ILane

namespace type_erasure::interfaces {

class IRoad {
 public:
  template <typename RoadType>
  // NOLINTNEXTLINE(bugprone-forwarding-reference-overload)
  explicit IRoad(RoadType&& road) : road_model_{std::make_unique<RoadModel<RoadType>>(std::forward<RoadType>(road))} {}

  ~IRoad() noexcept = default;

  IRoad(const IRoad& other) noexcept : road_model_{other.road_model_->Clone()} {}

  IRoad(IRoad&& other) noexcept : road_model_{std::move(other.road_model_)} {}

  auto operator=(const IRoad& other) noexcept -> IRoad& {
    if (this != &other) {
      road_model_ = other.road_model_->Clone();
    }

    return *this;
  }

  auto operator=(IRoad&& other) noexcept -> IRoad& {
    if (this != &other) {
      road_model_ = std::move(other.road_model_);
    }

    return *this;
  }

  auto GetLane(common::LaneId identifier) noexcept -> std::optional<ILane> {
    return road_model_ ? road_model_->GetLane(identifier) : std::nullopt;
  }

  [[nodiscard]] auto GetLane(common::LaneId identifier) const noexcept -> std::optional<const ILane> {
    return road_model_ ? std::as_const(*road_model_).GetLane(identifier) : std::nullopt;
  }

  auto GetNextRoad() noexcept -> std::optional<IRoad> {
    return road_model_ ? road_model_->GetNextRoad() : std::nullopt;
  }

  [[nodiscard]] auto GetNextRoad() const noexcept -> std::optional<const IRoad> {
    return road_model_ ? std::as_const(*road_model_).GetNextRoad() : std::nullopt;
  }

  auto GetPreviousRoad() noexcept -> std::optional<IRoad> {
    return road_model_ ? road_model_->GetPreviousRoad() : std::nullopt;
  }

  [[nodiscard]] auto GetPreviousRoad() const noexcept -> std::optional<const IRoad> {
    return road_model_ ? std::as_const(*road_model_).GetPreviousRoad() : std::nullopt;
  }

  struct RoadConcept {
    virtual ~RoadConcept() = default;

    [[nodiscard]] virtual auto Clone() const noexcept -> std::unique_ptr<RoadConcept> = 0;

    virtual auto GetLane(common::LaneId identifier) noexcept -> std::optional<ILane> = 0;

    [[nodiscard]] virtual auto GetLane(common::LaneId identifier) const noexcept -> std::optional<const ILane> = 0;

    virtual auto GetNextRoad() noexcept -> std::optional<IRoad> = 0;

    [[nodiscard]] virtual auto GetNextRoad() const noexcept -> std::optional<const IRoad> = 0;

    virtual auto GetPreviousRoad() noexcept -> std::optional<IRoad> = 0;

    [[nodiscard]] virtual auto GetPreviousRoad() const noexcept -> std::optional<const IRoad> = 0;
  };

  template <typename RoadType>
  struct RoadModel final : public RoadConcept {
    explicit RoadModel(const RoadType& road) : road_{road} {}

    ~RoadModel() noexcept override = default;

    [[nodiscard]] auto Clone() const noexcept -> std::unique_ptr<RoadConcept> override {
      return std::make_unique<RoadModel<RoadType>>(road_);
    }

    auto GetLane(common::LaneId identifier) noexcept -> std::optional<ILane> override {
      if (auto lane = road_.GetLane(identifier); lane.has_value()) {
        return ILane{lane.value()};
      }

      return std::nullopt;
    }

    [[nodiscard]] auto GetLane(common::LaneId identifier) const noexcept -> std::optional<const ILane> override {
      if (const auto lane = road_.GetLane(identifier); lane.has_value()) {
        return ILane{lane.value()};
      }

      return std::nullopt;
    }

    auto GetNextRoad() noexcept -> std::optional<IRoad> override {
      if (auto next_road = road_.GetNextRoad(); next_road.has_value()) {
        return IRoad{next_road.value()};
      }

      return std::nullopt;
    }

    [[nodiscard]] auto GetNextRoad() const noexcept -> std::optional<const IRoad> override {
      if (const auto next_road = road_.GetNextRoad(); next_road.has_value()) {
        return IRoad{next_road.value()};
      }

      return std::nullopt;
    }

    auto GetPreviousRoad() noexcept -> std::optional<IRoad> override {
      if (auto previous_road = road_.GetPreviousRoad(); previous_road.has_value()) {
        return IRoad{previous_road.value()};
      }

      return std::nullopt;
    }

    [[nodiscard]] auto GetPreviousRoad() const noexcept -> std::optional<const IRoad> override {
      if (const auto previous_road = road_.GetPreviousRoad(); previous_road.has_value()) {
        return IRoad{previous_road.value()};
      }

      return std::nullopt;
    }

   private:
    RoadType road_;
  };

 private:
  std::unique_ptr<RoadConcept> road_model_;
};

}  // namespace type_erasure::interfaces

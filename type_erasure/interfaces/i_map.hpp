/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <memory>    // for std::unique_ptr
#include <optional>  // for std::optional
#include <utility>   // for std::forward

#include "common/common.hpp"                   // for RoadId
#include "type_erasure/interfaces/i_road.hpp"  // for IRoad

namespace type_erasure::interfaces {

class IMap {
 public:
  template <typename MapType>
  // NOLINTNEXTLINE(bugprone-forwarding-reference-overload)
  explicit IMap(MapType&& map) : map_model_{std::make_unique<MapModel<MapType>>(std::forward<MapType>(map))} {}

  ~IMap() noexcept = default;

  IMap(const IMap& other) noexcept : map_model_{other.map_model_->Clone()} {}

  IMap(IMap&& other) noexcept : map_model_{std::move(other.map_model_)} {}

  auto operator=(const IMap& other) noexcept -> IMap& {
    if (this != &other) {
      map_model_ = other.map_model_->Clone();
    }

    return *this;
  }

  auto operator=(IMap&& other) noexcept -> IMap& {
    if (this != &other) {
      map_model_ = std::move(other.map_model_);
    }

    return *this;
  }

  auto GetRoad(common::RoadId identifier) noexcept -> std::optional<IRoad> {
    return map_model_ ? map_model_->GetRoad(identifier) : std::nullopt;
  }

  [[nodiscard]] auto GetRoad(common::RoadId identifier) const noexcept -> std::optional<const IRoad> {
    return map_model_ ? std::as_const(*map_model_).GetRoad(identifier) : std::nullopt;
  }

  struct MapConcept {
    virtual ~MapConcept() noexcept = default;

    [[nodiscard]] virtual auto Clone() const noexcept -> std::unique_ptr<MapConcept> = 0;

    virtual auto GetRoad(common::RoadId identifier) noexcept -> std::optional<IRoad> = 0;

    [[nodiscard]] virtual auto GetRoad(common::RoadId identifier) const noexcept -> std::optional<const IRoad> = 0;
  };

  template <typename MapType>
  struct MapModel final : public MapConcept {
    explicit MapModel(const MapType& map) : map_{map} {}

    ~MapModel() noexcept override = default;

    [[nodiscard]] auto Clone() const noexcept -> std::unique_ptr<MapConcept> override {
      return std::make_unique<MapModel<MapType>>(map_);
    }

    auto GetRoad(common::RoadId identifier) noexcept -> std::optional<IRoad> override {
      if (auto road = map_.GetRoad(identifier); road.has_value()) {
        return IRoad{road.value()};
      }

      return std::nullopt;
    }

    [[nodiscard]] auto GetRoad(common::RoadId identifier) const noexcept -> std::optional<const IRoad> override {
      if (const auto road = map_.GetRoad(identifier); road.has_value()) {
        return IRoad{road.value()};
      }

      return std::nullopt;
    }

   private:
    MapType map_;
  };

 private:
  std::unique_ptr<MapConcept> map_model_;
};

}  // namespace type_erasure::interfaces

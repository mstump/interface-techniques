/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>
#include <functions_lane.hpp>
#include <optional>

namespace type_erasure::functions_map_adapter {

class LaneAdapter {
 public:
  explicit LaneAdapter(functions_map::FunctionsLane& lane) : lane_{lane} {}

  [[nodiscard]] auto GetWidth() const noexcept -> double { return lane_.get().GetWidth(); }

  auto GetLeftLane() noexcept -> std::optional<LaneAdapter> {
    if (auto lane = lane_.get().GetLeftLane(); lane.has_value()) {
      return LaneAdapter{lane.value()};
    }

    return std::nullopt;
  }

  [[nodiscard]] auto GetLeftLane() const noexcept -> std::optional<const LaneAdapter> {
    if (const auto lane = lane_.get().GetLeftLane(); lane.has_value()) {
      return LaneAdapter{lane.value()};
    }

    return std::nullopt;
  }

  auto GetRightLane() noexcept -> std::optional<LaneAdapter> {
    if (auto lane = lane_.get().GetRightLane(); lane.has_value()) {
      return LaneAdapter{lane.value()};
    }

    return std::nullopt;
  }

  [[nodiscard]] auto GetRightLane() const noexcept -> std::optional<const LaneAdapter> {
    if (const auto lane = lane_.get().GetRightLane(); lane.has_value()) {
      return LaneAdapter{lane.value()};
    }

    return std::nullopt;
  }

  inline auto Get() noexcept -> functions_map::FunctionsLane& { return lane_.get(); }

  [[nodiscard]] inline auto Get() const noexcept -> const functions_map::FunctionsLane& { return lane_.get(); }

 private:
  std::reference_wrapper<functions_map::FunctionsLane> lane_;
};

}  // namespace type_erasure::functions_map_adapter

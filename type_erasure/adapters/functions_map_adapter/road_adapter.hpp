/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>
#include <functions_lane.hpp>
#include <functions_road.hpp>
#include <optional>

#include "common/common.hpp"
#include "type_erasure/adapters/functions_map_adapter/lane_adapter.hpp"

namespace type_erasure::functions_map_adapter {

class RoadAdapter {
 public:
  explicit RoadAdapter(functions_map::FunctionsRoad& road) : road_{road} {}

  auto GetLane(common::LaneId identifier) noexcept -> std::optional<LaneAdapter> {
    if (auto lane = road_.get().GetLane(identifier()); lane.has_value()) {
      return LaneAdapter{lane.value()};
    }

    return std::nullopt;
  }

  [[nodiscard]] auto GetLane(common::LaneId identifier) const noexcept -> std::optional<const LaneAdapter> {
    if (const auto lane = road_.get().GetLane(identifier()); lane.has_value()) {
      return LaneAdapter{lane.value()};
    }

    return std::nullopt;
  }

  auto GetNextRoad() noexcept -> std::optional<RoadAdapter> {
    if (auto road = road_.get().GetNextRoad(); road.has_value()) {
      return RoadAdapter{road.value()};
    }

    return std::nullopt;
  }

  [[nodiscard]] auto GetNextRoad() const noexcept -> std::optional<const RoadAdapter> {
    if (const auto road = road_.get().GetNextRoad(); road.has_value()) {
      return RoadAdapter{road.value()};
    }

    return std::nullopt;
  }

  auto GetPreviousRoad() noexcept -> std::optional<RoadAdapter> {
    if (auto road = road_.get().GetPreviousRoad(); road.has_value()) {
      return RoadAdapter{road.value()};
    }

    return std::nullopt;
  }

  [[nodiscard]] auto GetPreviousRoad() const noexcept -> std::optional<const RoadAdapter> {
    if (const auto road = road_.get().GetPreviousRoad(); road.has_value()) {
      return RoadAdapter{road.value()};
    }

    return std::nullopt;
  }

  inline auto Get() noexcept -> functions_map::FunctionsRoad& { return road_.get(); }

  [[nodiscard]] inline auto Get() const noexcept -> const functions_map::FunctionsRoad& { return road_.get(); }

 private:
  std::reference_wrapper<functions_map::FunctionsRoad> road_;
};

}  // namespace type_erasure::functions_map_adapter

/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>
#include <functions_map.hpp>
#include <functions_road.hpp>
#include <optional>

#include "common/common.hpp"
#include "type_erasure/adapters/functions_map_adapter/road_adapter.hpp"

namespace type_erasure::functions_map_adapter {

class MapAdapter {
 public:
  explicit MapAdapter(functions_map::FunctionsMap& map) : map_(map){};

  auto GetRoad(common::RoadId identifier) noexcept -> std::optional<RoadAdapter> {
    if (auto road = map_.get().GetRoad(identifier()); road.has_value()) {
      return RoadAdapter{road.value()};
    }

    return std::nullopt;
  }

  [[nodiscard]] auto GetRoad(common::RoadId identifier) const noexcept -> std::optional<const RoadAdapter> {
    if (const auto road = map_.get().GetRoad(identifier()); road.has_value()) {
      return RoadAdapter{road.value()};
    }

    return std::nullopt;
  }

  inline auto Get() noexcept -> functions_map::FunctionsMap& { return map_.get(); }

  [[nodiscard]] inline auto Get() const noexcept -> const functions_map::FunctionsMap& { return map_.get(); }

 private:
  std::reference_wrapper<functions_map::FunctionsMap> map_;
};

}  // namespace type_erasure::functions_map_adapter

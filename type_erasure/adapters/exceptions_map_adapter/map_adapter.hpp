/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <exceptions_map.hpp>
#include <functional>
#include <optional>

#include "common/common.hpp"
#include "type_erasure/adapters/exceptions_map_adapter/road_adapter.hpp"

namespace type_erasure::exceptions_map_adapter {

class MapAdapter {
 public:
  explicit MapAdapter(exceptions_map::ExceptionsMap& map) : map_(map){};

  auto GetRoad(common::RoadId identifier) noexcept -> std::optional<RoadAdapter> {
    try {
      return RoadAdapter{map_.get().GetRoad(identifier())};
    } catch (...) {
      return std::nullopt;
    }
  }

  [[nodiscard]] auto GetRoad(common::RoadId identifier) const noexcept -> std::optional<const RoadAdapter> {
    try {
      return RoadAdapter{map_.get().GetRoad(identifier())};
    } catch (...) {
      return std::nullopt;
    }
  }

  inline auto Get() noexcept -> exceptions_map::ExceptionsMap& { return map_.get(); }

  [[nodiscard]] inline auto Get() const noexcept -> const exceptions_map::ExceptionsMap& { return map_.get(); }

 private:
  std::reference_wrapper<exceptions_map::ExceptionsMap> map_;
};

}  // namespace type_erasure::exceptions_map_adapter

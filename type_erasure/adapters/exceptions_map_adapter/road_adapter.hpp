/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <exceptions_road.hpp>
#include <functional>
#include <optional>

#include "common/common.hpp"
#include "type_erasure/adapters/exceptions_map_adapter/lane_adapter.hpp"

namespace type_erasure::exceptions_map_adapter {

class RoadAdapter {
 public:
  explicit RoadAdapter(exceptions_map::ExceptionsRoad& road) : road_{road} {}

  auto GetLane(common::LaneId identifier) noexcept -> std::optional<LaneAdapter> {
    try {
      return LaneAdapter{road_.get().GetLane(identifier())};
    } catch (...) {
      return std::nullopt;
    }
  }

  [[nodiscard]] auto GetLane(common::LaneId identifier) const noexcept -> std::optional<const LaneAdapter> {
    try {
      return LaneAdapter{road_.get().GetLane(identifier())};
    } catch (...) {
      return std::nullopt;
    }
  }

  auto GetNextRoad() noexcept -> std::optional<RoadAdapter> {
    try {
      return RoadAdapter{road_.get().GetNextRoad()};
    } catch (...) {
      return std::nullopt;
    }
  }

  [[nodiscard]] auto GetNextRoad() const noexcept -> std::optional<const RoadAdapter> {
    try {
      return RoadAdapter{road_.get().GetNextRoad()};
    } catch (...) {
      return std::nullopt;
    }
  }

  auto GetPreviousRoad() noexcept -> std::optional<RoadAdapter> {
    try {
      return RoadAdapter{road_.get().GetPreviousRoad()};
    } catch (...) {
      return std::nullopt;
    }
  }

  [[nodiscard]] auto GetPreviousRoad() const noexcept -> std::optional<const RoadAdapter> {
    try {
      return RoadAdapter{road_.get().GetPreviousRoad()};
    } catch (...) {
      return std::nullopt;
    }
  }

  inline auto Get() noexcept -> exceptions_map::ExceptionsRoad& { return road_.get(); }

  [[nodiscard]] inline auto Get() const noexcept -> const exceptions_map::ExceptionsRoad& { return road_.get(); }

 private:
  std::reference_wrapper<exceptions_map::ExceptionsRoad> road_;
};

}  // namespace type_erasure::exceptions_map_adapter

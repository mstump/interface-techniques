/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <exceptions_lane.hpp>
#include <functional>
#include <optional>

#include "type_erasure/interfaces/i_lane.hpp"

namespace type_erasure::exceptions_map_adapter {

class LaneAdapter {
 public:
  explicit LaneAdapter(exceptions_map::ExceptionsLane& lane) : lane_{lane} {}

  [[nodiscard]] auto GetWidth() const noexcept -> double { return lane_.get().GetWidth(); }

  auto GetLeftLane() noexcept -> std::optional<LaneAdapter> {
    try {
      return LaneAdapter{lane_.get().GetLeftLane()};
    } catch (...) {
      return std::nullopt;
    }
  }

  [[nodiscard]] auto GetLeftLane() const noexcept -> std::optional<const LaneAdapter> {
    try {
      return LaneAdapter{lane_.get().GetLeftLane()};
    } catch (...) {
      return std::nullopt;
    }
  }

  auto GetRightLane() noexcept -> std::optional<LaneAdapter> {
    try {
      return LaneAdapter{lane_.get().GetRightLane()};
    } catch (...) {
      return std::nullopt;
    }
  }

  [[nodiscard]] auto GetRightLane() const noexcept -> std::optional<const LaneAdapter> {
    try {
      return LaneAdapter{lane_.get().GetRightLane()};
    } catch (...) {
      return std::nullopt;
    }
  }

  inline auto Get() noexcept -> exceptions_map::ExceptionsLane& { return lane_.get(); }

  [[nodiscard]] inline auto Get() const noexcept -> const exceptions_map::ExceptionsLane& { return lane_.get(); }

 private:
  std::reference_wrapper<exceptions_map::ExceptionsLane> lane_;
};

}  // namespace type_erasure::exceptions_map_adapter

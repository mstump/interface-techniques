/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>
#include <memory>
#include <type_traits>

#include "common/common.hpp"
#include "template_inheritance/interfaces/i_map.hpp"
#include "template_inheritance/interfaces/i_road.hpp"
#include "template_inheritance/interfaces/road.hpp"

namespace template_inheritance::interfaces {

template <typename T>
class Map final : public interfaces::IMap {
 public:
  using RoadType = typename decltype(std::declval<T>().GetRoad(std::declval<common::RoadId>()))::value_type;

  ~Map() override = default;
  explicit Map(const T& map) : map_(map){};

  auto GetRoad(common::RoadId identifier) noexcept -> std::unique_ptr<interfaces::IRoad> override {
    if (auto road = map_.GetRoad(identifier); road.has_value()) {
      return std::make_unique<Road<RoadType>>(road.value());
    }

    return nullptr;
  }

  [[nodiscard]] auto GetRoad(common::RoadId identifier) const noexcept
      -> std::unique_ptr<const interfaces::IRoad> override {
    if (auto road = map_.GetRoad(identifier); road.has_value()) {
      return std::make_unique<const Road<RoadType>>(road.value());
    }

    return nullptr;
  }

  inline auto Get() noexcept -> decltype(auto) { return map_.Get(); }

  [[nodiscard]] inline auto Get() const noexcept -> decltype(auto) { return map_.Get(); }

 private:
  T map_;
};

}  // namespace template_inheritance::interfaces

/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>
#include <memory>
#include <type_traits>

#include "common/common.hpp"
#include "template_inheritance/interfaces/i_lane.hpp"
#include "template_inheritance/interfaces/i_road.hpp"
#include "template_inheritance/interfaces/lane.hpp"

namespace template_inheritance::interfaces {

template <typename T>
class Road final : public interfaces::IRoad {
 public:
  using LaneType = typename decltype(std::declval<T>().GetLane(std::declval<common::LaneId>()))::value_type;

  ~Road() override = default;
  explicit Road(const T& road) : road_{road} {}

  auto GetLane(common::LaneId identifier) noexcept -> std::unique_ptr<interfaces::ILane> override {
    if (auto lane = road_.GetLane(identifier); lane.has_value()) {
      return std::make_unique<Lane<LaneType>>(lane.value());
    }

    return nullptr;
  }

  [[nodiscard]] auto GetLane(common::LaneId identifier) const noexcept
      -> std::unique_ptr<const interfaces::ILane> override {
    if (auto lane = road_.GetLane(identifier); lane.has_value()) {
      return std::make_unique<const Lane<LaneType>>(lane.value());
    }

    return nullptr;
  }

  auto GetNextRoad() noexcept -> std::unique_ptr<interfaces::IRoad> override {
    if (auto next_road = road_.GetNextRoad(); next_road.has_value()) {
      return std::make_unique<Road>(next_road.value());
    }

    return nullptr;
  }

  [[nodiscard]] auto GetNextRoad() const noexcept -> std::unique_ptr<const interfaces::IRoad> override {
    if (auto next_road = road_.GetNextRoad(); next_road.has_value()) {
      return std::make_unique<const Road>(next_road.value());
    }

    return nullptr;
  }

  auto GetPreviousRoad() noexcept -> std::unique_ptr<interfaces::IRoad> override {
    if (auto previous_road = road_.GetPreviousRoad(); previous_road.has_value()) {
      return std::make_unique<Road>(previous_road.value());
    }

    return nullptr;
  }

  [[nodiscard]] auto GetPreviousRoad() const noexcept -> std::unique_ptr<const interfaces::IRoad> override {
    if (auto previous_road = road_.GetPreviousRoad(); previous_road.has_value()) {
      return std::make_unique<const Road>(previous_road.value());
    }

    return nullptr;
  }

  inline auto Get() noexcept -> decltype(auto) { return road_.Get(); }

  [[nodiscard]] inline auto Get() const noexcept -> decltype(auto) { return road_.Get(); }

 private:
  T road_;
};

}  // namespace template_inheritance::interfaces

/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>
#include <memory>

#include "template_inheritance/interfaces/i_lane.hpp"

namespace template_inheritance::interfaces {

template <typename T>
class Lane final : public interfaces::ILane {
 public:
  ~Lane() override = default;
  explicit Lane(const T& lane) : lane_{lane} {}

  [[nodiscard]] auto GetWidth() const noexcept -> double override { return lane_.GetWidth(); }

  auto GetLeftLane() noexcept -> std::unique_ptr<interfaces::ILane> override {
    if (auto left_lane = lane_.GetLeftLane(); left_lane.has_value()) {
      return std::make_unique<Lane>(*left_lane);
    }

    return nullptr;
  }

  [[nodiscard]] auto GetLeftLane() const noexcept -> std::unique_ptr<const interfaces::ILane> override {
    if (const auto left_lane = lane_.GetLeftLane(); left_lane.has_value()) {
      return std::make_unique<const Lane>(*left_lane);
    }

    return nullptr;
  }

  auto GetRightLane() noexcept -> std::unique_ptr<interfaces::ILane> override {
    if (auto right_lane = lane_.GetRightLane(); right_lane.has_value()) {
      return std::make_unique<Lane>(*right_lane);
    }

    return nullptr;
  }

  [[nodiscard]] auto GetRightLane() const noexcept -> std::unique_ptr<const interfaces::ILane> override {
    if (const auto right_lane = lane_.GetRightLane(); right_lane.has_value()) {
      return std::make_unique<const Lane>(*right_lane);
    }

    return nullptr;
  }

  inline auto Get() noexcept -> decltype(auto) { return lane_.Get(); }

  [[nodiscard]] inline auto Get() const noexcept -> decltype(auto) { return lane_.Get(); }

 private:
  T lane_;
};

}  // namespace template_inheritance::interfaces

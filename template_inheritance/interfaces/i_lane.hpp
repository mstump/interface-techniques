/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <memory>

#include "common/common.hpp"

namespace template_inheritance::interfaces {

struct ILane {
  virtual ~ILane() = default;

  [[nodiscard]] virtual auto GetWidth() const noexcept -> double = 0;

  virtual auto GetLeftLane() noexcept -> std::unique_ptr<interfaces::ILane> = 0;

  [[nodiscard]] virtual auto GetLeftLane() const noexcept -> std::unique_ptr<const interfaces::ILane> = 0;

  virtual auto GetRightLane() noexcept -> std::unique_ptr<interfaces::ILane> = 0;

  [[nodiscard]] virtual auto GetRightLane() const noexcept -> std::unique_ptr<const interfaces::ILane> = 0;
};

}  // namespace template_inheritance::interfaces

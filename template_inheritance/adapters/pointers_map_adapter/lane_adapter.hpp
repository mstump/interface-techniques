/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>
#include <memory>
#include <pointers_lane.hpp>

namespace template_inheritance::pointers_map_adapter {

class LaneAdapter {
 public:
  explicit LaneAdapter(pointers_map::PointersLane& lane) : lane_{lane} {}

  [[nodiscard]] auto GetWidth() const noexcept -> double { return lane_.get().width; }

  auto GetLeftLane() noexcept -> std::optional<LaneAdapter> {
    if (lane_.get().left_lane == nullptr) {
      return std::nullopt;
    }

    return LaneAdapter{*lane_.get().left_lane};
  }

  [[nodiscard]] auto GetLeftLane() const noexcept -> std::optional<const LaneAdapter> {
    if (lane_.get().left_lane == nullptr) {
      return std::nullopt;
    }

    return LaneAdapter{*lane_.get().left_lane};
  }

  auto GetRightLane() noexcept -> std::optional<LaneAdapter> {
    if (lane_.get().right_lane == nullptr) {
      return std::nullopt;
    }

    return LaneAdapter{*lane_.get().right_lane};
  }

  [[nodiscard]] auto GetRightLane() const noexcept -> std::optional<const LaneAdapter> {
    if (lane_.get().right_lane == nullptr) {
      return std::nullopt;
    }

    return LaneAdapter{*lane_.get().right_lane};
  }

  inline auto Get() noexcept -> pointers_map::PointersLane& { return lane_.get(); }

  [[nodiscard]] inline auto Get() const noexcept -> const pointers_map::PointersLane& { return lane_.get(); }

 private:
  std::reference_wrapper<pointers_map::PointersLane> lane_;
};

}  // namespace template_inheritance::pointers_map_adapter

/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>
#include <optional>
#include <pointers_map.hpp>

#include "common/common.hpp"
#include "template_inheritance/adapters/pointers_map_adapter/road_adapter.hpp"

namespace template_inheritance::pointers_map_adapter {

class MapAdapter {
 public:
  explicit MapAdapter(pointers_map::PointersMap& map) : map_(map){};

  auto GetRoad(common::RoadId identifier) noexcept -> std::optional<RoadAdapter> {
    try {
      return RoadAdapter{map_.get().roads.at(identifier())};
    } catch (...) {
      return std::nullopt;
    }
  }

  [[nodiscard]] auto GetRoad(common::RoadId identifier) const noexcept -> std::optional<const RoadAdapter> {
    try {
      return RoadAdapter{map_.get().roads.at(identifier())};
    } catch (...) {
      return std::nullopt;
    }
  }

  inline auto Get() noexcept -> pointers_map::PointersMap& { return map_.get(); }

  [[nodiscard]] inline auto Get() const noexcept -> const pointers_map::PointersMap& { return map_.get(); }

 private:
  std::reference_wrapper<pointers_map::PointersMap> map_;
};

}  // namespace template_inheritance::pointers_map_adapter

/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>
#include <functions_lane.hpp>
#include <optional>
#include <pointers_road.hpp>

#include "common/common.hpp"
#include "template_inheritance/adapters/pointers_map_adapter/lane_adapter.hpp"

namespace template_inheritance::pointers_map_adapter {

class RoadAdapter {
 public:
  explicit RoadAdapter(pointers_map::PointersRoad& road) : road_{road} {}

  auto GetLane(common::LaneId identifier) noexcept -> std::optional<LaneAdapter> {
    try {
      return std::make_optional<LaneAdapter>(road_.get().lanes.at(identifier()));
    } catch (...) {
      return std::nullopt;
    }
  }

  [[nodiscard]] auto GetLane(common::LaneId identifier) const noexcept -> std::optional<const LaneAdapter> {
    try {
      return std::make_optional<const LaneAdapter>(road_.get().lanes.at(identifier()));
    } catch (...) {
      return std::nullopt;
    }
  }

  auto GetNextRoad() noexcept -> std::optional<RoadAdapter> {
    if (road_.get().next_road == nullptr) {
      return std::nullopt;
    }

    return std::make_optional<RoadAdapter>(*road_.get().next_road);
  }

  [[nodiscard]] auto GetNextRoad() const noexcept -> std::optional<const RoadAdapter> {
    if (road_.get().next_road == nullptr) {
      return std::nullopt;
    }

    return std::make_optional<const RoadAdapter>(*road_.get().next_road);
  }

  auto GetPreviousRoad() noexcept -> std::optional<RoadAdapter> {
    if (road_.get().previous_road == nullptr) {
      return std::nullopt;
    }

    return std::make_optional<RoadAdapter>(*road_.get().previous_road);
  }

  [[nodiscard]] auto GetPreviousRoad() const noexcept -> std::optional<const RoadAdapter> {
    if (road_.get().previous_road == nullptr) {
      return std::nullopt;
    }

    return std::make_optional<const RoadAdapter>(*road_.get().previous_road);
  }

  [[nodiscard]]

  inline auto
  Get() noexcept -> pointers_map::PointersRoad& {
    return road_.get();
  }

  [[nodiscard]] inline auto Get() const noexcept -> const pointers_map::PointersRoad& { return road_.get(); }

 private:
  std::reference_wrapper<pointers_map::PointersRoad> road_;
};

}  // namespace template_inheritance::pointers_map_adapter

#!/usr/bin/env bash

for dir in $(find $PWD -type d -name "interfaces" | sort -u); do
   echo "$dir"
   cloc --include-lang="C++","C/C++ Header" --not-match-d='build' "$dir"
done

for dir in $(find $PWD -type d -name "adapters" | sort -u); do
   echo "$dir"
   cloc --include-lang="C++","C/C++ Header" --not-match-d='build' "$dir"
done

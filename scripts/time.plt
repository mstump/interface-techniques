#!/usr/bin/env -S gnuplot -c

set terminal svg enhanced background rgb 'white'

build_directory = system("dirname ".ARG0)."/../build/"

set output build_directory."time.svg"

array variants = [ "alias\\_or\\_wrapper", \
                   "crtp", \
                   "crtp\\_type\\_traits\\_if\\_constexpr", \
                   "inheritance", \
                   "template\\_inheritance", \
                   "template\\_specialization", \
                   "type\\_traits", \
                   "type\\_traits\\_if\\_constexpr" ]


array compile_time_data = [ build_directory."alias_or_wrapper/compile_time.txt", \
                            build_directory."crtp/compile_time.txt", \
                            build_directory."crtp_type_traits_if_constexpr/compile_time.txt", \
                            build_directory."inheritance/compile_time.txt", \
                            build_directory."template_inheritance/compile_time.txt", \
                            build_directory."template_specialization/compile_time.txt", \
                            build_directory."type_traits/compile_time.txt", \
                            build_directory."type_traits_if_constexpr/compile_time.txt" ]

array link_time_data = [ build_directory."alias_or_wrapper/link_time.txt", \
                         build_directory."crtp/link_time.txt", \
                         build_directory."crtp_type_traits_if_constexpr/link_time.txt", \
                         build_directory."inheritance/link_time.txt", \
                         build_directory."template_inheritance/link_time.txt", \
                         build_directory."template_specialization/link_time.txt", \
                         build_directory."type_traits/link_time.txt", \
                         build_directory."type_traits_if_constexpr/compile_time.txt" ]

array compile_time_sums[|compile_time_data|]
array link_time_sums[|link_time_data|]

do for [i=1:|compile_time_data|] {
  stats compile_time_data[i] using 3 nooutput
  compile_time_sums[i] = STATS_sum
}

do for [i=1:|link_time_data|] {
  stats link_time_data[i] using 3 nooutput
  link_time_sums[i] = STATS_sum
}

set title "Build Time"
set style data histograms
set style histogram rowstacked
set boxwidth 0.8 relative
set style fill solid 1.0 border -1
set xtics rotate by -45
set ylabel "time [s]"
set yrange [0:4]

plot compile_time_sums using 2:xticlabel(variants[$0+1]) t "Compile Time", \
     link_time_sums using 2 t "Link Time"

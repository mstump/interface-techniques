#!/usr/bin/env bash

for type in Debug \
            Release \
            RelWithDebInfo; do
    for variant in alias_or_wrapper \
                   crtp \
                   crtp_type_traits_if_constexpr \
                   inheritance \
                   template_inheritance \
                   template_specialization \
                   type_traits \
                   type_traits_if_constexpr; do
        objdump -S -d -C -M intel build/$variant/test/$type/$variant"_test" >build/$variant"_test_"$type.dump
    done
done

/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <memory>

#include "common/common.hpp"

namespace inheritance::interfaces {

struct ILane;

struct IRoad {
  virtual ~IRoad() = default;

  virtual auto GetLane(common::LaneId identifier) noexcept -> std::unique_ptr<interfaces::ILane> = 0;
  [[nodiscard]] virtual auto GetLane(common::LaneId identifier) const noexcept
      -> std::unique_ptr<const interfaces::ILane> = 0;

  virtual auto GetNextRoad() noexcept -> std::unique_ptr<interfaces::IRoad> = 0;
  [[nodiscard]] virtual auto GetNextRoad() const noexcept -> std::unique_ptr<const interfaces::IRoad> = 0;

  virtual auto GetPreviousRoad() noexcept -> std::unique_ptr<interfaces::IRoad> = 0;
  [[nodiscard]] virtual auto GetPreviousRoad() const noexcept -> std::unique_ptr<const interfaces::IRoad> = 0;
};

}  // namespace inheritance::interfaces

/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <memory>

#include "common/common.hpp"

namespace inheritance::interfaces {

struct IRoad;

//
// Enforces:
// * Complete function signature
//   * return type
//   * noexcept
//   * const
//   * nodiscard
//
// Enables:
// * run-time polymorphism
// * passing as base class pointer/reference without templates
//
// Cannot enforce `Get` function to retrieve the underlying type
//
struct IMap {
  virtual ~IMap() = default;

  virtual auto GetRoad(common::RoadId identifier) noexcept -> std::unique_ptr<interfaces::IRoad> = 0;
  [[nodiscard]] virtual auto GetRoad(common::RoadId identifier) const noexcept
      -> std::unique_ptr<const interfaces::IRoad> = 0;
};

}  // namespace inheritance::interfaces

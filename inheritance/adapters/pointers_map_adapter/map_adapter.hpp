/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>
#include <memory>
#include <pointers_map.hpp>

#include "common/common.hpp"
#include "inheritance/adapters/pointers_map_adapter/road_adapter.hpp"
#include "inheritance/interfaces/i_map.hpp"
#include "inheritance/interfaces/i_road.hpp"

namespace inheritance::pointers_map_adapter {

class MapAdapter final : public interfaces::IMap {
 public:
  ~MapAdapter() override = default;

  explicit MapAdapter(pointers_map::PointersMap& map) : map_(map){};

  auto GetRoad(common::RoadId identifier) noexcept -> std::unique_ptr<interfaces::IRoad> override {
    try {
      return std::make_unique<RoadAdapter>(map_.get().roads.at(identifier()));
    } catch (...) {
      return nullptr;
    }
  }

  [[nodiscard]] auto GetRoad(common::RoadId identifier) const noexcept
      -> std::unique_ptr<const interfaces::IRoad> override {
    try {
      return std::make_unique<const RoadAdapter>(map_.get().roads.at(identifier()));
    } catch (...) {
      return nullptr;
    }
  }

  inline auto Get() noexcept -> pointers_map::PointersMap& { return map_.get(); }

  [[nodiscard]] inline auto Get() const noexcept -> const pointers_map::PointersMap& { return map_.get(); }

 private:
  std::reference_wrapper<pointers_map::PointersMap> map_;
};

}  // namespace inheritance::pointers_map_adapter

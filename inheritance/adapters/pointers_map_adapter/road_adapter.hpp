/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>
#include <memory>
#include <pointers_road.hpp>

#include "common/common.hpp"
#include "inheritance/adapters/pointers_map_adapter/lane_adapter.hpp"
#include "inheritance/interfaces/i_lane.hpp"
#include "inheritance/interfaces/i_road.hpp"

namespace inheritance::pointers_map_adapter {

class RoadAdapter final : public interfaces::IRoad {
 public:
  ~RoadAdapter() override = default;
  explicit RoadAdapter(pointers_map::PointersRoad& road) : road_{road} {}

  auto GetLane(common::LaneId identifier) noexcept -> std::unique_ptr<interfaces::ILane> override {
    try {
      return std::make_unique<LaneAdapter>(road_.get().lanes.at(identifier()));
    } catch (...) {
      return nullptr;
    }
  }

  [[nodiscard]] auto GetLane(common::LaneId identifier) const noexcept
      -> std::unique_ptr<const interfaces::ILane> override {
    try {
      return std::make_unique<const LaneAdapter>(road_.get().lanes.at(identifier()));
    } catch (...) {
      return nullptr;
    }
  }

  auto GetNextRoad() noexcept -> std::unique_ptr<interfaces::IRoad> override {
    return road_.get().next_road == nullptr ? nullptr : std::make_unique<RoadAdapter>(*road_.get().next_road);
  }

  [[nodiscard]] auto GetNextRoad() const noexcept -> std::unique_ptr<const interfaces::IRoad> override {
    return road_.get().next_road == nullptr ? nullptr : std::make_unique<const RoadAdapter>(*road_.get().next_road);
  }

  auto GetPreviousRoad() noexcept -> std::unique_ptr<interfaces::IRoad> override {
    return road_.get().previous_road == nullptr ? nullptr : std::make_unique<RoadAdapter>(*road_.get().previous_road);
  }

  [[nodiscard]] auto GetPreviousRoad() const noexcept -> std::unique_ptr<const interfaces::IRoad> override {
    return road_.get().previous_road == nullptr ? nullptr
                                                : std::make_unique<const RoadAdapter>(*road_.get().previous_road);
  }

  inline auto Get() noexcept -> pointers_map::PointersRoad& { return road_.get(); }

  [[nodiscard]] inline auto Get() const noexcept -> const pointers_map::PointersRoad& { return road_.get(); }

 private:
  std::reference_wrapper<pointers_map::PointersRoad> road_;
};

}  // namespace inheritance::pointers_map_adapter

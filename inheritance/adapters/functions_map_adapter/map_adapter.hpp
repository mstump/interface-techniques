/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>
#include <functions_map.hpp>
#include <memory>

#include "common/common.hpp"
#include "inheritance/adapters/functions_map_adapter/road_adapter.hpp"
#include "inheritance/interfaces/i_map.hpp"
#include "inheritance/interfaces/i_road.hpp"

namespace inheritance::functions_map_adapter {

class MapAdapter final : public interfaces::IMap {
 public:
  ~MapAdapter() override = default;

  explicit MapAdapter(functions_map::FunctionsMap& map) : map_(map){};

  auto GetRoad(common::RoadId identifier) noexcept -> std::unique_ptr<interfaces::IRoad> override {
    if (auto road = map_.get().GetRoad(identifier()); road.has_value()) {
      return std::make_unique<RoadAdapter>(road.value());
    }
    return nullptr;
  }

  [[nodiscard]] auto GetRoad(common::RoadId identifier) const noexcept
      -> std::unique_ptr<const interfaces::IRoad> override {
    if (auto road = map_.get().GetRoad(identifier()); road.has_value()) {
      return std::make_unique<const RoadAdapter>(road.value());
    }
    return nullptr;
  }

  inline auto Get() noexcept -> functions_map::FunctionsMap& { return map_.get(); }

  [[nodiscard]] inline auto Get() const noexcept -> const functions_map::FunctionsMap& { return map_.get(); }

 private:
  std::reference_wrapper<functions_map::FunctionsMap> map_;
};

}  // namespace inheritance::functions_map_adapter

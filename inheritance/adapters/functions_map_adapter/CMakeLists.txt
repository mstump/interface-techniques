# ##############################################################################
# Copyright (c) 2023 Mercedes-Benz Tech Innovation GmbH
#
# This program and the accompanying materials are made available under the terms
# of the Eclipse Public License 2.0 which is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# ##############################################################################

add_library(inheritance_functions_map_adapter INTERFACE)
add_library(inheritance::functions_map_adapter ALIAS
            inheritance_functions_map_adapter)
target_include_directories(inheritance_functions_map_adapter
                           INTERFACE ${PROJECT_SOURCE_DIR})
target_link_libraries(inheritance_functions_map_adapter
                      INTERFACE inheritance::interfaces functions_map::map)

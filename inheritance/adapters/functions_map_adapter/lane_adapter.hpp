/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>
#include <functions_lane.hpp>
#include <memory>

#include "inheritance/interfaces/i_lane.hpp"

namespace inheritance::functions_map_adapter {

class LaneAdapter final : public interfaces::ILane {
 public:
  ~LaneAdapter() override = default;
  explicit LaneAdapter(functions_map::FunctionsLane& lane) : lane_{lane} {}

  [[nodiscard]] auto GetWidth() const noexcept -> double override { return lane_.get().GetWidth(); }

  auto GetLeftLane() noexcept -> std::unique_ptr<interfaces::ILane> override {
    if (auto left_lane = lane_.get().GetLeftLane(); left_lane.has_value()) {
      return std::make_unique<LaneAdapter>(left_lane.value());
    }
    return nullptr;
  }

  [[nodiscard]] auto GetLeftLane() const noexcept -> std::unique_ptr<const interfaces::ILane> override {
    if (auto left_lane = lane_.get().GetLeftLane(); left_lane.has_value()) {
      return std::make_unique<const LaneAdapter>(left_lane.value());
    }
    return nullptr;
  }

  auto GetRightLane() noexcept -> std::unique_ptr<interfaces::ILane> override {
    if (auto right_lane = lane_.get().GetRightLane(); right_lane.has_value()) {
      return std::make_unique<LaneAdapter>(right_lane.value());
    }
    return nullptr;
  }

  [[nodiscard]] auto GetRightLane() const noexcept -> std::unique_ptr<const interfaces::ILane> override {
    if (auto right_lane = lane_.get().GetRightLane(); right_lane.has_value()) {
      return std::make_unique<const LaneAdapter>(right_lane.value());
    }
    return nullptr;
  }

  inline auto Get() noexcept -> functions_map::FunctionsLane& { return lane_.get(); }

  [[nodiscard]] inline auto Get() const noexcept -> const functions_map::FunctionsLane& { return lane_.get(); }

 private:
  std::reference_wrapper<functions_map::FunctionsLane> lane_;
};

}  // namespace inheritance::functions_map_adapter

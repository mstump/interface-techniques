/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>
#include <functions_road.hpp>
#include <memory>

#include "common/common.hpp"
#include "inheritance/adapters/functions_map_adapter/lane_adapter.hpp"
#include "inheritance/interfaces/i_lane.hpp"
#include "inheritance/interfaces/i_road.hpp"

namespace inheritance::functions_map_adapter {

class RoadAdapter final : public interfaces::IRoad {
 public:
  ~RoadAdapter() override = default;
  explicit RoadAdapter(functions_map::FunctionsRoad& road) : road_{road} {}

  auto GetLane(common::LaneId identifier) noexcept -> std::unique_ptr<interfaces::ILane> override {
    if (auto lane = road_.get().GetLane(identifier()); lane.has_value()) {
      return std::make_unique<LaneAdapter>(lane.value());
    }
    return nullptr;
  }

  [[nodiscard]] auto GetLane(common::LaneId identifier) const noexcept
      -> std::unique_ptr<const interfaces::ILane> override {
    if (auto lane = road_.get().GetLane(identifier()); lane.has_value()) {
      return std::make_unique<const LaneAdapter>(lane.value());
    }
    return nullptr;
  }

  auto GetNextRoad() noexcept -> std::unique_ptr<interfaces::IRoad> override {
    if (auto next_road = road_.get().GetNextRoad(); next_road.has_value()) {
      return std::make_unique<RoadAdapter>(next_road.value());
    }
    return nullptr;
  }

  [[nodiscard]] auto GetNextRoad() const noexcept -> std::unique_ptr<const interfaces::IRoad> override {
    if (auto next_road = road_.get().GetNextRoad(); next_road.has_value()) {
      return std::make_unique<const RoadAdapter>(next_road.value());
    }
    return nullptr;
  }

  auto GetPreviousRoad() noexcept -> std::unique_ptr<interfaces::IRoad> override {
    if (auto next_road = road_.get().GetPreviousRoad(); next_road.has_value()) {
      return std::make_unique<RoadAdapter>(next_road.value());
    }
    return nullptr;
  }

  [[nodiscard]] auto GetPreviousRoad() const noexcept -> std::unique_ptr<const interfaces::IRoad> override {
    if (auto next_road = road_.get().GetPreviousRoad(); next_road.has_value()) {
      return std::make_unique<const RoadAdapter>(next_road.value());
    }
    return nullptr;
  }

  inline auto Get() noexcept -> functions_map::FunctionsRoad& { return road_.get(); }

  [[nodiscard]] inline auto Get() const noexcept -> const functions_map::FunctionsRoad& { return road_.get(); }

 private:
  std::reference_wrapper<functions_map::FunctionsRoad> road_;
};

}  // namespace inheritance::functions_map_adapter

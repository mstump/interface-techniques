/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <exceptions_lane.hpp>
#include <functional>
#include <limits>
#include <memory>

#include "inheritance/interfaces/i_lane.hpp"

namespace inheritance::exceptions_map_adapter {

class LaneAdapter final : public interfaces::ILane {
 public:
  ~LaneAdapter() override = default;
  explicit LaneAdapter(exceptions_map::ExceptionsLane& lane) : lane_{lane} {}

  [[nodiscard]] auto GetWidth() const noexcept -> double override {
    try {
      return lane_.get().GetWidth();
    } catch (...) {
      return std::numeric_limits<double>::quiet_NaN();
    }
  }

  auto GetLeftLane() noexcept -> std::unique_ptr<interfaces::ILane> override {
    try {
      return std::make_unique<LaneAdapter>(lane_.get().GetLeftLane());
    } catch (...) {
      return nullptr;
    }
  }

  [[nodiscard]] auto GetLeftLane() const noexcept -> std::unique_ptr<const interfaces::ILane> override {
    try {
      return std::make_unique<const LaneAdapter>(lane_.get().GetLeftLane());
    } catch (...) {
      return nullptr;
    }
  }

  auto GetRightLane() noexcept -> std::unique_ptr<interfaces::ILane> override {
    try {
      return std::make_unique<LaneAdapter>(lane_.get().GetRightLane());
    } catch (...) {
      return nullptr;
    }
  }

  [[nodiscard]] auto GetRightLane() const noexcept -> std::unique_ptr<const interfaces::ILane> override {
    try {
      return std::make_unique<const LaneAdapter>(lane_.get().GetRightLane());
    } catch (...) {
      return nullptr;
    }
  }

  inline auto Get() noexcept -> exceptions_map::ExceptionsLane& { return lane_.get(); }

  [[nodiscard]] inline auto Get() const noexcept -> const exceptions_map::ExceptionsLane& { return lane_.get(); }

 private:
  std::reference_wrapper<exceptions_map::ExceptionsLane> lane_;
};

}  // namespace inheritance::exceptions_map_adapter

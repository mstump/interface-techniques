# ##############################################################################
# Copyright (c) 2023 Mercedes-Benz Tech Innovation GmbH
#
# This program and the accompanying materials are made available under the terms
# of the Eclipse Public License 2.0 which is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# ##############################################################################

add_library(inheritance_exceptions_map_adapter INTERFACE)
add_library(inheritance::exceptions_map_adapter ALIAS
            inheritance_exceptions_map_adapter)
target_include_directories(inheritance_exceptions_map_adapter
                           INTERFACE ${PROJECT_SOURCE_DIR})
target_link_libraries(inheritance_exceptions_map_adapter
                      INTERFACE inheritance::interfaces exceptions_map::map)

/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <exceptions_map.hpp>
#include <functional>
#include <memory>

#include "common/common.hpp"
#include "inheritance/adapters/exceptions_map_adapter/road_adapter.hpp"
#include "inheritance/interfaces/i_map.hpp"
#include "inheritance/interfaces/i_road.hpp"

namespace inheritance::exceptions_map_adapter {

class MapAdapter final : public interfaces::IMap {
 public:
  ~MapAdapter() override = default;

  explicit MapAdapter(exceptions_map::ExceptionsMap& map) : map_(map){};

  auto GetRoad(common::RoadId identifier) noexcept -> std::unique_ptr<interfaces::IRoad> override {
    try {
      return std::make_unique<RoadAdapter>(map_.get().GetRoad(identifier()));
    } catch (...) {
      return nullptr;
    }
  }

  [[nodiscard]] auto GetRoad(common::RoadId identifier) const noexcept
      -> std::unique_ptr<const interfaces::IRoad> override {
    try {
      return std::make_unique<const RoadAdapter>(map_.get().GetRoad(identifier()));
    } catch (...) {
      return nullptr;
    }
  }

  inline auto Get() noexcept -> exceptions_map::ExceptionsMap& { return map_.get(); }

  [[nodiscard]] inline auto Get() const noexcept -> const exceptions_map::ExceptionsMap& { return map_.get(); }

 private:
  std::reference_wrapper<exceptions_map::ExceptionsMap> map_;
};

}  // namespace inheritance::exceptions_map_adapter

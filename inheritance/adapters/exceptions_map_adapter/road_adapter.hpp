/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <exceptions_road.hpp>
#include <functional>
#include <memory>

#include "common/common.hpp"
#include "inheritance/adapters/exceptions_map_adapter/lane_adapter.hpp"
#include "inheritance/interfaces/i_lane.hpp"
#include "inheritance/interfaces/i_road.hpp"

namespace inheritance::exceptions_map_adapter {

class RoadAdapter final : public interfaces::IRoad {
 public:
  ~RoadAdapter() override = default;
  explicit RoadAdapter(exceptions_map::ExceptionsRoad& road) : road_{road} {}

  auto GetLane(common::LaneId identifier) noexcept -> std::unique_ptr<interfaces::ILane> override {
    try {
      return std::make_unique<LaneAdapter>(road_.get().GetLane(identifier()));
    } catch (...) {
      return nullptr;
    }
  }

  [[nodiscard]] auto GetLane(common::LaneId identifier) const noexcept
      -> std::unique_ptr<const interfaces::ILane> override {
    try {
      return std::make_unique<const LaneAdapter>(road_.get().GetLane(identifier()));
    } catch (...) {
      return nullptr;
    }
  }

  auto GetNextRoad() noexcept -> std::unique_ptr<interfaces::IRoad> override {
    try {
      return std::make_unique<RoadAdapter>(road_.get().GetNextRoad());
    } catch (...) {
      return nullptr;
    }
  }

  [[nodiscard]] auto GetNextRoad() const noexcept -> std::unique_ptr<const interfaces::IRoad> override {
    try {
      return std::make_unique<const RoadAdapter>(road_.get().GetNextRoad());
    } catch (...) {
      return nullptr;
    }
  }

  auto GetPreviousRoad() noexcept -> std::unique_ptr<interfaces::IRoad> override {
    try {
      return std::make_unique<RoadAdapter>(road_.get().GetPreviousRoad());
    } catch (...) {
      return nullptr;
    }
  }

  [[nodiscard]] auto GetPreviousRoad() const noexcept -> std::unique_ptr<const interfaces::IRoad> override {
    try {
      return std::make_unique<const RoadAdapter>(road_.get().GetPreviousRoad());
    } catch (...) {
      return nullptr;
    }
  }

  inline auto Get() noexcept -> exceptions_map::ExceptionsRoad& { return road_.get(); }

  [[nodiscard]] inline auto Get() const noexcept -> const exceptions_map::ExceptionsRoad& { return road_.get(); }

 private:
  std::reference_wrapper<exceptions_map::ExceptionsRoad> road_;
};

}  // namespace inheritance::exceptions_map_adapter

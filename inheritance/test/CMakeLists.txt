# ##############################################################################
# Copyright (c) 2023 Mercedes-Benz Tech Innovation GmbH
#
# This program and the accompanying materials are made available under the terms
# of the Eclipse Public License 2.0 which is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# ##############################################################################

add_executable(inheritance_test)
add_executable(inheritance::test ALIAS inheritance_test)
target_sources(inheritance_test PRIVATE main.cpp)
target_link_libraries(
  inheritance_test
  PRIVATE inheritance::interfaces
          inheritance::exceptions_map_adapter
          inheritance::functions_map_adapter
          inheritance::pointers_map_adapter
          exceptions_map::map
          functions_map::map
          pointers_map::map)
add_test(NAME inheritance_test COMMAND inheritance_test)

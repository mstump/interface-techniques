/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <exceptions_map.hpp>   // for ExceptionsMap
#include <exceptions_road.hpp>  // for ExceptionsRoad
#include <functional>           // for std::reference_wrapper
#include <optional>             // for std::optional

#include "common/common.hpp"                                                         // for RoadId
#include "template_specialization/adapters/exceptions_map_adapter/road_adapter.hpp"  // for RoadAdapter
#include "template_specialization/interfaces/i_map.hpp"                              // for IMap

namespace template_specialization::interfaces {

template <>
struct IMap<exceptions_map::ExceptionsMap> {
  using RoadType = IRoad<exceptions_map::ExceptionsRoad>;

  explicit IMap(exceptions_map::ExceptionsMap& map) : map_(map) {}

  auto GetRoad(const common::RoadId identifier) noexcept -> std::optional<RoadType> {
    try {
      return RoadType{map_.get().GetRoad(identifier())};
    } catch (...) {
      return std::nullopt;
    }
  }

  [[nodiscard]] auto GetRoad(const common::RoadId identifier) const noexcept -> std::optional<const RoadType> {
    try {
      return RoadType{map_.get().GetRoad(identifier())};
    } catch (...) {
      return std::nullopt;
    }
  }

  auto Get() noexcept -> exceptions_map::ExceptionsMap& { return map_.get(); }

  [[nodiscard]] auto Get() const noexcept -> const exceptions_map::ExceptionsMap& { return map_.get(); }

 private:
  std::reference_wrapper<exceptions_map::ExceptionsMap> map_;
};

}  // namespace template_specialization::interfaces

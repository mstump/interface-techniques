/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <exceptions_lane.hpp>  // for ExceptionsLane
#include <exceptions_road.hpp>  // for ExceptionsRoad
#include <functional>           // for std::reference_wrapper
#include <optional>             // for std::optional

#include "common/common.hpp"                                                         // for LaneId, RoadId
#include "template_specialization/adapters/exceptions_map_adapter/lane_adapter.hpp"  // for LaneAdapter
#include "template_specialization/interfaces/i_road.hpp"                             // for IRoad

namespace template_specialization::interfaces {

template <>
struct IRoad<exceptions_map::ExceptionsRoad> {
  using LaneType = ILane<exceptions_map::ExceptionsLane>;

  explicit IRoad(exceptions_map::ExceptionsRoad& road) : road_(road) {}

  auto GetLane(const common::LaneId identifier) noexcept -> std::optional<LaneType> {
    try {
      return LaneType{road_.get().GetLane(identifier())};
    } catch (...) {
      return std::nullopt;
    }
  }

  [[nodiscard]] auto GetLane(const common::LaneId identifier) const noexcept -> std::optional<const LaneType> {
    try {
      return LaneType{road_.get().GetLane(identifier())};
    } catch (...) {
      return std::nullopt;
    }
  }

  auto GetNextRoad() noexcept -> std::optional<IRoad> {
    try {
      return IRoad{road_.get().GetNextRoad()};
    } catch (...) {
      return std::nullopt;
    }
  }

  [[nodiscard]] auto GetNextRoad() const noexcept -> std::optional<const IRoad> {
    try {
      return IRoad{road_.get().GetNextRoad()};
    } catch (...) {
      return std::nullopt;
    }
  }

  auto GetPreviousRoad() noexcept -> std::optional<IRoad> {
    try {
      return IRoad{road_.get().GetPreviousRoad()};
    } catch (...) {
      return std::nullopt;
    }
  }

  [[nodiscard]] auto GetPreviousRoad() const noexcept -> std::optional<const IRoad> {
    try {
      return IRoad{road_.get().GetPreviousRoad()};
    } catch (...) {
      return std::nullopt;
    }
  }

  auto Get() noexcept -> exceptions_map::ExceptionsRoad& { return road_.get(); }

  [[nodiscard]] auto Get() const noexcept -> const exceptions_map::ExceptionsRoad& { return road_.get(); }

 private:
  std::reference_wrapper<exceptions_map::ExceptionsRoad> road_;
};

}  // namespace template_specialization::interfaces

/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <exceptions_lane.hpp>  // for ExceptionsLane
#include <functional>           // for std::reference_wrapper
#include <optional>             // for std::optional

#include "template_specialization/interfaces/i_lane.hpp"  // for ILane

namespace template_specialization::interfaces {

template <>
inline auto ILane<exceptions_map::ExceptionsLane>::GetLeftLane() noexcept -> std::optional<ILane> {
  try {
    return ILane{lane_.get().GetLeftLane()};
  } catch (...) {
    return std::nullopt;
  }
}

template <>
[[nodiscard]] inline auto ILane<exceptions_map::ExceptionsLane>::GetLeftLane() const noexcept
    -> std::optional<const ILane> {
  try {
    return ILane{lane_.get().GetLeftLane()};
  } catch (...) {
    return std::nullopt;
  }
}

template <>
inline auto ILane<exceptions_map::ExceptionsLane>::GetRightLane() noexcept -> std::optional<ILane> {
  try {
    return ILane{lane_.get().GetRightLane()};
  } catch (...) {
    return std::nullopt;
  }
}

template <>
[[nodiscard]] inline auto ILane<exceptions_map::ExceptionsLane>::GetRightLane() const noexcept
    -> std::optional<const ILane> {
  try {
    return ILane{lane_.get().GetRightLane()};
  } catch (...) {
    return std::nullopt;
  }
}

}  // namespace template_specialization::interfaces

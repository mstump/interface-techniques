/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>         // for std::reference_wrapper
#include <optional>           // for std::optional
#include <pointers_map.hpp>   // for PointersMap
#include <pointers_road.hpp>  // for PointersRoad

#include "common/common.hpp"                                                       // for RoadId
#include "template_specialization/adapters/pointers_map_adapter/road_adapter.hpp"  // for RoadAdapter
#include "template_specialization/interfaces/i_map.hpp"                            // for IMap

namespace template_specialization::interfaces {

template <>
struct IMap<pointers_map::PointersMap> {
  using RoadType = IRoad<pointers_map::PointersRoad>;

  explicit IMap(pointers_map::PointersMap& map) : map_(map) {}

  auto GetRoad(const common::RoadId identifier) noexcept -> std::optional<RoadType> {
    try {
      return RoadType{map_.get().roads.at(identifier())};
    } catch (...) {
      return std::nullopt;
    }
  }

  [[nodiscard]] auto GetRoad(const common::RoadId identifier) const noexcept -> std::optional<const RoadType> {
    try {
      return RoadType{map_.get().roads.at(identifier())};
    } catch (...) {
      return std::nullopt;
    }
  }

  auto Get() noexcept -> pointers_map::PointersMap& { return map_.get(); }

  [[nodiscard]] auto Get() const noexcept -> const pointers_map::PointersMap& { return map_.get(); }

 private:
  std::reference_wrapper<pointers_map::PointersMap> map_;
};

}  // namespace template_specialization::interfaces

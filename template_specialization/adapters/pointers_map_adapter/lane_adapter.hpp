/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>         // for std::reference_wrapper
#include <optional>           // for std::optional
#include <pointers_lane.hpp>  // for PointersLane

#include "template_specialization/interfaces/i_lane.hpp"  // for ILane

namespace template_specialization::interfaces {

template <>
[[nodiscard]] inline auto ILane<pointers_map::PointersLane>::GetWidth() const noexcept -> double {
  return lane_.get().width;
}

template <>
inline auto ILane<pointers_map::PointersLane>::GetLeftLane() noexcept -> std::optional<ILane> {
  return lane_.get().left_lane == nullptr ? std::nullopt : std::make_optional<ILane>(*lane_.get().left_lane);
}

template <>
[[nodiscard]] inline auto ILane<pointers_map::PointersLane>::GetLeftLane() const noexcept
    -> std::optional<const ILane> {
  return lane_.get().left_lane == nullptr ? std::nullopt : std::make_optional<ILane>(*lane_.get().left_lane);
}

template <>
inline auto ILane<pointers_map::PointersLane>::GetRightLane() noexcept -> std::optional<ILane> {
  return lane_.get().right_lane == nullptr ? std::nullopt : std::make_optional<ILane>(*lane_.get().right_lane);
}

template <>
[[nodiscard]] inline auto ILane<pointers_map::PointersLane>::GetRightLane() const noexcept
    -> std::optional<const ILane> {
  return lane_.get().right_lane == nullptr ? std::nullopt : std::make_optional<ILane>(*lane_.get().right_lane);
}

}  // namespace template_specialization::interfaces

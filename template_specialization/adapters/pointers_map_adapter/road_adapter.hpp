/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>         // for std::reference_wrapper
#include <optional>           // for std::optional
#include <pointers_lane.hpp>  // for PointersLane
#include <pointers_road.hpp>  // for PointersRoad

#include "common/common.hpp"                                                       // for RoadId
#include "template_specialization/adapters/pointers_map_adapter/lane_adapter.hpp"  // for LaneAdapter
#include "template_specialization/interfaces/i_road.hpp"                           // for IRoad

namespace template_specialization::interfaces {

template <>
struct IRoad<pointers_map::PointersRoad> {
  using LaneType = ILane<pointers_map::PointersLane>;

  explicit IRoad(pointers_map::PointersRoad& road) : road_(road) {}

  auto GetLane(const common::LaneId identifier) noexcept -> std::optional<LaneType> {
    try {
      return LaneType{road_.get().lanes.at(identifier())};
    } catch (...) {
      return std::nullopt;
    }
  }

  [[nodiscard]] auto GetLane(const common::LaneId identifier) const noexcept -> std::optional<const LaneType> {
    try {
      return LaneType{road_.get().lanes.at(identifier())};
    } catch (...) {
      return std::nullopt;
    }
  }

  auto GetNextRoad() noexcept -> std::optional<IRoad> {
    return road_.get().next_road == nullptr ? std::nullopt : std::make_optional<IRoad>(*road_.get().next_road);
  }

  [[nodiscard]] auto GetNextRoad() const noexcept -> std::optional<const IRoad> {
    return road_.get().next_road == nullptr ? std::nullopt : std::make_optional<IRoad>(*road_.get().next_road);
  }

  auto GetPreviousRoad() noexcept -> std::optional<IRoad> {
    return road_.get().previous_road == nullptr ? std::nullopt : std::make_optional<IRoad>(*road_.get().previous_road);
  }

  [[nodiscard]] auto GetPreviousRoad() const noexcept -> std::optional<const IRoad> {
    return road_.get().previous_road == nullptr ? std::nullopt : std::make_optional<IRoad>(*road_.get().previous_road);
  }

  auto Get() noexcept -> pointers_map::PointersRoad& { return road_.get(); }

  [[nodiscard]] auto Get() const noexcept -> const pointers_map::PointersRoad& { return road_.get(); }

 private:
  std::reference_wrapper<pointers_map::PointersRoad> road_;
};

}  // namespace template_specialization::interfaces

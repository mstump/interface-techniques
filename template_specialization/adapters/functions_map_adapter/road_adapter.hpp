/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>          // for std::reference_wrapper
#include <functions_lane.hpp>  // for FunctionsLane
#include <functions_road.hpp>  // for FunctionsRoad
#include <optional>            // for std::optional

#include "common/common.hpp"                                                        // for LaneId, RoadId
#include "template_specialization/adapters/functions_map_adapter/lane_adapter.hpp"  // for LaneAdapter
#include "template_specialization/adapters/functions_map_adapter/road_adapter.hpp"  // for RoadAdapter
#include "template_specialization/interfaces/i_lane.hpp"                            // for ILane
#include "template_specialization/interfaces/i_road.hpp"                            // for IRoad

namespace template_specialization::interfaces {

template <>
struct IRoad<functions_map::FunctionsRoad> {
  using LaneType = ILane<functions_map::FunctionsLane>;

  explicit IRoad(functions_map::FunctionsRoad& road) : road_(road) {}

  auto GetLane(const common::LaneId identifier) noexcept -> std::optional<LaneType> {
    if (auto lane = road_.get().GetLane(identifier()); lane.has_value()) {
      return LaneType{lane.value()};
    }

    return std::nullopt;
  }

  [[nodiscard]] auto GetLane(const common::LaneId identifier) const noexcept -> std::optional<const LaneType> {
    if (auto lane = road_.get().GetLane(identifier()); lane.has_value()) {
      return LaneType{lane.value()};
    }

    return std::nullopt;
  }

  auto GetNextRoad() noexcept -> std::optional<IRoad> {
    if (auto next_road = road_.get().GetNextRoad(); next_road.has_value()) {
      return IRoad{next_road.value()};
    }

    return std::nullopt;
  }

  [[nodiscard]] auto GetNextRoad() const noexcept -> std::optional<const IRoad> {
    if (auto next_road = road_.get().GetNextRoad(); next_road.has_value()) {
      return IRoad{next_road.value()};
    }

    return std::nullopt;
  }

  auto GetPreviousRoad() noexcept -> std::optional<IRoad> {
    if (auto previous_road = road_.get().GetPreviousRoad(); previous_road.has_value()) {
      return IRoad{previous_road.value()};
    }

    return std::nullopt;
  }

  [[nodiscard]] auto GetPreviousRoad() const noexcept -> std::optional<const IRoad> {
    if (auto previous_road = road_.get().GetPreviousRoad(); previous_road.has_value()) {
      return IRoad{previous_road.value()};
    }

    return std::nullopt;
  }

  auto Get() noexcept -> functions_map::FunctionsRoad& { return road_.get(); }

  [[nodiscard]] auto Get() const noexcept -> const functions_map::FunctionsRoad& { return road_.get(); }

 private:
  std::reference_wrapper<functions_map::FunctionsRoad> road_;
};

}  // namespace template_specialization::interfaces

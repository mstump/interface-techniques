/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>          // for std::reference_wrapper
#include <functions_lane.hpp>  // for FunctionsLane
#include <optional>            // for std::optional

#include "template_specialization/interfaces/i_lane.hpp"  // for ILane

namespace template_specialization::interfaces {

template <>
inline auto ILane<functions_map::FunctionsLane>::GetLeftLane() noexcept -> std::optional<ILane> {
  if (auto left_lane = lane_.get().GetLeftLane(); left_lane.has_value()) {
    return ILane{left_lane.value()};
  }

  return std::nullopt;
}

template <>
[[nodiscard]] inline auto ILane<functions_map::FunctionsLane>::GetLeftLane() const noexcept
    -> std::optional<const ILane> {
  if (auto left_lane = lane_.get().GetLeftLane(); left_lane.has_value()) {
    return ILane{left_lane.value()};
  }

  return std::nullopt;
}

template <>
inline auto ILane<functions_map::FunctionsLane>::GetRightLane() noexcept -> std::optional<ILane> {
  if (auto right_lane = lane_.get().GetRightLane(); right_lane.has_value()) {
    return ILane{right_lane.value()};
  }

  return std::nullopt;
}

template <>
[[nodiscard]] inline auto ILane<functions_map::FunctionsLane>::GetRightLane() const noexcept
    -> std::optional<const ILane> {
  if (auto right_lane = lane_.get().GetRightLane(); right_lane.has_value()) {
    return ILane{right_lane.value()};
  }

  return std::nullopt;
}

}  // namespace template_specialization::interfaces

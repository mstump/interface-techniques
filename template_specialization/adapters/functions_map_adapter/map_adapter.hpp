/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>          // for std::reference_wrapper
#include <functions_map.hpp>   // for FunctionsMap
#include <functions_road.hpp>  // for FunctionsRoad
#include <optional>            // for std::optional

#include "common/common.hpp"                                                        // for RoadId
#include "template_specialization/adapters/functions_map_adapter/road_adapter.hpp"  // for RoadAdapter
#include "template_specialization/interfaces/i_map.hpp"                             // for IMap
#include "template_specialization/interfaces/i_road.hpp"                            // for IRoad

namespace template_specialization::interfaces {

template <>
struct IMap<functions_map::FunctionsMap> {
  using RoadType = IRoad<functions_map::FunctionsRoad>;

  explicit IMap(functions_map::FunctionsMap& map) : map_(map) {}

  auto GetRoad(const common::RoadId identifier) noexcept -> std::optional<RoadType> {
    if (auto road = map_.get().GetRoad(identifier()); road.has_value()) {
      return RoadType{road.value()};
    }

    return std::nullopt;
  }

  [[nodiscard]] auto GetRoad(const common::RoadId identifier) const noexcept -> std::optional<const RoadType> {
    if (auto road = map_.get().GetRoad(identifier()); road.has_value()) {
      return RoadType{road.value()};
    }

    return std::nullopt;
  }

  auto Get() noexcept -> functions_map::FunctionsMap& { return map_.get(); }

  [[nodiscard]] auto Get() const noexcept -> const functions_map::FunctionsMap& { return map_.get(); }

 private:
  std::reference_wrapper<functions_map::FunctionsMap> map_;
};

}  // namespace template_specialization::interfaces

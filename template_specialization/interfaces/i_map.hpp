/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>  // for std::reference_wrapper
#include <optional>    // for std::optional
#include <utility>     // for std::declval

#include "common/common.hpp"
#include "template_specialization/interfaces/i_road.hpp"

namespace template_specialization::interfaces {

template <typename MapType>
struct IMap {
  using RoadType = decltype(std::declval<MapType>().GetRoad(std::declval<common::RoadId::UnderlyingType>()));

  explicit IMap(MapType& map) : map_(map) {}

  auto GetRoad(const common::RoadId identifier) noexcept -> std::optional<RoadType> {
    return IRoad{map_.get().GetRoad(identifier)};
  }

  [[nodiscard]] auto GetRoad(const common::RoadId identifier) const noexcept -> std::optional<const RoadType> {
    return IRoad{map_.get().GetRoad(identifier)};
  }

  auto Get() noexcept -> MapType& { return map_.get(); }

  [[nodiscard]] auto Get() const noexcept -> const MapType& { return map_.get(); }

 private:
  std::reference_wrapper<MapType> map_;
};

}  // namespace template_specialization::interfaces

/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>   // for std::reference_wrapper
#include <optional>     // for std::optional
#include <type_traits>  // for std::is_arithmetic_v
#include <utility>      // for std::declval

namespace template_specialization::interfaces {

template <typename LaneType>
struct ILane {
  explicit ILane(LaneType& lane) : lane_(lane) {}

  [[nodiscard]] auto GetWidth() const noexcept -> double { return lane_.get().GetWidth(); }

  auto GetLeftLane() noexcept -> std::optional<ILane> { return ILane{lane_.get().GetLeftLane()}; }

  [[nodiscard]] auto GetLeftLane() const noexcept -> std::optional<const ILane> {
    return ILane{lane_.get().GetLeftLane()};
  }

  auto GetRightLane() noexcept -> std::optional<ILane> { return ILane{lane_.get().GetRightLane()}; }

  [[nodiscard]] auto GetRightLane() const noexcept -> std::optional<const ILane> {
    return ILane{lane_.get().GetRightLane()};
  }

  auto Get() noexcept -> LaneType& { return lane_.get(); }

  [[nodiscard]] auto Get() const noexcept -> const LaneType& { return lane_.get(); }

 private:
  std::reference_wrapper<LaneType> lane_;
};

}  // namespace template_specialization::interfaces

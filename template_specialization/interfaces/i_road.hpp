/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>  // for std::reference_wrapper
#include <optional>    // for std::optional
#include <utility>     // for std::declval

#include "common/common.hpp"
#include "template_specialization/interfaces/i_lane.hpp"

namespace template_specialization::interfaces {

template <typename RoadType>
struct IRoad {
  using LaneType = decltype(std::declval<RoadType>().GetLane(std::declval<common::LaneId::UnderlyingType>()));

  explicit IRoad(RoadType& road) : road_(road) {}

  auto GetLane(const common::LaneId identifier) noexcept -> std::optional<LaneType> {
    return ILane{road_.get().GetLane(identifier)};
  }

  [[nodiscard]] auto GetLane(const common::LaneId identifier) const noexcept -> std::optional<const LaneType> {
    return ILane{road_.get().GetLane(identifier)};
  }

  auto GetNextRoad() noexcept -> std::optional<IRoad> { return IRoad{road_.get().GetNextRoad()}; }

  [[nodiscard]] auto GetNextRoad() const noexcept -> std::optional<const IRoad> {
    return IRoad{road_.get().GetNextRoad()};
  }

  auto Get() noexcept -> RoadType& { return road_.get(); }

  [[nodiscard]] auto Get() const noexcept -> const RoadType& { return road_.get(); }

 private:
  std::reference_wrapper<RoadType> road_;
};

}  // namespace template_specialization::interfaces

# ##############################################################################
# Copyright (c) 2023 Mercedes-Benz Tech Innovation GmbH
#
# This program and the accompanying materials are made available under the terms
# of the Eclipse Public License 2.0 which is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# ##############################################################################

set_directory_properties(
  PROPERTIES
    RULE_LAUNCH_COMPILE
    "time -a -f \"%S %U %e '%C'\" -o ${CMAKE_CURRENT_BINARY_DIR}/compile_time.txt"
    RULE_LAUNCH_LINK
    "time -a -f \"%S %U %e '%C'\" -o ${CMAKE_CURRENT_BINARY_DIR}/link_time.txt")

add_library(crtp_interfaces INTERFACE)
add_library(crtp::interfaces ALIAS crtp_interfaces)
target_include_directories(crtp_interfaces INTERFACE ${PROJECT_SOURCE_DIR})
target_link_libraries(crtp_interfaces INTERFACE common::common)

add_subdirectory(adapters)
add_subdirectory(test)

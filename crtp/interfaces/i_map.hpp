/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "common/common.hpp"  // for LaneId, RoadId

namespace crtp::interfaces {

//
// Enforces:
// * Function signature without return type
//   * noexcept
//   * const
//   * nodiscard
//
// Enables:
// * compile-time polymorphism
// * passing as concrete class with templates
// * `Get` function to retrieve the underlying type
// * value semantics
//
template <typename MapType>
struct IMap {
  auto GetRoad(const common::RoadId identifier) noexcept -> decltype(auto) { return Self().DoGetRoad(identifier); }
  [[nodiscard]] auto GetRoad(const common::RoadId identifier) const noexcept -> decltype(auto) {
    return Self().DoGetRoad(identifier);
  }

  auto Get() noexcept -> decltype(auto) { return Self().DoGet(); }
  [[nodiscard]] auto Get() const noexcept -> decltype(auto) { return Self().DoGet(); }

 private:
  IMap() = default;
  friend MapType;

  constexpr auto Self() noexcept -> MapType& { return static_cast<MapType&>(*this); }
  constexpr auto Self() const noexcept -> const MapType& { return static_cast<const MapType&>(*this); }
};

}  // namespace crtp::interfaces

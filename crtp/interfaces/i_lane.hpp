/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

namespace crtp::interfaces {

template <typename LaneType>
struct ILane {
  [[nodiscard]] auto GetWidth() const noexcept -> decltype(auto) { return Self().DoGetWidth(); }

  auto GetLeftLane() noexcept -> decltype(auto) { return Self().DoGetLeftLane(); }
  [[nodiscard]] auto GetLeftLane() const noexcept -> decltype(auto) { return Self().DoGetLeftLane(); }

  auto GetRightLane() noexcept -> decltype(auto) { return Self().DoGetRightLane(); }
  [[nodiscard]] auto GetRightLane() const noexcept -> decltype(auto) { return Self().DoGetRightLane(); }

  auto Get() noexcept -> decltype(auto) { return Self().DoGet(); }
  [[nodiscard]] auto Get() const noexcept -> decltype(auto) { return Self().DoGet(); }

 private:
  ILane() = default;
  friend LaneType;

  constexpr auto Self() noexcept -> LaneType& { return static_cast<LaneType&>(*this); }
  constexpr auto Self() const noexcept -> const LaneType& { return static_cast<const LaneType&>(*this); }
};

}  // namespace crtp::interfaces

/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>          // for std::reference_wrapper
#include <functions_lane.hpp>  // for FunctionsLane
#include <optional>            // for std::optional

#include "crtp/interfaces/i_lane.hpp"  // for ILane

namespace crtp::functions_map_adapter {

class LaneAdapter : public interfaces::ILane<LaneAdapter> {
 public:
  explicit LaneAdapter(functions_map::FunctionsLane& lane) : lane_{lane} {}

 private:
  friend interfaces::ILane<LaneAdapter>;

  [[nodiscard]] auto DoGetWidth() const noexcept -> double { return lane_.get().GetWidth(); }

  auto DoGetLeftLane() noexcept -> std::optional<LaneAdapter> {
    if (auto left_lane = lane_.get().GetLeftLane(); left_lane.has_value()) {
      return LaneAdapter{left_lane.value()};
    }

    return std::nullopt;
  }

  [[nodiscard]] auto DoGetLeftLane() const noexcept -> std::optional<const LaneAdapter> {
    if (const auto left_lane = lane_.get().GetLeftLane(); left_lane.has_value()) {
      return LaneAdapter{left_lane.value()};
    }

    return std::nullopt;
  }

  auto DoGetRightLane() noexcept -> std::optional<LaneAdapter> {
    if (auto right_lane = lane_.get().GetRightLane(); right_lane.has_value()) {
      return LaneAdapter{right_lane.value()};
    }

    return std::nullopt;
  }

  [[nodiscard]] auto DoGetRightLane() const noexcept -> std::optional<const LaneAdapter> {
    if (const auto right_lane = lane_.get().GetRightLane(); right_lane.has_value()) {
      return LaneAdapter{right_lane.value()};
    }

    return std::nullopt;
  }

  inline auto DoGet() noexcept -> functions_map::FunctionsLane& { return lane_.get(); }

  [[nodiscard]] inline auto DoGet() const noexcept -> const functions_map::FunctionsLane& { return lane_.get(); }

  std::reference_wrapper<functions_map::FunctionsLane> lane_;
};

}  // namespace crtp::functions_map_adapter

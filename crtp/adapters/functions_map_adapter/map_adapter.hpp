/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>         // for std::reference_wrapper
#include <functions_map.hpp>  // for FunctionsMap
#include <optional>           // for std::optional

#include "common/common.hpp"                                     // for RoadId
#include "crtp/adapters/functions_map_adapter/road_adapter.hpp"  // for RoadAdapter
#include "crtp/interfaces/i_map.hpp"                             // for IMap

namespace crtp::functions_map_adapter {

class MapAdapter : public interfaces::IMap<MapAdapter> {
 public:
  explicit MapAdapter(functions_map::FunctionsMap& map) : map_(map) {}

 private:
  friend interfaces::IMap<MapAdapter>;

  auto DoGetRoad(common::RoadId identifier) noexcept -> std::optional<RoadAdapter> {
    if (auto road = map_.get().GetRoad(identifier()); road.has_value()) {
      return RoadAdapter{road.value()};
    }

    return std::nullopt;
  }

  [[nodiscard]] auto DoGetRoad(common::RoadId identifier) const noexcept -> std::optional<const RoadAdapter> {
    if (const auto road = map_.get().GetRoad(identifier()); road.has_value()) {
      return RoadAdapter{road.value()};
    }

    return std::nullopt;
  }

  inline auto DoGet() noexcept -> functions_map::FunctionsMap& { return map_.get(); }

  [[nodiscard]] inline auto DoGet() const noexcept -> const functions_map::FunctionsMap& { return map_.get(); }

  std::reference_wrapper<functions_map::FunctionsMap> map_;
};

}  // namespace crtp::functions_map_adapter

/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>          // for std::reference_wrapper
#include <functions_road.hpp>  // for FunctionsRoad
#include <optional>            // for std::optional

#include "common/common.hpp"                                     // for RoadId
#include "crtp/adapters/functions_map_adapter/lane_adapter.hpp"  // for LaneAdapter
#include "crtp/interfaces/i_road.hpp"                            // for IRoad

namespace crtp::functions_map_adapter {

class RoadAdapter : public interfaces::IRoad<RoadAdapter> {
 public:
  explicit RoadAdapter(functions_map::FunctionsRoad& road) : road_{road} {}

 private:
  friend interfaces::IRoad<RoadAdapter>;

  auto DoGetLane(common::LaneId identifier) noexcept -> std::optional<LaneAdapter> {
    if (auto lane = road_.get().GetLane(identifier()); lane.has_value()) {
      return LaneAdapter{lane.value()};
    }

    return std::nullopt;
  }

  [[nodiscard]] auto DoGetLane(common::LaneId identifier) const noexcept -> std::optional<const LaneAdapter> {
    if (const auto lane = road_.get().GetLane(identifier()); lane.has_value()) {
      return LaneAdapter{lane.value()};
    }

    return std::nullopt;
  }

  auto DoGetNextRoad() noexcept -> std::optional<RoadAdapter> {
    if (auto next_road = road_.get().GetNextRoad(); next_road.has_value()) {
      return RoadAdapter{next_road.value()};
    }

    return std::nullopt;
  }

  [[nodiscard]] auto DoGetNextRoad() const noexcept -> std::optional<const RoadAdapter> {
    if (const auto next_road = road_.get().GetNextRoad(); next_road.has_value()) {
      return RoadAdapter{next_road.value()};
    }

    return std::nullopt;
  }

  auto DoGetPreviousRoad() noexcept -> std::optional<RoadAdapter> {
    if (auto previous_road = road_.get().GetPreviousRoad(); previous_road.has_value()) {
      return RoadAdapter{previous_road.value()};
    }

    return std::nullopt;
  }

  [[nodiscard]] auto DoGetPreviousRoad() const noexcept -> std::optional<const RoadAdapter> {
    if (const auto previous_road = road_.get().GetPreviousRoad(); previous_road.has_value()) {
      return RoadAdapter{previous_road.value()};
    }

    return std::nullopt;
  }

  inline auto DoGet() noexcept -> functions_map::FunctionsRoad& { return road_.get(); }

  [[nodiscard]] inline auto DoGet() const noexcept -> const functions_map::FunctionsRoad& { return road_.get(); }

  std::reference_wrapper<functions_map::FunctionsRoad> road_;
};

}  // namespace crtp::functions_map_adapter

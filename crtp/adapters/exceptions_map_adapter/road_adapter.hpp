/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <exceptions_road.hpp>  // for ExceptionsRoad
#include <functional>           // for std::reference_wrapper
#include <optional>             // for std::optional

#include "common/common.hpp"                                      // for LaneId, RoadId
#include "crtp/adapters/exceptions_map_adapter/lane_adapter.hpp"  // for LaneAdapter
#include "crtp/interfaces/i_road.hpp"                             // for IRoad

namespace crtp::exceptions_map_adapter {

class RoadAdapter : public interfaces::IRoad<RoadAdapter> {
 public:
  explicit RoadAdapter(exceptions_map::ExceptionsRoad& road) : road_{road} {}

 private:
  friend interfaces::IRoad<RoadAdapter>;

  auto DoGetLane(common::LaneId identifier) noexcept -> std::optional<LaneAdapter> {
    try {
      return LaneAdapter{road_.get().GetLane(identifier())};
    } catch (...) {
      return std::nullopt;
    }
  }

  [[nodiscard]] auto DoGetLane(common::LaneId identifier) const noexcept -> std::optional<const LaneAdapter> {
    try {
      return LaneAdapter{road_.get().GetLane(identifier())};
    } catch (...) {
      return std::nullopt;
    }
  }

  auto DoGetNextRoad() noexcept -> std::optional<RoadAdapter> {
    try {
      return RoadAdapter{road_.get().GetNextRoad()};
    } catch (...) {
      return std::nullopt;
    }
  }

  [[nodiscard]] auto DoGetNextRoad() const noexcept -> std::optional<const RoadAdapter> {
    try {
      return RoadAdapter{road_.get().GetNextRoad()};
    } catch (...) {
      return std::nullopt;
    }
  }

  auto DoGetPreviousRoad() noexcept -> std::optional<RoadAdapter> {
    try {
      return RoadAdapter{road_.get().GetPreviousRoad()};
    } catch (...) {
      return std::nullopt;
    }
  }

  [[nodiscard]] auto DoGetPreviousRoad() const noexcept -> std::optional<const RoadAdapter> {
    try {
      return RoadAdapter{road_.get().GetPreviousRoad()};
    } catch (...) {
      return std::nullopt;
    }
  }

  inline auto DoGet() noexcept -> exceptions_map::ExceptionsRoad& { return road_.get(); }

  [[nodiscard]] inline auto DoGet() const noexcept -> const exceptions_map::ExceptionsRoad& { return road_.get(); }

  std::reference_wrapper<exceptions_map::ExceptionsRoad> road_;
};

}  // namespace crtp::exceptions_map_adapter

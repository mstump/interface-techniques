/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <exceptions_lane.hpp>  // for ExceptionsLane
#include <functional>           // for std::reference_wrapper
#include <optional>             // for std::optional

#include "crtp/interfaces/i_lane.hpp"  // for ILane

namespace crtp::exceptions_map_adapter {

class LaneAdapter : public interfaces::ILane<LaneAdapter> {
 public:
  explicit LaneAdapter(exceptions_map::ExceptionsLane& lane) : lane_{lane} {}

 private:
  friend interfaces::ILane<LaneAdapter>;

  [[nodiscard]] auto DoGetWidth() const noexcept -> double {
    try {
      return lane_.get().GetWidth();
    } catch (...) {
      return std::numeric_limits<double>::quiet_NaN();
    }
  }

  auto DoGetLeftLane() noexcept -> std::optional<LaneAdapter> {
    try {
      return LaneAdapter{lane_.get().GetLeftLane()};
    } catch (...) {
      return std::nullopt;
    }
  }

  [[nodiscard]] auto DoGetLeftLane() const noexcept -> std::optional<const LaneAdapter> {
    try {
      return LaneAdapter{lane_.get().GetLeftLane()};
    } catch (...) {
      return std::nullopt;
    }
  }

  auto DoGetRightLane() noexcept -> std::optional<LaneAdapter> {
    try {
      return LaneAdapter{lane_.get().GetRightLane()};
    } catch (...) {
      return std::nullopt;
    }
  }

  [[nodiscard]] auto DoGetRightLane() const noexcept -> std::optional<const LaneAdapter> {
    try {
      return LaneAdapter{lane_.get().GetRightLane()};
    } catch (...) {
      return std::nullopt;
    }
  }

  inline auto DoGet() noexcept -> exceptions_map::ExceptionsLane& { return lane_.get(); }

  [[nodiscard]] inline auto DoGet() const noexcept -> const exceptions_map::ExceptionsLane& { return lane_.get(); }

  std::reference_wrapper<exceptions_map::ExceptionsLane> lane_;
};

}  // namespace crtp::exceptions_map_adapter

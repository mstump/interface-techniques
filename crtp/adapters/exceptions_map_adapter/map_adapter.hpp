/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <exceptions_map.hpp>  // for ExceptionsMap
#include <functional>          // for std::reference_wrapper
#include <optional>            // for std::optional

#include "common/common.hpp"                                      // for RoadId
#include "crtp/adapters/exceptions_map_adapter/road_adapter.hpp"  // for RoadAdapter
#include "crtp/interfaces/i_map.hpp"                              // for IMap

namespace crtp::exceptions_map_adapter {

class MapAdapter : public interfaces::IMap<MapAdapter> {
 public:
  explicit MapAdapter(exceptions_map::ExceptionsMap& map) : map_(map) {}

 private:
  friend interfaces::IMap<MapAdapter>;

  auto DoGetRoad(common::RoadId identifier) noexcept -> std::optional<RoadAdapter> {
    try {
      return RoadAdapter{map_.get().GetRoad(identifier())};
    } catch (...) {
      return std::nullopt;
    }
  }

  [[nodiscard]] auto DoGetRoad(common::RoadId identifier) const noexcept -> std::optional<const RoadAdapter> {
    try {
      return RoadAdapter{map_.get().GetRoad(identifier())};
    } catch (...) {
      return std::nullopt;
    }
  }

  inline auto DoGet() noexcept -> exceptions_map::ExceptionsMap& { return map_.get(); }

  [[nodiscard]] inline auto DoGet() const noexcept -> const exceptions_map::ExceptionsMap& { return map_.get(); }

  std::reference_wrapper<exceptions_map::ExceptionsMap> map_;
};

}  // namespace crtp::exceptions_map_adapter

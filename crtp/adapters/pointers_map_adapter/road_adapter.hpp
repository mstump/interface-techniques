/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>         // for std::reference_wrapper
#include <optional>           // for std::optional
#include <pointers_road.hpp>  // for PointersRoad

#include "common/common.hpp"                                    // for RoadId
#include "crtp/adapters/pointers_map_adapter/lane_adapter.hpp"  // for LaneAdapter
#include "crtp/interfaces/i_road.hpp"                           // for IRoad

namespace crtp::pointers_map_adapter {

class RoadAdapter : public interfaces::IRoad<RoadAdapter> {
 public:
  explicit RoadAdapter(pointers_map::PointersRoad& road) : road_{road} {}

 private:
  friend interfaces::IRoad<RoadAdapter>;

  auto DoGetLane(common::LaneId identifier) noexcept -> std::optional<LaneAdapter> try {
    return LaneAdapter{road_.get().lanes.at(identifier())};
  } catch (...) {
    return std::nullopt;
  }

  [[nodiscard]] auto DoGetLane(common::LaneId identifier) const noexcept -> std::optional<const LaneAdapter> try {
    return LaneAdapter{road_.get().lanes.at(identifier())};
  } catch (...) {
    return std::nullopt;
  }

  auto DoGetNextRoad() noexcept -> std::optional<RoadAdapter> {
    return road_.get().next_road == nullptr ? std::nullopt : std::make_optional<RoadAdapter>(*road_.get().next_road);
  }

  [[nodiscard]] auto DoGetNextRoad() const noexcept -> std::optional<const RoadAdapter> {
    return road_.get().next_road == nullptr ? std::nullopt
                                            : std::make_optional<const RoadAdapter>(*road_.get().next_road);
  }

  auto DoGetPreviousRoad() noexcept -> std::optional<RoadAdapter> {
    return road_.get().next_road == nullptr ? std::nullopt
                                            : std::make_optional<RoadAdapter>(*road_.get().previous_road);
  }

  [[nodiscard]] auto DoGetPreviousRoad() const noexcept -> std::optional<const RoadAdapter> {
    return road_.get().next_road == nullptr ? std::nullopt
                                            : std::make_optional<const RoadAdapter>(*road_.get().previous_road);
  }

  inline auto DoGet() noexcept -> pointers_map::PointersRoad& { return road_.get(); }

  [[nodiscard]] inline auto DoGet() const noexcept -> const pointers_map::PointersRoad& { return road_.get(); }

  std::reference_wrapper<pointers_map::PointersRoad> road_;
};

}  // namespace crtp::pointers_map_adapter

/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>        // for std::reference_wrapper
#include <optional>          // for std::optional
#include <pointers_map.hpp>  // for PointersMap

#include "common/common.hpp"                                    // for RoadId
#include "crtp/adapters/pointers_map_adapter/road_adapter.hpp"  // for RoadAdapter
#include "crtp/interfaces/i_map.hpp"                            // for IMap

namespace crtp::pointers_map_adapter {

class MapAdapter : public interfaces::IMap<MapAdapter> {
 public:
  explicit MapAdapter(pointers_map::PointersMap& map) : map_(map) {}

 private:
  friend interfaces::IMap<MapAdapter>;

  auto DoGetRoad(common::RoadId identifier) noexcept -> std::optional<RoadAdapter> try {
    return RoadAdapter{map_.get().roads.at(identifier())};
  } catch (...) {
    return std::nullopt;
  }

  [[nodiscard]] auto DoGetRoad(common::RoadId identifier) const noexcept -> std::optional<const RoadAdapter> try {
    return RoadAdapter{map_.get().roads.at(identifier())};
  } catch (...) {
    return std::nullopt;
  }

  inline auto DoGet() noexcept -> pointers_map::PointersMap& { return map_.get(); }

  [[nodiscard]] inline auto DoGet() const noexcept -> const pointers_map::PointersMap& { return map_.get(); }

  std::reference_wrapper<pointers_map::PointersMap> map_;
};

}  // namespace crtp::pointers_map_adapter

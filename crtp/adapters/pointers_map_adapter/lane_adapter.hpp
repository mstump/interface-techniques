/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>         // for std::reference_wrapper
#include <optional>           // for std::optional
#include <pointers_lane.hpp>  // for PointersLane

#include "crtp/interfaces/i_lane.hpp"  // for ILane

namespace crtp::pointers_map_adapter {

class LaneAdapter : public interfaces::ILane<LaneAdapter> {
 public:
  explicit LaneAdapter(pointers_map::PointersLane& lane) : lane_{lane} {}

 private:
  friend interfaces::ILane<LaneAdapter>;

  [[nodiscard]] auto DoGetWidth() const noexcept -> double { return lane_.get().width; }

  auto DoGetLeftLane() noexcept -> std::optional<LaneAdapter> {
    return lane_.get().left_lane == nullptr ? std::nullopt : std::make_optional<LaneAdapter>(*lane_.get().left_lane);
  }

  [[nodiscard]] auto DoGetLeftLane() const noexcept -> std::optional<const LaneAdapter> {
    return lane_.get().left_lane == nullptr ? std::nullopt
                                            : std::make_optional<const LaneAdapter>(*lane_.get().left_lane);
  }

  auto DoGetRightLane() noexcept -> std::optional<LaneAdapter> {
    return lane_.get().right_lane == nullptr ? std::nullopt : std::make_optional<LaneAdapter>(*lane_.get().right_lane);
  }

  [[nodiscard]] auto DoGetRightLane() const noexcept -> std::optional<const LaneAdapter> {
    return lane_.get().right_lane == nullptr ? std::nullopt
                                             : std::make_optional<const LaneAdapter>(*lane_.get().right_lane);
  }

  inline auto DoGet() noexcept -> pointers_map::PointersLane& { return lane_.get(); }

  [[nodiscard]] inline auto DoGet() const noexcept -> const pointers_map::PointersLane& { return lane_.get(); }

  std::reference_wrapper<pointers_map::PointersLane> lane_;
};

}  // namespace crtp::pointers_map_adapter
